program aws;

uses
  QForms,
  Main in 'Main.pas' {MainForm},
  Time in 'Time.pas' {TimeForm},
  FUPS in 'FUPS.pas' {UpsForm},
  Orient in 'Orient.pas' {UnfForm},
  FBTM in 'FBTM.pas' {BtmForm},
  IPCUdp in 'SRC\IPCUdp.pas',
  RxStrPktCom in 'SRC\RxStrPktCom.pas',
  UstDisplay in 'UstDisplay.pas' {UstForm},
  TrkCtrlInternal in 'SRC\TrkCtrlInternal.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TTimeForm, TimeForm);
  Application.CreateForm(TUpsForm, UpsForm);
  Application.CreateForm(TUnfForm, UnfForm);
  Application.CreateForm(TBtmForm, BtmForm);
  Application.CreateForm(TUstForm, UstForm);
  //  Application.CreateForm(TForm5, Form5);
  Application.Run;
end.
