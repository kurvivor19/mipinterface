unit Orient;

interface

uses
  SysUtils, Types, Classes, Variants, QTypes, QGraphics, QControls, QForms, 
  QDialogs, QStdCtrls, QButtons, IniFiles,
  ILM01, RxUnf, Nmea0183,
  RxAlias, RxUtils, QExtCtrls;

type TUNFStat = packed record
  ET:TExchangeState; // ��������� ������
  NumOfSat:U8;       // ������� ���. ���������
  UTCTime:U32;        // ����� ��� UTC
  Valid:BOOL;        // ���� �����
  Heading:F32;       // ���� ���. ����.
  Latitude:F32;      // ������ ���. ����. (- �����)
  Longitude:F32;     // ������� ���. ����. (- ��������)
  Altitude:F32;      // ������ ��� ������� ���� �. (- ���� ������ ����)
end;

type TILMStat = packed record
  ET:TExchangeState; // ��������� ������
  X:F32; // ���� ������� � ���������� ����������� ���. ����.
  Y:F32; // ���� ������� � ���������� ����������� ���. ����.
end;


type TUnfEvent = procedure of object;
type TUnfSetup = procedure( What:U8 ) of object;

type TIlmEvent = procedure of object;
type TIlmSetup = procedure( What:U8 ) of object;

type
  TUnfForm = class(TForm)
    GroupBox1: TGroupBox;
    SBRdZ: TSpeedButton;
    SBZSetup: TSpeedButton;
    CBZMean: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    LGPSStar:TLabel;
    LUnfOn: TLabel;
    EZ:TEdit;
    EX:TEdit;
    EY:TEdit;
    GroupBox2: TGroupBox;
    SBRdXY: TSpeedButton;
    SBXYSetup: TSpeedButton;
    RBXYManual: TRadioButton;
    RBRdXY: TRadioButton;
    CBXYMean: TCheckBox;
    Label7: TLabel;
    Label8: TLabel;
    Label4: TLabel;
    LTime: TLabel;
    LZ: TLabel;
    Label5: TLabel;
    LX: TLabel;
    LY: TLabel;
    LIlmON: TLabel;
    ETZ: TEdit;
    SpeedButton3: TSpeedButton;
    Label15: TLabel;
    Label16: TLabel;
    RBZManual: TRadioButton;
    RBRdZ: TRadioButton;
    GroupBox3: TGroupBox;
    RBLLAManual: TRadioButton;
    RBRdLLA: TRadioButton;
    CBLLAMean: TCheckBox;
    Label6: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    ELat: TEdit;
    ELong: TEdit;
    EAlt: TEdit;
    LAlt: TLabel;
    LLong: TLabel;
    LLat: TLabel;
    Label11: TLabel;
    SBRdLLA: TSpeedButton;
    SBLLASetup: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure SBRdZClick(Sender: TObject);
    procedure RBZManualClick(Sender: TObject);
  private
    FIlmDeviceName,FUnfDeviceName:string;
    Ilm:TILM01;
    Sps550:TRxUnf;
    //
    FZCnt,FXYCnt,FLLACnt:U32;
    FGetZ,FGetXY,FGetLLA:BOOL;
    FZMean,FXYMean,FLLAMean:BOOL;
    //
    FS: TFormatSettings;
    FLat,FLong,FAlt:F64;
    //
    FRotZ:F64;
    FRotY:F64;
    FRotX:F64;
    FRotZAdjustment:F64;
    FRotXAdjustment:F64;
    FRotYAdjustment:F64;
    FUTCTime: TDateTime;
    FIlmET,FUnfET:TExchangeState;
    FOnUnfEvent: TUnfEvent;
    FOnUnfSetup: TUnfSetup;
    FOnIlmEvent: TIlmEvent;
    FOnIlmSetup: TIlmSetup;
    FTimeZone:U8;
    IniFile:TIniFile;
    FILMStat: TILMStat;
    FUNFStat: TUNFStat;
    procedure IlmDataAvaliable(Sender:TObject; AX,AY:F64);
    procedure SPSDataAvail(Sender:TObject; S:TNmeaSentences);
    function  GetIlmDeviceName: string;
    function  GetUnfDeviceName: string;
    procedure SetIlmDeviceName(const Value: string);
    procedure SetUnfDeviceName(const Value: string);
    //
    procedure SetRotX(const Value:F64);
    procedure SetRotY(const Value:F64);
    procedure SetRotZ(const Value:F64);
    procedure SetTimeZone(const Value: U8);
    procedure SetRotXAdjustment(const Value: F64);
    procedure SetRotYAdjustment(const Value: F64);
    procedure SetRotZAdjustment(const Value: F64);
    procedure SetAlt(const Value: F64);
    procedure SetLat(const Value: F64);
    procedure SetLong(const Value: F64);
    procedure SetILMStat(const Value: TILMStat);
    procedure SetUNFStat(const Value: TUNFStat);
  public
    function  Connect():BOOL;
    procedure Disconnect();
    property IlmDeviceName:string read GetIlmDeviceName write SetIlmDeviceName;
    property UnfDeviceName:string read GetUnfDeviceName write SetUnfDeviceName;
    //
    property UTCTime:TDateTime read FUTCTime;
    property UnfET:TExchangeState read FUnfET;
    property IlmET:TExchangeState read FIlmET; 
    //
    property RotX:F64 read FRotX write SetRotX;
    property RotY:F64 read FRotY write SetRotY;
    property RotZ:F64 read FRotZ write SetRotZ;
    //
    property Lat:F64 read FLat write SetLat;
    property Long:F64 read FLong write SetLong;
    property Alt:F64 read FAlt write SetAlt;
    //
    property TimeZone:U8 read FTimeZone write SetTimeZone;
    //
    property UNFStat:TUNFStat read FUNFStat write SetUNFStat;
    property ILMStat:TILMStat read FILMStat write SetILMStat;
    //
    property RotXAdjustment:F64 read FRotXAdjustment write SetRotXAdjustment;
    property RotYAdjustment:F64 read FRotYAdjustment write SetRotYAdjustment;
    property RotZAdjustment:F64 read FRotZAdjustment write SetRotZAdjustment;
    //
    property OnUnfEvent:TUnfEvent read FOnUnfEvent write FOnUnfEvent default NIL;
    property OnUnfSetup:TUnfSetup read FOnUnfSetup write FOnUnfSetup default NIL;
    property OnIlmEvent:TIlmEvent read FOnIlmEvent write FOnIlmEvent default NIL;
    property OnIlmSetup:TIlmSetup read FOnIlmSetup write FOnIlmSetup default NIL;
  end;

var
  UnfForm:TUnfForm;

IMPLEMENTATION

{$R *.xfm}

//const MEAN_DELAY = 4.0;
const MEAN_DELAY = 60;

function TUnfForm.Connect: BOOL;
begin
  Ilm.Connect();
  SPS550.Connect();
end;

procedure TUnfForm.Disconnect;
begin
  Ilm.Disconnect();
  SPS550.Disconnect();
  Ilm.Free;
  SPS550.Free;
end;

procedure TUnfForm.FormCreate(Sender: TObject);
begin
  FS.DecimalSeparator:='.';

  Ilm:=TILM01.Create(Self);
  Ilm.Chnl.DeviceName:=FIlmDeviceName;
  Ilm.OnDataAvail:=IlmDataAvaliable;
  Ilm.ReversX:=False;
  Ilm.ReversY:=False;

  SPS550:=TRxUnf.Create(Self);
  SPS550.Chnl.DeviceName:=FUnfDeviceName;
  SPS550.OnDataAvail:=SPSDataAvail;
  
  //IniFile.Free;
end;

procedure TUnfForm.IlmDataAvaliable(Sender:TObject; AX,AY:F64);
begin
  FIlmET:=Ilm.ExState;
  if Assigned(OnIlmEvent) then OnIlmEvent;
  FILMStat.ET:=Ilm.ExState;

  FILMStat.X:=AX;
  FILMStat.Y:=AY;
  case Ilm.ExState of
    exsAnswer:begin

      LIlmOn.Caption:=' ���  '; LIlmOn.Color:=cllime; //LIlmOn.Font.Color:=clWhite;
      RBRdXY.Enabled:=True;
      EX.Enabled:=True; EY.Enabled:=True;

      LX.Caption:=Format('%6.2f',[AX],FS);
      LY.Caption:=Format('%6.2f',[AY],FS);

      if not CBXYMean.Checked and FGetXY then begin
        EX.Text:=Format('%6.2f',[AX],FS);
        EY.Text:=Format('%6.2f',[AY],FS);
        FRotX:=AX; FRotY:=AY;
        FGetXY:=False;
        SBRdXY.Enabled:=True;
        Exit;
      end;
      if CBXYMean.Checked and FGetXY then begin
        if FXYMean then begin
          EX.Text:=Format('%6.2f',[AX],FS);
          EY.Text:=Format('%6.2f',[AY],FS);
          FGetXY:=False;
          FXYMean:=False;
          SBRdXY.Enabled:=True;
          Exit;
        end;
        if FXYCnt = 1 then begin FRotX:=0.0; FRotY:=0.0; end;
        if FXYCnt > MEAN_DELAY then begin
          FXYMean:=True;
          FXYCnt:=1;
          FRotX:=FRotX/(1.0*MEAN_DELAY);
          FRotY:=FRotY/(1.0*MEAN_DELAY);
          Exit;
        end;
        FRotX:=FRotX + AX; FRotY:=FRotY + AY;
        Inc(FXYCnt);
      end;

    end;
    {}
    exsNoAnswered,
    exsDisconnected:begin
      LIlmOn.Caption:=' ���� '; LIlmOn.Color:=clSilver; LIlmOn.Font.Color:=clBlack;
      RBRdXY.Enabled:=False; RBRdXY.Checked:=False;
      CBXYMean.Enabled:=False; CBXYMean.Checked:=True;
      if not RBXYManual.Checked then RBXYManual.Toggle;
      if RBRdXY.Checked then RBRdXY.Toggle;
      SBRdXY.Enabled:=False;
      EX.Enabled:=True; EY.Enabled:=True;
      FGetXY:=False;
      LX.Caption:='';
      LY.Caption:='';
    end;
    {}
  end;
end;

procedure TUnfForm.SPSDataAvail(Sender: TObject; S: TNmeaSentences);
var AHeading,ALat,ALong,AAlt:F64;
begin
  FUnfET:=SPS550.ExState;
  // setup structure
  FUNFStat.ET:=SPS550.ExState;

  case SPS550.ExState of
    exsAnswer:begin

      LUnfOn.Caption:=' ���  '; LUnfOn.Color:=clLime; //LUnfOn.Font.Color:=clWhite;

      RBRdZ.Enabled:=True;
      CBZMean.Enabled:=True;

      RBRdLLA.Enabled:=True;
      CBLLAMean.Enabled:=True;
      
      case S.SentenceID of
        GGA:begin
          FUNFStat.Latitude:=S.GGA.Latitude;
          FUNFStat.Longitude:=S.GGA.Longitude;
          FUNFStat.Altitude:=S.GGA.Altitude;

          ALat:=S.GGA.Latitude;
          ALong:=S.GGA.Longitude;
          AAlt:=S.GGA.Altitude;
          with S.GGA do begin
            try
              FUTCTime:=EncodeTime(Hour,Min,Sec,MSec);
              FUNFStat.UTCTime:=DateTimeToTimeStamp(FUTCTime).Time;

              LTime.Caption:=TimeToStr(FUTCTime);
              LGPSStar.Caption:=IntToStr(NumOfSat);

              FUNFStat.NumOfSat:=NumOfSat;
              //
              LLat.Caption:=Format('%7.2f',[ALat],FS);
              LLong.Caption:=Format('%7.2f',[ALong],FS);
              LAlt.Caption:=Format('%7.2f',[AAlt],FS);
              if Assigned(OnUnfEvent) then OnUnfEvent;
            except
              on EConvertError do Exit;
            end;
          end;

        if not (CBLLAMean.Checked and RBRdLLA.Checked) and FGetLLA then begin
            ELat.Text:=Format('%7.2f',[ALat],FS);
            ELong.Text:=Format('%7.2f',[ALong],FS);
            EAlt.Text:=Format('%7.2f',[AAlt],FS);
            //FRotZ:=AHeading;
            FGetLLA:=False;
            SBRdLLA.Enabled:=True;
            if Assigned(OnUnfEvent) then OnUnfEvent;
            Exit;
         end;
         if CBLLAMean.Checked and RBRdLLA.Checked and FGetLLA then begin
            if FLLAMean then begin
              ELat.Text:=Format('%7.2f',[FLat],FS);
              ELong.Text:=Format('%7.2f',[FLong],FS);
              EAlt.Text:=Format('%7.2f',[FAlt],FS);
              FLLAMean:=False;
              FGetLLA:=False;
              SBRdLLA.Enabled:=True;
              if Assigned(OnUnfEvent) then OnUnfEvent;
              Exit;
            end;
            if FLLACnt = 1 then begin FLat:=0.0; FLong:=0.0; FAlt:=0.0; end;
            if FLLACnt > MEAN_DELAY then begin
               FLLAMean:=True;
               FLLACnt:=1;
               FLat:=FLat/(1.0*MEAN_DELAY);
               FLong:=FLong/(1.0*MEAN_DELAY);
               FAlt:=FAlt/(1.0*MEAN_DELAY);
               if Assigned(OnUnfEvent) then OnUnfEvent;
               Exit;
            end;
            FLat:=FLat + ALat;
            FLong:=FLong + ALong;
            FAlt:=FAlt + AAlt;
            Inc(FLLACnt);
          end;

        end;

        HDT:begin
         AHeading:=S.HDT.Heading;
         FUNFStat.Heading:=AHeading;
         FUNFStat.Valid:=S.HDT.Valid;
         if Assigned(OnUnfEvent) then OnUnfEvent;
         if not S.HDT.Valid then begin
            LZ.Caption:='�������';
            FGetZ:=False;
            SBRdZ.Enabled:=False;
         end else begin
            if not (CBZMean.Checked and RBRdZ.Checked and FGetZ) then SBRdZ.Enabled:=True;
            LZ.Caption:=Format('%7.2f',[AHeading],FS);
         end;

         if not (CBLLAMean.Checked and RBRdLLA.Checked and FGetLLA) then SBRdLLA.Enabled:=True;

         if not (CBZMean.Checked and RBRdZ.Checked) and FGetZ then begin
            EZ.Text:=Format('%7.2f',[AHeading],FS);

            FRotZ:=AHeading;
            FGetZ:=False;
            SBRdZ.Enabled:=True;
            Exit;
         end;
         if CBZMean.Checked and RBRdZ.Checked and FGetZ then begin
            if FZMean then begin
              EZ.Text:=Format('%7.2f',[FRotZ],FS);
              FZMean:=False;
              FGetZ:=False;
              SBRdZ.Enabled:=True;
              Exit;
            end;
            if FZCnt = 1 then FRotZ:=0.0;
            if FZCnt > MEAN_DELAY then begin
               FZMean:=True;
               FZCnt:=1;
               FRotZ:=FRotZ/(1.0*MEAN_DELAY);
               Exit;
            end;
            FRotZ:=FRotZ + AHeading;
            Inc(FZCnt);
          end;
        end;
        //else Exit;
      end;
    end;
    {}
    exsNoAnswered,
    exsDisconnected:begin
      LUnfOn.Caption:=' ���� '; LUnfOn.Color:=clSilver; LUnfOn.Font.Color:=clBlack;
      LGPSStar.Caption:='0';
      //
      RBRdZ.Enabled:=False;
      if not RBZManual.Checked then RBZManual.Toggle;
      if RBRdZ.Checked then RBRdZ.Toggle;
      CBZMean.Enabled:=False; CBZMean.Checked:=True;
      SBRdZ.Enabled:=False;
      LZ.Caption:='';
      //
      RBRdLLA.Enabled:=False;
      if not RBLLAManual.Checked then RBLLAManual.Toggle;
      if RBRdLLA.Checked then RBRdLLA.Toggle;
      CBLLAMean.Enabled:=False; CBLLAMean.Checked:=True;
      SBRdLLA.Enabled:=False;
      LLat.Caption:=''; LLong.Caption:=''; LAlt.Caption:='';
      //
      LTime.Caption:='00:00:00';
      //
      if Assigned(OnUnfEvent) then OnUnfEvent;
    end;
    {}
  end;

end;

procedure TUnfForm.SetIlmDeviceName(const Value: string);
begin
  Ilm.Chnl.DeviceName:=Value;
  //Ilm.Connect();
end;

procedure TUnfForm.SetUnfDeviceName(const Value: string);
begin
  SPS550.Chnl.DeviceName:=Value;
end;

function TUnfForm.GetIlmDeviceName: string;
begin
  Result:=Ilm.Chnl.DeviceName;
end;

function TUnfForm.GetUnfDeviceName: string;
begin
   Result:=SPS550.Chnl.DeviceName;
end;

procedure TUnfForm.SetRotX(const Value: F64);
begin
  FRotX:=Value;
  EX.Text:=FloatToStr(Value,FS);
end;

procedure TUnfForm.SetRotY(const Value: F64);
begin
  FRotY:=Value;
  EY.Text:=FloatToStr(Value,FS);
end;

procedure TUnfForm.SetRotZ(const Value: F64);
begin
  FRotZ:=Value;
  EZ.Text:=FloatToStr(Value,FS);
end;

procedure TUnfForm.SBRdZClick(Sender: TObject);
begin
  case (Sender as TSpeedButton).Tag of
    // Heafing
    0:begin
      SBRdZ.Enabled:=False; FGetZ:=True;
    end;
    1:begin
      if RBZMAnual.Checked then FRotZ:=StrToFloat(EZ.Text);
      if Assigned(OnUnfSetup) then OnUnfSetup((Sender as TSpeedButton).Tag);
    end;
    // LLA
    2:begin
      SBRdLLA.Enabled:=False; FGetLLA:=True;
    end;
    3:begin
      if RBLLAMAnual.Checked then begin
        FLat:=StrToFloat(ELat.Text);
        FLong:=StrToFloat(ELong.Text);
        FAlt:=StrToFloat(EAlt.Text);
      end;
      if Assigned(OnUnfSetup) then OnUnfSetup((Sender as TSpeedButton).Tag);
    end;
    // Time
    4:begin
      FTimeZone:=StrToInt(ETZ.Text);
      if Assigned(OnUnfSetup) then OnUnfSetup((Sender as TSpeedButton).Tag);
    end;
    // ILM
    10:begin
      SBRdXY.Enabled:=False; FGetXY:=True;
    end;
    11:begin
      if RBXYManual.Checked then begin
        FRotX:=StrToFloat(EX.Text);
        FRotY:=StrToFloat(EY.Text);
      end;  
      if Assigned(OnIlmSetup) then OnIlmSetup(0);
    end;
  end;
end;

procedure TUnfForm.RBZManualClick(Sender: TObject);
begin
  case (Sender as TRadioButton).Tag of
    0:begin
      RBRdZ.Enabled:=True;
      CBZMean.Enabled:=False; CBZMean.Checked:=True;
      EZ.Enabled:=True;
      SBRdZ.Enabled:=False;
    end;
    1:begin
      CBZMean.Enabled:=True; CBZMean.Checked:=True;
      //EZ.Enabled:=False;
      SBRdZ.Enabled:=True;
    end;
    10:begin
      CBXYMean.Enabled:=False; CBXYMean.Checked:=True;
      EX.Enabled:=True; EY.Enabled:=True;
      SBRdXY.Enabled:=False;
      FGetXY:=False;
    end;
    11:begin
      SBRdXY.Enabled:=True;
      //EX.Enabled:=False; EY.Enabled:=False;
      CBXYMean.Enabled:=True;
    end;  
  end;
end;

procedure TUnfForm.SetTimeZone(const Value: U8);
begin
  FTimeZone:=Value;
  ETZ.Text:=IntToStr(Value);
end;

procedure TUnfForm.SetAlt(const Value: F64);
begin
  FAlt:=Value;
  EAlt.Text:=FloatToStr(Value,FS);
end;

procedure TUnfForm.SetLat(const Value: F64);
begin
  FLat:=Value;
  ELat.Text:=FloatToStr(Value,FS);
end;

procedure TUnfForm.SetLong(const Value: F64);
begin
  FLong:=Value;
  ELong.Text:=FloatToStr(Value,FS);
end;

procedure TUnfForm.SetRotXAdjustment(const Value:F64);
begin
  FRotXAdjustment:=Value;
end;

procedure TUnfForm.SetRotYAdjustment(const Value:F64);
begin
  FRotYAdjustment:=Value;
end;

procedure TUnfForm.SetRotZAdjustment(const Value:F64);
begin
  FRotZAdjustment:=Value;
end;

procedure TUnfForm.SetILMStat(const Value: TILMStat);
begin
  FILMStat := Value;
end;

procedure TUnfForm.SetUNFStat(const Value: TUNFStat);
begin
  FUNFStat := Value;
end;

end.
