unit UstView;

interface

uses
  SysUtils, Types, Classes, Variants, QTypes, QGraphics, QControls, QForms, 
  QDialogs, QStdCtrls, QExtCtrls, QGrids,
  Ust;

type
  TForm3 = class(TForm)
    Bevel1: TBevel;
    FUstList: TStringGrid;
    RGFormat: TRadioGroup;
    BLoadUst: TButton;
    FUstInfo: TLabel;
    procedure FormCreate(Sender: TObject);
  private
  public
    Ust:TUst;
    procedure ShowUst();
  end;

var
  Form3: TForm3;

implementation

{$R *.xfm}

const CRLF = #013#010;

procedure TForm3.FormCreate(Sender: TObject);
begin
  //Ust:=TUst.Create(Self);
  //FUstDir:='D:\UST';
  //FUstDir:=ExtractFilePath(Application.ExeName) + 'UST\';

  FUstInfo.Caption:=
  '���� ������������    : ' + CRLF +
  '������������ ���     : ' + CRLF +
  '������ ������        : ' + CRLF +
  '�����������          : ' + CRLF +
  '��������� ������     : ' + CRLF +
  '������������ ������  : ' + CRLF +
  '��������� ���        : ' + CRLF +
  '��� �����            : ' + CRLF +
  '�����������          : ' + CRLF +
  '��� ����������       : ' + CRLF +
  '����������� ��=0     : ';

  FUstList.Cells[0,0]:='� �����';
  FUstList.Cells[1,0]:='�����';
  FUstList.Cells[2,0]:='������';
  FUstList.Cells[3,0]:='���� �����';
end;

procedure TForm3.ShowUst;
begin
  case RGFormat.ItemIndex of
    0:Format:=fDetail;
    1:Format:=fSimple;
  end;

  case Ust.Dir of
     CW: TrkDirStr:='������ (CW)';
    CCW: TrkDirStr:='����� (CCW)';
  end;

  case Ust.TrkType of
    trkHigh: TrkTypeStr:='�������';
    trkLow : TrkTypeStr:='������';
  end;

  if Ust.AzZeroPassed then AzZeroStr:='����'
                      else AzZeroStr:='���';

  TimeStepStr:=IntToStr(Ust.TimeStep div 1000) + ' ���  ';
  if (Ust.TimeStep mod 1000) <> 0 then TimeStepStr:=TimeStepStr + IntToStr(Ust.TimeStep mod 1000) + ' ����';
  {*
  FUstInfo.Caption:=
  '���� ������������    : ' + ExtractFileName(UstFileName) + CRLF +
  '������������ ���     : ' + Ust.SatName + CRLF +
  '������ ������        : ' + DateTimeToStr(Ust.StartTime) + ' ' +
                              AngDegToStr(Ust.Az[1],Format) + ' ' +
                              AngDegToStr(Ust.El[1],Format) + CRLF +
  '�����������          : ' + DateTimeToStr(Ust.CulmiTime) + ' '+
                              AngDegToStr(Ust.Az[Ust.CulmiPnt],Format) + ' ' +
                              AngDegToStr(Ust.El[Ust.CulmiPnt],Format) + CRLF +
  '��������� ������     : ' + DateTimeToStr(Ust.StopTime)  + ' ' +
                              AngDegToStr(Ust.Az[Ust.NumOfPnts],Format) + ' ' +
                              AngDegToStr(Ust.El[Ust.NumOfPnts],Format) + CRLF +
  '������������ ������  : ' + TimeToStr(Ust.TrackingTime) + CRLF +
  '��������� ���        : ' + TimeStepStr +  CRLF +
  '��� �����            : ' + InttoStr(Ust.NumOfPnts) + CRLF +
  '�����������          : ' + TrkDirStr + CRLF +
  '��� ����������       : ' + TrkTypeStr + CRLF +
  '����������� ��=0     : ' + AzZeroStr;
   *}

  FUstInfo.Caption:=
  '���� ������������ : ' + ExtractFileName(UstFileName) + CRLF +
  '������ ������     : ' + //DateTimeToStr(Ust.StartTime) + ' ' +
                              AngDegToStr(Ust.Az[1],Format) + ' ' +
                              AngDegToStr(Ust.El[1],Format) + CRLF +
  '�����������       : ' + //DateTimeToStr(Ust.CulmiTime) + ' '+
                              AngDegToStr(Ust.Az[Ust.CulmiPnt],Format) + ' ' +
                              AngDegToStr(Ust.El[Ust.CulmiPnt],Format) + CRLF +
  '��������� ������  : ' + //DateTimeToStr(Ust.StopTime)  + ' ' +
                              AngDegToStr(Ust.Az[Ust.NumOfPnts],Format) + ' ' +
                              AngDegToStr(Ust.El[Ust.NumOfPnts],Format) + CRLF +
  '�����������       : ' + TrkDirStr + CRLF +
  '��� ����������    : ' + TrkTypeStr + CRLF +
  '����������� ��=0  : ' + AzZeroStr;

  FUstList.RowCount:=Ust.NumOfPnts+1;
  for I:=1 to Ust.NumOfPnts do begin
    if I = Ust.CulmiPnt then FUstList.Cells[0,I]:='*' + IntToStr(I)
                        else FUstList.Cells[0,I]:=IntToStr(I);
    TrkTime:=IncMilliSecond(0.0,Ust.TimeStep*(I-1));
    FUstList.Cells[1,I]:=TimeToStr(TrkTime);
    FUstList.Cells[2,I]:=AngDegToStr(Ust.Az[I],Format);
    FUstList.Cells[3,I]:=AngDegToStr(Ust.El[I],Format);
  end;
  FUstList.Visible:=True;
end;

end.
