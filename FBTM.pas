unit FBTM;

interface

uses
  SysUtils, Types, Classes, Variants, QTypes, QGraphics, QControls, QForms, 
  QDialogs, QStdCtrls, Termo, RxAlias, RxUtils;

type TBTMStat = packed record
  ET:TExchangeState; // ��������� ������
  T1:S16;       // ����������� 1
  T2:S16;       // ����������� 2
  Load1:BOOL;   // �������� 1 ��������
  Load2:BOOL;   // �������� 2 ��������
  Alm1:BOOL;    // ����� �������� 1
  Alm2:BOOL;    // ����� �������� 2
end;

type TBTMEvent = procedure of object;

type
  TBtmForm = class(TForm)
    GroupBox1: TGroupBox;
    LOn: TLabel;
    LLoad1: TLabel;
    LLoad2: TLabel;
    LT1: TLabel;
    LT2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    FDeviceName:string;
    BTM:TTermoCtrl;
    FOnBTMEvent: TBTMEvent;
    FET: TExchangeState;
    FBTMStat: TBTMStat;
    function GetDeviceName: string;
    procedure SetDeviceName(const Value: string);
    procedure DataAvaliable();
    procedure SetBTMStat(const Value: TBTMStat);
  public
    function  Connect():BOOL;
    procedure Disconnect();
    //
    property BTMStat:TBTMStat read FBTMStat write SetBTMStat;
    //
    property ET:TExchangeState read FET;
    property DeviceName:string read GetDeviceName write SetDeviceName;
    property OnEvent:TBTMEvent read FOnBTMEvent write FOnBTMEvent default NIL;
  end;

var
  BtmForm: TBtmForm;

implementation

{$R *.xfm}

procedure TBtmForm.DataAvaliable;
begin
  FET:=BTM.ExState;
  if Assigned(OnEvent) then OnEvent;
  FBTMStat.ET:=BTM.ExState;
  case BTM.ExState of
    exsAnswer:begin
      LOn.Caption:=' ���  '; LOn.Color:=cllime; //LIlmOn.Font.Color:=clWhite;
      if BTM.Load1 then LLoad1.Color:=cllime else LLoad1.Color:=clSilver;
      FBTMStat.Load1:=BTM.Load1;
      if BTM.Load2 then LLoad2.Color:=cllime else LLoad2.Color:=clSilver;
      FBTMStat.Load2:=BTM.Load2;
      //LT1.Caption:=FormatFloat('0',BTM.T1);
      LT1.Caption:=IntToStr(BTM.T1);
      FBTMStat.T1:=BTM.T1;
      LT2.Caption:=IntToStr(BTM.T2);
      FBTMStat.T2:=BTM.T2;
      if BTM.Alm1 then LT1.Color:=clRed else LT1.Color:=clSilver;
      FBTMStat.Alm1 := BTM.Alm1;
      if BTM.Alm2 then LT2.Color:=clRed else LT2.Color:=clSilver;
      FBTMStat.Alm2 := BTM.Alm2;
    end;
    {}
    exsNoAnswered,
    exsDisconnected:begin
      LOn.Caption:=' ���� '; LOn.Color:=clSilver; LOn.Font.Color:=clBlack;
      LLoad1.Color:=clSilver;
      LLoad2.Color:=clSilver;
      LT1.Caption:='';
      LT2.Caption:='';
    end;
    {}
  end;
end;

function TBtmForm.Connect: BOOL;
begin
  BTM.Connect();
end;

procedure TBtmForm.Disconnect;
begin
  BTM.Disconnect();
end;

procedure TBtmForm.FormCreate(Sender: TObject);
begin
  BTM:=TTermoCtrl.Create(Self);
  BTM.Chnl.DeviceName:=FDeviceName;
  BTM.OnDataAvail:=DataAvaliable;
end;

function TBtmForm.GetDeviceName: string;
begin
  Result:=BTM.Chnl.DeviceName;
end;

procedure TBtmForm.SetDeviceName(const Value: string);
begin
 BTM.Chnl.DeviceName:=Value;
end;

procedure TBtmForm.SetBTMStat(const Value: TBTMStat);
begin
  FBTMStat := Value;
end;

end.
