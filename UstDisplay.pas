unit UstDisplay;

interface

uses
  SysUtils, Types, Classes, Variants, QTypes, QGraphics, QControls, QForms, 
  QDialogs, QStdCtrls, QComCtrls, QExtCtrls, Ust;

type
  TUstForm = class(TForm)
    pnlHeader: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lblFileName: TLabel;
    lblSatName: TLabel;
    lblPointsNumber: TLabel;
    lblDate: TLabel;
    lblTime: TLabel;
    lstPoints: TListView;
    btnLoad: TButton;
    btnCancel: TButton;
    GBRadar: TGroupBox;
    ImageRadar: TImage;
    procedure btnLoadClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Ust:TUst;

    function ReadFile(UstName:string):boolean;
  end;

var
  UstForm: TUstForm;

implementation

uses Main;

var
  Radar: TRadar;
{$R *.xfm}

function TUstForm.ReadFile(UstName:string):boolean;
var
  Hour, Min, Sec, MSec: Word;
  Start, Zenith, Finish: TListItem;
  I, Idx: Integer;
  bmp: TBitmap;
begin
  Ust.Clear();
  Result:=Ust.Read(UstName);
  if not Result then Exit;
  // set labels texts
  lblFileName.Caption:=UstName;
  lblSatName.Caption:=Ust.Hdr.SatName;
  lblPointsNumber.Caption:=IntToStr(Ust.Hdr.NumOfPnts);
  lblDate.Caption:=DateTimeToStr(Ust.Hdr.StartTime);
  Hour:=Ust.Hdr.NumOfPnts Div 3600;
  Min:=Ust.Hdr.NumOfPnts Mod 3600;
  Sec:=Min;
  Min:=Min Div 60;
  Sec:=Sec Mod 60;
  lblTime.Caption:=TimeToStr(EncodeTime(Hour, Min, Sec, 0));

  // fill interesting points table
  lstPoints.Items.Clear();
  Start:=lstPoints.Items.Add;
  Start.Caption:=String('������');
  Start.SubItems.Add(TimeToStr(Ust.Hdr.StartTime));
  Start.SubItems.Add(CurrToStr(Ust.Pnt[1].Az));
  Start.SubItems.Add(CurrToStr(Ust.Pnt[1].El));

  Idx:=0;
  For I:=2 To Ust.Hdr.NumOfPnts - 1 Do
  begin
    if Ust.Pnt[I].El > Ust.Pnt[Idx].El then Idx:=I;
  end;
  Zenith:=lstPoints.Items.Add;
  Zenith.Caption:='�����';
  DecodeTime(Ust.Hdr.StartTime, Hour, Min, Sec, MSec);
  Sec:=Sec + Idx;
  Min:=Min + (Sec Div 60);
  Sec:=Sec Mod 60;
  Hour:=(Hour + (Min Div 60)) Mod 24;
  Min:=Min Mod 60;
  Zenith.SubItems.Add(TimeToStr(EncodeTime(Hour, Min, Sec, 0)));
  Zenith.SubItems.Add(CurrToStr(Ust.Pnt[Idx].Az));
  Zenith.SubItems.Add(CurrToStr(Ust.Pnt[Idx].El));

  Finish:=lstPoints.Items.Add;
  Finish.Caption:='�����';
  DecodeTime(Ust.Hdr.StartTime, Hour, Min, Sec, MSec);
  Sec:=Sec + Ust.Hdr.NumOfPnts;
  Min:=Min + (Sec Div 60);
  Sec:=Sec Mod 60;
  Hour:=(Hour + (Min Div 60)) Mod 24;
  Min:=Min Mod 60;
  Finish.SubItems.Add(TimeToStr(EncodeTime(Hour, Min, Sec, 0)));
  Finish.SubItems.Add(CurrToStr(Ust.Pnt[Ust.Hdr.NumOfPnts].Az));
  Finish.SubItems.Add(CurrToStr(Ust.Pnt[Ust.Hdr.NumOfPnts].El));

  // draw radar
  Radar.NumOfPnts:=UstForm.Ust.Hdr.NumOfPnts;
  for I:=1 to UstForm.Ust.Hdr.NumOfPnts do begin
    Radar.Az[I]:=(Ust.Pnt[I].Az);
    Radar.El[I]:=(Ust.Pnt[I].El);
  end;
  Radar.Track:=True;

  bmp:= Tbitmap.create;
  DrawRadar(bmp, @Radar);

  ImageRadar.Canvas.Draw(0,0,bmp);
  bmp.Free;
end;

procedure TUstForm.btnLoadClick(Sender: TObject);
begin
  Self.ModalResult:=mrOk;
  //Close;
end;

procedure TUstForm.FormCreate(Sender: TObject);
begin
  Ust:= TUst.Create(Self);
  
  Radar.AzRadar:=30;
  Radar.ElRadar:=15;
  Radar.Text:=True;
  Radar.ColorBack:=clBlack;
  Radar.ColorRadar:=clWhite;
  Radar.ColorTrack:=clYellow;
  Radar.ColorPoint:=clRed;
  Radar.SizeTrack:=3;
  Radar.SizePoint:=5;
  Radar.Width:=ImageRadar.Width;
  Radar.Height:=ImageRadar.Height;
end;

procedure TUstForm.FormDestroy(Sender: TObject);
begin
  Ust.Free();
end;

end.
