UNIT RxUtils;
INTERFACE uses SysUtils, RxAlias;

type TVersionInfo = packed record
    Major:U8;
    Minor:U8;
    Realese:U8;
    Build:U8;
end;

type
  TDeviceName = string;
  TExchangeState = ( exsDisconnected, exsRequest, exsAnswer, exsNoAnswer, exsNoAnswered );

  TCoordynateSystem = ( csWGS84, csEP90, csCS82, csCS95, csEP9002 );

type TRFPolarization = ( rfpHP, rfpVP, rfpRHP, rfpLHP );

type
  TDegStrFormat = ( fFloat, fDegMinSec );

const
  angSecPerMin = 60;
  angMinPerDeg = 60;
  angSecPerDeg = angMinPerDeg*angSecPerMin;

  MSecsPerHour = MinsPerHour*SecsPerMin*MSecsPerSec; 

function AngDegToSec(ADeg:F64):S32;
function AngSecToDeg(ASec:S32):F64;

function RadToAngSec(ADeg:F64):S32;
function AngSecToRad(ASec:S32):F64;

function AngDegToStr(ADeg:Double; const DegStrFormat:TDegStrFormat):string;
function AngSecToStr(ASec:S32; const DegStrFormat:TDegStrFormat):string;     

function VerInfoToStr(VI:TVersionInfo):string;
function StrToVerInfo(S:string):TVersionInfo;

function ComNumToDeviceName(ComNumber:U16):string;
function DeviceNameToComNum(DeviceName:TDeviceName):U16;

IMPLEMENTATION uses StrUtils;

function VerInfoToStr(VI:TVersionInfo):string;
begin
  Result:=Format('%d.%d.%d.%d',[VI.Major,VI.Minor,VI.Realese,VI.Build]);
  {**
  Result:=IntToStr(FVerInfo.Major) + '.' +
          IntToStr(FVerInfo.Minor) + '.' +
          IntToStr(FVerInfo.Realese) + '.' +
          IntToStr(FVerInfo.Build);
  {**}        
end;

function StrToVerInfo(S:string):TVersionInfo;
begin
end;

function ComNumToDeviceName(ComNumber:U16):string;
begin
  Result:='\\.\COM'+IntToStr(ComNumber);
end;

function DeviceNameToComNum(DeviceName:TDeviceName):U16;
begin
  Result:=StrToInt(MidStr(DeviceName,8,3));
end;

function AngDegToStr(ADeg:F64; const DegStrFormat:TDegStrFormat):string;
var S:string[3];
    ASec,AMinSec:S32;
    DDD,MM,SS:U16;
begin
  case DegStrFormat of
    fFloat:begin
      Result:=Format('%7.2f',[abs(ADeg)]);
      if ADeg < 0 then Result[1]:='-';
    end;
    fDegMinSec:begin
      ASec:=Round(ADeg*angSecPerDeg);
      DDD:=abs(ASec div angSecPerDeg);
      AMinSec:=ASec mod angSecPerDeg;
      MM:=AMinSec div angMinPerDeg;
      SS:=AMinSec mod angMinPerDeg;
      //
      Str(DDD:3,S);
      if (ADeg < 0) and (abs(ADeg) <> 0) then Result:='-' else Result:=' ';
      Result:=Result + Copy(S,1,3) + '�';
      Str(MM:2,S); Result:=Result + Copy(S,1,2) + '''';
      Str(SS:2,S); Result:=Result + Copy(S,1,2) + '"';
    end;
  end;
end;

function AngSecToStr(ASec:S32; const DegStrFormat:TDegStrFormat):string;
var S:string[3];
    DDD,MM,SS:U16;
    AMinSec:S32;
begin
  case DegStrFormat of
    fFloat:begin
    end;
    fDegMinSec:begin
      DDD:=ASec div angSecPerDeg;
      AMinSec:=ASec mod angSecPerDeg;
      MM:=AMinSec div angMinPerDeg;
      SS:=AMinSec mod angMinPerDeg;
      //
      Str(DDD:3,S);
      if (ASec < 0) and (abs(ASec) <> 0) then Result:='-' else Result:=' ';
      Result:=Result + Copy(S,1,3) + '�';
      Str(MM:2,S); Result:=Result + Copy(S,1,2) + '''';
      Str(SS:2,S); Result:=Result + Copy(S,1,2) + '"';
    end;
  end;
end;

function AngDegToSec(ADeg:F64):S32;
begin
  Result:=Round(ADeg*angSecPerDeg);
end;

function AngSecToDeg(ASec:S32):F64;
begin
  Result:=(ASec*1.0)/(angSecPerDeg*1.0);
end;

function AngSecToRad(ASec:S32):F64;
begin
end;

function RadToAngSec(ADeg:F64):S32;
begin
end;

//INITIALIZATION
//FINALIZATION
END.
