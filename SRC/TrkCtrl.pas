UNIT TrkCtrl;
INTERFACE uses SysUtils, Classes, QExtCtrls,
   Axis, ExProtcl, Ust, KMath, RxAlias, RxUtils,
   TrkCtrlInternal;

{$I States.pas}

type TTrkMode = ( tmNone, tmWait, tmPrg, tmSearch, tmAuto );
  TTrkCtrlDataAvail = procedure(Sender:TObject) of object;

type TTrkCtrlStat = packed record
  ET:TExchangeState; // ��������� ������
  Brk:BOOL;          // ��������� �������
  Pwr:BOOL;          // ��������� ������� ���
  Time:U32;          // ������� ����� �� � ����. �� ����� �����
  Mode:TTrkMode;     // ����� ������ ��
  SigLev:F32;        // ������� ������� � %
  Hold:BOOL;         // ������ �������
  Az,El:F32;         // ������� ��������� �� � ���  
  Axis:array[1..3] of TAxisStat; // ��������� �� ����
end;  

type
  TTrkCtrl = class(TComponent)
  private
    FTimer:TTimer;
    //FUst:TUst;
    OutPkt:TOutPacket;
    Inner:TTrkCtrlInternal;
    //
    FTime:U32;
    FSL:F64;
    FPwr,FBrk,FHold:BOOL;
    FTM:TTrkMode;
    FET:TExchangeState;
    //Mode:TTCMode;
    //
    //FNextPnt:U32;
    FNP:BOOL;
    FStartTime,CTime:U32;
    FAT,FAS:BOOL;
    FTrkType:TTrType;
    //K1,K2,K3:F64;
    FTracking:BOOL;
    //
    FOnDataAvail: TTrkCtrlDataAvail;
    FDeviceName: TDeviceName;
    FAz:F64;
    FEl:F64;
    FRotX,FRotY,FRotZ:F64;
    FAsyncCmd,FAxisAsyncCmd:BOOL;
    FLoadTrk:BOOL;
    LoadTrkState:U8;
    FAutoSearch,FAutoTrack:BOOL;
    FNumOfBlks,FNumOfBlk,FBlkSize,FPntToLoad:U16;
    FTrackLoading: BOOL;
    FLoadPercent: U16;
    FTracing:BOOL;
    FColFsm: Byte;
    FColCnt:Word;
    Dist:F64;
    IsConnect:BOOL;
    FTimeZone: U32;
    FTrkCtrlStat: TTrkCtrlStat;
    AnsCntr:U8;
    procedure FTimerTimer(Sender:TObject);
    function GetTime: TDateTime;
    procedure StopTrack();
    procedure SetRotX(const Value: F64);
    procedure SetRotY(const Value: F64);
    procedure SetRotZ(const Value: F64);
    function GetTimeZone: U32;
    procedure SetTimeZone(const Value: U32);
    procedure SetTrkLoLimit(const Value: F64);
    procedure SetTrkCtrlStat(const Value: TTrkCtrlStat);
  protected
  public
    Mode:TTCMode;
    FTrkTime:U32;
    K1,K2,K3:F64;
    FNextPnt:U16;
    //
    Axis:array[1..3] of TAxis;
    constructor Create(AOwner:TComponent; Tracing:BOOL); // override;
    destructor Destroy; override;
    procedure Connect;
    procedure Disconnect;
    procedure Stop();
    procedure SetTime(ATime:TDateTime);
    procedure SetCorr(AAz,AEl:F64; ATime:S32);
    procedure MoveBySpd(ASpd1,ASpd2,ASpd3:F64);
    procedure LoadTrack(USTName:string; ATrack,ASearch:BOOL; AStandAlone:BOOL);
    //
    procedure Park;
    procedure MoveToCollapsePos;
    procedure MoveToPosForShutterOpen;
    procedure MoveTo(Az,El:F64);
    procedure PowerOn();
    procedure PowerOff();
    //
    property TrkLoLimit:F64 write SetTrkLoLimit;
    //
    property TrkCtrlStat:TTrkCtrlStat read FTrkCtrlStat write SetTrkCtrlStat;
    //
    property Time:TDateTime read GetTime;
    property SL:F64 read FSL;
    property Pwr:BOOL read FPwr;
    property Brk:BOOL read FBrk;
    property Hold:BOOL  read FHold;
    property TM:TTrkMode read FTM;
    property ET:TExchangeState  read FET;
    property Az:F64 read FAz;
    property El:F64 read FEl;
    property TrackLoading:BOOL read FTrackLoading;
    property LoadPercent:U16 read FLoadPercent;
    property NumOfBlks:U16 read FNumOfBlks;
    property NumOfBlk:U16 read FNumOfBlk;
    property BlkSize:U16  read FBlkSize write FBlkSize;
    property RotX:F64 read FRotX write SetRotX;
    property RotY:F64 read FRotY write SetRotY;
    property RotZ:F64 read FRotZ write SetRotZ;
    property TimeZone:U32 read GetTimeZone write SetTimeZone;  
    property DeviceName:TDeviceName read FDeviceName write FDeviceName;
    property OnDataAvail:TTrkCtrlDataAvail read FOnDataAvail write FonDataAvail default NIL;
  end;

IMPLEMENTATION uses Math, DateUtils;

const
  ANSID_NOANSWER  = 0;
  ANSID_STAT   = 1;
  ANSID_LIMITS = 2;
  ANSID_PARAM  = 3;

var FP:U16 = 1;

procedure TTrkCtrl.FTimerTimer(Sender: TObject);
var
   AnsID,I:U8;
   A,E:F64;
   K:array[1..3] of F64;
   K11,K21,K31, K12,K22,K32:F64;
begin
  AnsID:=Inner.DecodePacket();
  FET:=exsNoAnswer;
  
  if (AnsID > ANSID_NOANSWER) then begin
    FET:=exsAnswer;
    //if AnsCnt <> 20 then Inc(AnsCntr);
    AnsCntr:=20;
    // �������� ������
    Inner.GetData(FTime,A,E,FSL,BOOL(FPwr),BOOL(FBrk),BOOL(FHold),BOOL(FNP),U8(FTM),FNextPnt);
    for I:=1 to 3 do begin Axis[I].Decode(AnsID); K[I]:=Axis[I].Pos end;
    KMath.KinematicToAE(K[1],K[2],K[3],FAz,FEl);
    // ������� �������������
    FTracking:=(FTM = tmPrg) or (FTM = tmSearch) or (FTM = tmAuto);
    //KMath.CK1RToCK1(FAz,FEl, FRotX,FRotY,FRotZ);

   for I:=1 to 3 do if Axis[I].AsyncCmd() then begin
    FAxisAsyncCmd:=True; Break;
   end;

   if not FAxisAsyncCmd then begin
    if FAsyncCmd then begin
      case OutPkt.Cmd of
        CMD_SETTIME:Inner.SetTime(OutPkt.SetTime.Time); // ���������� �������� �����
        CMD_STOP:Inner.Stop();
        CMD_SETCORR:Inner.SetCorrection(OutPkt.SetCorr.Az,OutPkt.SetCorr.El,OutPkt.SetCorr.Time);
        CMD_MOVEBYSPD:Inner.MoveBy3Speed(OutPkt.MoveBySpd.Spd[1],OutPkt.MoveBySpd.Spd[2],OutPkt.MoveBySpd.Spd[3]); // �������� �� ���� ���������
        CMD_POWERON:Inner.PowerOn();
        CMD_POWEROFF:Inner.PowerOff();
      end;
    end else begin

        case Mode of
          TC_IDLE:begin
            Inner.GetStat();
            FTrackLoading:=False;
          end;
          // ���� ����� ������ � ��������
          TC_WAIT_INITPNT:begin
            if Axis[1].INP and Axis[2].INP and Axis[3].INP then begin // ��� � ��������
              GetFstPnt(FP+1,K11,K21,K31); // �������� ������ ����� ����������
              GetFstPnt(FP+2,K12,K22,K32); // �������� ������ ����� ����������
              // ��������� ���������
              Inner.LoadTrkHdr(BOOL(FAT),BOOL(FAS), U8(FTrkType), FStartTime, K1,K2,K3, K11,K21,K31, K12,K22,K32, FP+2);
              Mode:=TC_WAIT_STARTTIME; // ������� �� �������� ������� ������
            end else Inner.GetStat(); // ���� �� � �������� ��������� ������
          end;
          // ���� ����� �� ������ �������������
          TC_WAIT_STARTTIME:begin
            if FTracking then begin // �������� ������� �������������
              FTrackLoading:=True;
              Mode:=TC_TRACKING; // ��������� � ������������� 
            end;
            Inner.GetStat(); // ���� ��� �������� ��������� ������
          end;
          // ������������ �� ������
          TC_TRACKING:begin
            if FNP then begin // ������� ������������ �������� ��������� �����
              // ������� ����� �� ������ FNextPnt �� ��
              if GetPnt(FNextPnt, K1,K2,K3) then begin
                // ���� ����� ��������� ����� FNextPnt < NumOfPnts
                Inner.LoadTrkPnt(K1,K2,K3); // ��������� ��������� �����
                Inc(FTrkTime,1);
              end else begin
                StopTrack(); // ����� ��������� ����������
              end;
              FNP:=False; 
            end else Inner.GetStat();
          end;

          TC_LOADTRKHDR:begin
            //FP:=2; GetFstPnt(1,K1,K2,K3);
            GetFstPnt(FP,K1,K2,K3);
            Inner.LoadTrkHdrToFile('track', BOOL(FAT), BOOL(FAS), U8(FTrkType), FStartTime, KMath.PointsCount, K1,K2,K3);
            Mode:=TC_LOADTRK;
          end;          

          TC_LOADTRK:begin
              if GetPnt(FP, K1,K2,K3) then begin
                Inc(FNumOfBlk); FLoadPercent:=NumOfBlk*100 div NumOfBlks;
                Inner.LoadTrkPntToFile(FP, K1,K2,K3);
                Inc(FP);
              end else begin
                Inner.LoadTrkEndToFile();
                FTrackLoading:=False;
                FLoadPercent:=100;
                Mode:=TC_IDLE;
              end;
          end;
        end;

    end;
   end;

   FAsyncCmd:=False;
   FAxisAsyncCmd:=False;

  end else begin
    Inner.SendLastPacket();
    if AnsCntr <> 0 then Dec(AnsCntr);
    if AnsCntr = 0 then FET:=exsNoAnswered;
  end;  

  if Assigned(OnDataAvail) then OnDataAvail(Self);

  FTrkCtrlStat.ET:=FET;
  FTrkCtrlStat.Time:=FTime;
  FTrkCtrlStat.Brk:=FBrk;
  FTrkCtrlStat.Pwr:=FPwr;
  FTrkCtrlStat.Mode:=FTM;
  FTrkCtrlStat.SigLev:=FSL;
  FTrkCtrlStat.Hold:=FHold;
  FTrkCtrlStat.Az:=FAz;
  FTrkCtrlStat.El:=FEl;
  for I:=1 to 3 do FTrkCtrlStat.Axis[I]:=Axis[I].AxisStat;

end;

procedure TTrkCtrl.LoadTrack(USTName:string; ATrack,ASearch:BOOL; AStandAlone:BOOL);
var I:U16; CTime:U32;
begin
  FAT:=ATrack; FAS:=ASearch;
  KMath.AEToKinematic(USTName); // ������������� v������ [Az,El] --> [K1,K2,K3]
  CTime:=FTime;
  FStartTime:=KMath.StartTime;
  FTrkType:=KMath.TrType;

  TrimTrack(Axis[1].Pos,Axis[2].Pos,Axis[3].Pos,FStartTime,CTime + 3*MSecsPerSec, FStartTime,FP);

  GetFstPnt(FP,K1,K2,K3);
  // ��������� ���1 2 3 � ��������
  Axis[1].MoveTo(K1,3.0);
  Axis[2].MoveTo(K2,6.0);
  Axis[3].MoveTo(K3,6.0);

  if AStandAlone then begin
    //KMath.PointsCount:=100;
    FLoadPercent:=0; FTrackLoading:=True; FNumOfBlk:=1; FNumOfBlks:=KMath.PointsCount;
    Mode:=TC_LOADTRKHDR;
    Exit;
  end;

  Mode:=TC_WAIT_INITPNT; // ������� �� �������� �������� ���������
end;

procedure TTrkCtrl.Stop;
begin
  FLoadTrk:=False;
  LoadTrkState:= 0;
  FTrackLoading:=False;
  FLoadPercent:=0;
  OutPkt.Cmd:=CMD_STOP;
  FAsyncCmd:=True;
  Mode:=TC_IDLE;
end;

procedure TTrkCtrl.StopTrack;
begin
  FLoadTrk:=False;
  LoadTrkState:= 0;
  FTrackLoading:=False;
  FLoadPercent:=0;
  Inner.Stop();
  Mode:=TC_IDLE;
end;

constructor TTrkCtrl.Create(AOwner: TComponent; Tracing:BOOL);
var I:Byte;
begin
  inherited Create(AOwner);
  Inner:=TTrkCtrlInternal.Create();
  FET:=exsDisconnected;

  for I:=1 to 3 do begin Axis[I]:=TAxis.Create(I, Inner); end;

  FTimer:=TTimer.Create(Self);
  with FTimer do begin
    Enabled:=False;
    Interval:=50;
    OnTimer:=FTimerTimer;
  end;

  FTracing:=Tracing;
  Dist:=100.0;
  FColFsm:=0;
  AnsCntr:=20;

  Mode:=TC_IDLE;
end;

procedure TTrkCtrl.Connect;
begin
  IsConnect:=False;
  if FDeviceName = '' then Exit;
  Inner.DeviceName := FDeviceName;
  Inner.Baud := 115200;
  IsConnect:=True;
  //if FTracing then TracingOn;
  Inner.Connect();
  Inner.GetStat();
  FTimer.Enabled:=True;
end;

procedure TTrkCtrl.Disconnect;
begin
  FET:=exsDisconnected;
  if not IsConnect then Exit;
  FTimer.Enabled:=False;
  Inner.Disconnect();
end;

destructor TTrkCtrl.Destroy;
begin
  //FUst.Free;
  inherited Destroy;
end;


procedure TTrkCtrl.SetCorr(AAz, AEl: F64; ATime: S32);
begin
  OutPkt.Cmd:=CMD_SETCORR;
  OutPkt.SetCorr.Az:=AAz;
  OutPkt.SetCorr.El:=AEl;
  OutPkt.SetCorr.Time:=ATime;
  FAsyncCmd:=True;
end;

procedure TTrkCtrl.SetTime(ATime: TDateTime);
begin
  OutPkt.Cmd:=CMD_SETTIME;
  OutPkt.SetTime.Time:=ATime;
  FAsyncCmd:=True;
end;

procedure TTrkCtrl.MoveBySpd(ASpd1, ASpd2, ASpd3: F64);
begin
  OutPkt.Cmd:=CMD_MOVEBYSPD;
  OutPkt.MoveBySpd.Spd[1]:=ASpd1;
  OutPkt.MoveBySpd.Spd[2]:=ASpd2;
  OutPkt.MoveBySpd.Spd[3]:=ASpd3;
  FAsyncCmd:=True;
end;

procedure TTrkCtrl.PowerOff;
begin
  OutPkt.Cmd:=CMD_POWEROFF;
  FAsyncCmd:=True;
end;

procedure TTrkCtrl.PowerOn;
begin
  OutPkt.Cmd:=CMD_POWERON;
  FAsyncCmd:=True;
end;

procedure TTrkCtrl.MoveToCollapsePos;
begin
  Axis[1].MoveTo(Axis[1].CP,1.0);
  Axis[2].MoveTo(Axis[2].CP,3.0);
  Axis[3].MoveTo(Axis[3].CP,3.0);
  //Axis[1].MoveTo(0.0,1.0);
  //Axis[2].MoveTo(0.0,3.0);
  //FColFsm:=1; FColCnt:=100;
end;

procedure TTrkCtrl.MoveToPosForShutterOpen;
begin
{
  Axis[1].MoveTo(0.0,1.0);
  Axis[2].MoveTo(0.0,3.0);
  FColFsm:=10; FColCnt:=100;
}
end;

procedure TTrkCtrl.Park;
begin
  Axis[1].MoveTo(Axis[1].CP1,1.0);
  Axis[2].MoveTo(Axis[2].CP1,3.0);
  Axis[3].MoveTo(Axis[3].CP1,3.0);
  //FColFsm:=20; FColCnt:=100;
end;

procedure TTrkCtrl.MoveTo(Az,El: F64);
var A,E:F64;
begin
  Axis[1].MoveTo(0.0,1.0);
  A:=Az; E:=El;
  //CK1RToCK1(A,E, FRotX,FRotY,FRotZ);
  E:=E-90.0;
  if A > 270.0 then A:=A-360.0;
  Axis[2].MoveTo(E,3.0);
  Axis[3].MoveTo(A,3.0);
end;



function TTrkCtrl.GetTime:TDateTime;
begin
  Result:=IncMilliSecond(Date(),FTime);  
end;

procedure TTrkCtrl.SetRotX(const Value: F64);
begin
  FRotX:=Value;
  KMath.SetRotX(Value);
end;

procedure TTrkCtrl.SetRotY(const Value: F64);
begin
  FRotY:=Value;
  KMath.SetRotY(Value);
end;

procedure TTrkCtrl.SetRotZ(const Value: F64);
begin
  FRotZ:=Value;
  KMath.SetRotZ(Value);
end;

function TTrkCtrl.GetTimeZone: U32;
begin
  Result:=FTimeZone div MSecsPerHour; 
end;

procedure TTrkCtrl.SetTimeZone(const Value: U32);
begin
  FTimeZone:=Value*MSecsPerHour;
  KMath.SetTimeZone(FTimeZone);
end;

procedure TTrkCtrl.SetTrkLoLimit(const Value: F64);
begin
  KMath.TrakLoLimit:=Value;
end;

procedure TTrkCtrl.SetTrkCtrlStat(const Value: TTrkCtrlStat);
begin
  FTrkCtrlStat:=Value;
end;

END.
