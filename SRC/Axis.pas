UNIT Axis;

INTERFACE uses SysUtils, Classes, ExProtcl, RxAlias, RxUtils, TrkCtrlInternal,
ExProtclInternal;

const StopedConst = 1.0;

type TAxisStat = packed record
  Pos:F32;   // ������� ��������� ��. ����.
  Spd:F32;   // ������� �������� ��. ����. � ���.
  LSN:BOOL;  // ���������� ��-
  LSP:BOOL;  // ���������� ��+
  PLSN:BOOL; // ���������� ���-
  PLSP:BOOL; // ���������� ���+
  //
  PLSNPos:F32; // ��������� ���-
  PLSPPos:F32; // ��������� ���+
end;

type TAxis = class(TPersistent)
  private
    Control:TTrkCtrlInternal;
    ANum:Integer;
    FAsyncCmd:BOOL;
    FSpd: F64;
    FPos: F64;
    FPlsnPos,FPlspPos,FZPPos:F64;
    FRdy,FAlm:BOOL;
    FLimits:BOOL;
    FLimStat:array[1..4] of BOOL;
    FStoped:BOOL;
    FOldPos:F64;
    FStopedCnt:U32;
    FCP: F64;
    FCP1: F64;
    FINP:BOOL;
    FAxisStat: TAxisStat;
    function GetLimStat(I: U8): BOOL;
    procedure SetAxisStat(const Value: TAxisStat);
    //
  protected
  public
    OutPkt:TOutPacket;
    procedure Decode(AnsID:U8);
    constructor Create(Num:Byte; Ctrl:TTrkCtrlInternal);
    procedure MoveTo(APos,ASpd:F64);
    procedure RestoreZP();
    procedure SaveZP(ADir:S8);
    procedure GetLimits();
    procedure SetLimits(ALSP, ALSN: F64);
    procedure SetPos(APos:F64);

    function AsyncCmd:BOOL;
    property Pos:F64 read FPos;
    property Spd:F64 read FSpd;
    property PlsnPos:F64 read FPlsnPos;
    property PlspPos:F64 read FPlspPos;
    property ZPPos:F64 read FZPPos;
    property Rdy:BOOL read FRdy;
    property Alm:BOOL read FAlm;
    {
    property Hlsn:BOOL read FHlsn;
    property Hlsp:BOOL read FHlsp;
    property Plsn:BOOL read FPlsn;
    property Plsp:BOOL read FPlsp;
    }
    //
    property AxisStat:TAxisStat read FAxisStat write SetAxisStat;
    //
    property LimStat[I:U8]:BOOL read GetLimStat;
    property Limits:BOOL read FLimits;
    property Stoped:BOOL read FStoped;
    property INP:BOOL read FINP;
    property CP:F64 read FCP write FCP;
    property CP1:F64 read FCP1 write FCP1;
  end;

IMPLEMENTATION

function TAxis.AsyncCmd: BOOL;
begin
  Result:=False;
  if not FAsyncCmd then Exit;
  Result:=True;
  FAsyncCmd:=False;
  case OutPkt.Cmd of
    CMD_MOVETO:Control.Axis[Anum].MoveTo(OutPkt.MoveTo.Pos,OutPkt.MoveTo.Spd);
    CMD_RESTOREZP:Control.Axis[Anum].RestoreZP; // ������������ ��
    CMD_SAVEZP:Control.Axis[Anum].SaveZP(OutPkt.SaveZP.Dir); // ������������ ��
    CMD_SETPOS:Control.Axis[Anum].SetPos(OutPkt.SetPos.Pos); // ���������� ���������
    CMD_GETLIMITS:Control.Axis[Anum].GetLimits;   // ��������� ���������� -���,���+
    CMD_SETLIMITS:Control.Axis[Anum].SetLimits(OutPkt.SetLimits.LSP,OutPkt.SetLimits.LSN);//
  end;
end;

constructor TAxis.Create(Num:Byte; Ctrl:TTrkCtrlInternal);
begin
  ANum:=Num;
  Control:=Ctrl;
end;

procedure TAxis.Decode(AnsID:U8);
begin
  Control.Axis[ANum].GetData(ANum,FPos,FSpd,FPlsnPos,FPlspPos,FZPPos,
    FLimStat[1],FLimStat[2],FLimStat[3],FLimStat[4],FRdy,FAlm,FINP);
  FLimits:=False;
  if (AnsID = ANS_LIMITS) then FLimits:=True;

  if abs(abs(FPos) - abs(FOldPos)) <  StopedConst then begin
    Inc(FStopedCnt);
    if FStopedCnt = 20*3 then FStoped:=True;
  end else begin
     FStoped:=False;
     FStopedCnt:=0;
  end;
  FOldPos:=FPos;

  FAxisStat.Pos:=FPos;
  FAxisStat.Spd:=FSpd;
  FAxisStat.LSN:=FLimStat[1];
  FAxisStat.PLSN:=FLimStat[2];
  FAxisStat.PLSN:=FLimStat[3];
  FAxisStat.LSP:=FLimStat[4];
  FAxisStat.PLSNPos:=FPlsnPos;
  FAxisStat.PLSPPos:=FPlspPos;
end;

function TAxis.GetLimStat(I: U8): BOOL;
begin
  Result:=FLimStat[I];
end;

procedure TAxis.MoveTo(APos, ASpd: F64);
begin
  OutPkt.Cmd:=CMD_MOVETO;
  OutPkt.MoveTo.Pos:=APos;
  OutPkt.MoveTo.Spd:=ASpd;
  FAsyncCmd:=True;
  FStoped:=False;
  FOldPos:=FPos + 2.0;
end;

procedure TAxis.GetLimits;
begin
  OutPkt.Cmd:=CMD_GETLIMITS;
  FAsyncCmd:=True;
end;

procedure TAxis.RestoreZP;
begin
  OutPkt.Cmd:=CMD_RESTOREZP;
  FAsyncCmd:=True;
end;

procedure TAxis.SaveZP(ADir: S8);
begin
  OutPkt.Cmd:=CMD_SAVEZP;
  OutPkt.SaveZP.Dir:=ADir;
  FAsyncCmd:=True;
end;

procedure TAxis.SetLimits(ALSP, ALSN: F64);
begin
  OutPkt.Cmd:=CMD_SETLIMITS;
  OutPkt.SetLimits.LSP:=ALSP;
  OutPkt.SetLimits.LSN:=ALSN;
  FAsyncCmd:=True;
end;

procedure TAxis.SetPos(APos: F64);
begin
  OutPkt.Cmd:=CMD_SETPOS;
  OutPkt.SetPos.Pos:=APos;
  FAsyncCmd:=True;
end;

procedure TAxis.SetAxisStat(const Value: TAxisStat);
begin
  FAxisStat:=Value;
end;

END.


