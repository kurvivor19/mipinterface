UNIT RxAlias;
INTERFACE

type
  U8  = Byte;
  U16 = Word;
  U32 = LongWord;

type
  S8  = ShortInt;
  S16 = SmallInt;
  S32 = LongInt;

type
  F32 = Single;
  F64 = Double;
  F80 = Extended;

type
  BOOL = Boolean;
  B8   = ByteBool;
  B16  = WordBool;
  B32  = LongBool;

type
  PTR = Pointer;

type
  C8  = AnsiChar;
  C16 = WideChar;

const
  SPC  = #032; //  $20
  CR   = #013; //  $0D  ^M
  LF   = #010; //  $0A  ^J
  CRLF = CR + LF;    

IMPLEMENTATION
END.
 