UNIT ExProtcl2;
INTERFACE uses RxAlias;

type TFstPnt = packed record
  Pnt:array[1..3] of F32;
end;

// Command
type TLoadTrackHeader = packed record
  Cmd:U8;
  AutoTrk:U8;
  StartTime:U32;
  NumOfPnts:U16;
  Az,El:F32;
end;

const
  ATS_AT = $01;
  ATS_AS = $02;

type TLoadTrackPoint = packed record
  Cmd:U8;
  NumOfPnt:U16;
  Az,El:F32;
end;

type TLoadTrackEnd = packed record
  Cmd:U8;
end;

type TLoadTrackHdr = packed record
  Cmd:U8;
  AutoTrk:U8;
  TrkType:U8;
  StartTime:U32;
  K1,K2,K3:TFstPnt;
  FstPnt:U16;
end;

type TLoadTrackPnt = packed record
  Cmd:U8;
  K1,K2,K3:F32;
end;

type TLoadTrkHdrToFile = packed record
  Cmd:U8;
  //FileName:array[1..7] of Char;
  AutoTrk:U8;
  TrkType:U8;
  StartTime:U32;
  NumOfPnts:U16;
  K1,K2,K3:F32;
end;

type TLoadTrkPntToFile = packed record
  Cmd:U8;
  NumOfPnt:U16;
  K1,K2,K3:F32;
end;

type TLoadTrkEndToFile = packed record
  Cmd:U8;
end;

type TPwrOnOff = packed record
  Cmd:U8;
end;

type TSetCorrection = packed record
  Cmd:U8;
  Az,El:F32;
  Time:S32;
end;

type TGetStat = packed record
  Cmd:U8;
end;

type TStop = packed record
  Cmd:U8;
end;

type TMoveBy3Speed = packed record
  Cmd:U8;
  Spd:array[1..3] of F32;
end;

type TSetTime = packed record
  Cmd:U8;
  Time:U32;
end;

type TAxisGetLimits = packed record
  Cmd:U8;
end;

type TAxisSetLimits = packed record
  Cmd:U8;
  PlsnPos:F32;
  PlspPos:F32;
end;

type TAxisRestoreZP = packed record
  Cmd:U8;
end;

type TAxisSaveZP = packed record
  Cmd:U8;
  Dir:S32;
end;

type TAxisSetPos = packed record
  Cmd:U8;
  Pos:F32;
end;

type TAxisMoveTo = packed record
  Cmd:U8;
  Pos:F32;
  Spd:F32;
end;

type TParamVar = packed record
  case U8 of
    0:(B0,B1,B2,B3:U8;);
    1:(Lo16,Hi16:U16;);
    2:(Long:S32;);
    3:(LongW:U32;);
    4:(Float32:F32;);
end;

type TGetParam = packed record
  Cmd:U8;
  DevId:U8;
  Id:U8;
end;

type TSetParam = packed record
  Cmd:U8;
  DevId:U8;
  Id:U8;
  Param:TParamVar;
end;

type TSaveParam = packed record
  Cmd:U8;
end;

const
  PARAM_AXISZP      = 0;

const
  CMD_GETSTAT       = 0;
  CMD_SETTIME       = 1;
  CMD_STOP       = 2;
  CMD_MOVEBY3SPEED  = 3;
  CMD_POWERON       = 10;
  CMD_POWEROFF      = 11;  
  CMD_GETPARAM      = 20;
  CMD_SETPARAM      = 21;
  CMD_SAVEPARAM     = 22;


  CMD_AELOADTRKHDR    = 230;
  CMD_AELOADTRKPNT    = 231;
  CMD_AELOADTRKEND    = 232;
  CMD_SETCORRECTION = 245;

  CMD_LOADTRKHDRTOFILE = 240;
  CMD_LOADTRKPNTTOFILE = 241;
  CMD_LOADTRKENDTOFILE = 242;  

  CMD_LOADTRKHDR    = 250;
  CMD_LOADTRKPNT    = 251;

  CMD_AXISMOVETO    = 100;
  CMD_AXISGETLIMITS = 110;
  CMD_AXISSETLIMITS = 120;
  CMD_AXISSETPOS    = 130;
  CMD_AXISSAVEZP    = 140;
  CMD_AXISRESTOREZP = 150;


type TOPacket = packed record
  case U8 of
    // Command
    $7E:(Cmd:Byte;);
    CMD_GETSTAT:      (GetStat:TGetStat;);
    CMD_SETTIME:      (SetTime:TSetTime;);
    CMD_STOP:      (Stop:TStop;);
    CMD_GETPARAM:     (GetParam:TGetParam;);
    CMD_SETPARAM:     (SetParam:TSetParam;);
    CMD_SAVEPARAM:    (SaveParam:TSaveParam;);
    CMD_MOVEBY3SPEED: (MoveBy3Speed:TMoveBy3Speed;);

    CMD_AELOADTRKHDR:   (LoadTrackHeader:TLoadTrackHeader;);
    CMD_AELOADTRKPNT:   (LoadTrackPoint:TLoadTrackPoint;);
    CMD_AELOADTRKEND:   (LoadTrackEnd:TLoadTrackEnd;);
    CMD_SETCORRECTION:(SetCorrection:TSetCorrection;);

    CMD_LOADTRKHDR:   (LoadTrackHdr:TLoadTrackHdr;);
    CMD_LOADTRKPNT:   (LoadTrackPnt:TLoadTrackPnt;);

    CMD_LOADTRKHDRTOFILE: (LoadTrkHdrToFile:TLoadTrkHdrToFile;);
    CMD_LOADTRKPNTTOFILE: (LoadTrkPntToFile:TLoadTrkPntToFile;);
    CMD_LOADTRKENDTOFILE: (LoadTrkEndToFile:TLoadTrkEndToFile;);        

    CMD_AXISMOVETO:   (AxisMoveTo:TAxisMoveTo;);
    CMD_AXISGETLIMITS:(AxisGetLimits:TAxisGetLimits;);
    CMD_AXISSETLIMITS:(AxisSetLimits:TAxisSetLimits;);
    CMD_AXISSETPOS:   (AxisSetPos:TAxisSetPos;);
    CMD_AXISSAVEZP:   (AxisSaveZP:TAxisSaveZP;);
    CMD_AXISRESTOREZP:(AxisRestoreZP:TAxisRestoreZP;);
    CMD_POWERON:(PwrOnOff:TPwrOnOff;);
end;

// Answer
type TTrkCtlStat = packed record
  Time:U32;
  Stat:U16;
  SigLev:F32;
  NextPnt:U16;
end;

const
  TRK_MASK   = $0004;
  TM_MASK    = $000F;
  TR_MASK    = $0001;
  AS_MASK    = $0002;
  AT_MASK    = $0003;
  HOLD_MASK  = $0010;
  PWR_MASK   = $0020;
  BRK_MASK   = $0040;
  NEXT_PNT   = $1000;
  INP_MASK  = $4000;

type TAxisStat = packed record
  Stat:U16;
  Pos:F32;
  Spd:F32;
end;

const
  HLSN_MASK = $0080;
  PLSN_MASK = $0010;
  PLSP_MASK = $0008;
  HLSP_MASK = $0001;
  RDY_MASK  = $0200;
  ALM_MASK  = $0100;

type TStat = packed record
  Ans:U8;
  Stat:TTrkCtlStat;
  Axis:array[1..3] of TAxisStat;
end;

type TAxisLimits = packed record
  LSN:F32;
  ZP:F32;
  LSP:F32;
end;

type TLimits = packed record
  Ans:U8;
  Axis:array[1..3] of TAxisLimits;
end;

type TParam = packed record
  Ans:U8;
  DevId:U8;
  Id:U8;
  Param:TParamVar;
end;

const
  ANS_STAT   = 0;
  ANS_PARAM  = 20;
  ANS_LIMITS = 110;


type TIPacket = packed record
  case U8 of
   // Answer
    $7E:(Ans:U8;);
    ANS_STAT:  (Stat:TStat;);
    ANS_LIMITS:(Limits:TLimits;);
    ANS_PARAM: (Param:TParam;);
end;

function  EncodeAxisStat(const Hlsn,Plsn,Plsp,Hlsp,Rdy,Alm:BOOL):U16;
procedure DecodeAxisStat(const Stat:U16; var Hlsn,Plsn,Plsp,Hlsp,Rdy,Alm,INP:BOOL);

//function  EncodeStat(const Pwr,Brk,Hold,Tracking:BOOL; const AtsState:U8):U16;
//procedure DecodeStat(const Stat:U16; var Pwr,Brk,Hold:BOOL; var TrackMode:U8);

function  EncodeStat(const Pwr,Brk,Hold:BOOL; const TrackMode:U8):U16;
procedure DecodeStat(const Stat:U16; var Pwr,Brk,Hold,NP:BOOL; var TrackMode:U8);


IMPLEMENTATION

function EncodeAxisStat(const Hlsn,Plsn,Plsp,Hlsp,Rdy,Alm:BOOL):U16;
begin
  Result:=0;
  if Hlsn then Result:=Result or HLSN_MASK;
  if Plsn then Result:=Result or PLSN_MASK;
  if Plsp then Result:=Result or PLSP_MASK;
  if Hlsp then Result:=Result or HLSP_MASK;
  if Rdy  then Result:=Result or RDY_MASK;
  if Alm  then Result:=Result or ALM_MASK;
end;

procedure DecodeAxisStat(const Stat:U16; var Hlsn,Plsn,Plsp,Hlsp,Rdy,Alm,INP:BOOL);
begin
  Hlsn:=(Stat and HLSN_MASK) <> 0;
  Plsn:=(Stat and PLSN_MASK) <> 0;
  Plsp:=(Stat and PLSP_MASK) <> 0;
  Hlsp:=(Stat and HLSP_MASK) <> 0;
  Rdy:= (Stat and RDY_MASK) <> 0;
  Alm:= (Stat and ALM_MASK) <> 0;
  INP:= (Stat and INP_MASK) <> 0;
end;

{
function  EncodeStat(const Pwr,Brk,Hold,Tracking:BOOL; const AtsState:U8):U16;
begin
  Result:=0;
  if Pwr  then Result:=Result or PWR_MASK;
  if Brk  then Result:=Result or BRK_MASK;
  if Hold then Result:=Result or HOLD_MASK;
  if Tracking then Result:=Result or TRK_MASK or (AtsState and $03);
end;

procedure DecodeStat(const Stat:U16; var Pwr,Brk,Hold,Trk:BOOL; var TrackMode:U8);
begin
  Pwr:= (Stat and PWR_MASK) <> 0;
  Brk:= (Stat and BRK_MASK) <> 0;
  Hold:=(Stat and HOLD_MASK) <> 0;
  Trk:= (Stat and TRK_MASK) <> 0;
  TrackMode:=Stat and TM_MASK;
end;
}

function  EncodeStat(const Pwr,Brk,Hold:BOOL; const TrackMode:U8):U16;
begin
  Result:=TrackMode and TM_MASK;
  if Pwr  then Result:=Result or PWR_MASK;
  if Brk  then Result:=Result or BRK_MASK;
  if Hold then Result:=Result or HOLD_MASK;
end;

procedure DecodeStat(const Stat:U16; var Pwr,Brk,Hold,NP:BOOL; var TrackMode:U8);
begin
  Pwr:= (Stat and PWR_MASK) <> 0;
  Brk:= (Stat and BRK_MASK) <> 0;
  Hold:=(Stat and HOLD_MASK) <> 0;
  NP:=(Stat and NEXT_PNT) <> 0;
  TrackMode:=Stat and TM_MASK;
end;

END.
