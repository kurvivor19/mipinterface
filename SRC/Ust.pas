UNIT Ust;
INTERFACE uses Classes, SysUtils,
  RxUtils,
  RxAlias;

type TUstPnt = packed record
  Az,El:F32;
end;

type TSatName = string[32];

type TUstHdr = packed record
  SatName:TSatName;
  StartTime:TDateTime;
  TimeStep:F32;
  NumOfPnts:U16;
  HdrExtLen:U16;
end;

type TUstHdrExtBuf = packed array[1..65535] of U8;

type TUst = class(TComponent)
  private
    F:Integer;
    FHdr:TUstHdr;
    FHdrExt:TUstHdrExtBuf;
    FPnt:array[1..65535] of TUstPnt;
    function GetPnt(I:U16):TUstPnt;
  protected
  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    function Read(UstName:string):boolean; virtual;
    procedure Write(UstName:string); virtual;
    function  GetHdrExt(AHdrExt:PTR):BOOL; virtual;
    procedure AddHdr(const ASatName:TSatName; const AStartTime:TDateTime; const ATimeStep:F32); virtual;
    procedure AddHdrExt(var AHdrExt; const AHdrExtLen:U16); virtual;
    procedure AddPnt(const APnt:TUstPnt); overload; virtual;
    procedure AddPnt(const Az,El:F32); overload; virtual;
    procedure Clear(); virtual;
    property Hdr:TUstHdr read FHdr;
    property Pnt[I:U16]:TUstPnt read GetPnt;
  published
end;

procedure Register;

IMPLEMENTATION uses Math, DateUtils;

type TUstBinHdr = packed record
  SatName:array[1..32] of Char;
  Year,Month,Day:U16;
  Hour,Min,Sec:U16;
  TimeStep:F32;
  NumOfPnts:U16;
  Reserv:array[1..12] of U8;
  ExtraBytes:U16;
end;

type TUstPkdPnt = packed record
  Az,El:U16;
end;

const
  UstToDeg  = 45.0/8192.0;
  DegToUst  = 8192.0/45.0;

procedure Register;
begin
  RegisterComponents('PolusRm', [TUst]);
end;

constructor TUst.Create(AOwner:TComponent);
var I:U16;
begin
  inherited Create(AOwner);
  FHdr.SatName:='';
  FHdr.StartTime:=0.0;
  FHdr.TimeStep:=0.0;
  FHdr.NumOfPnts:=0;
  FHdr.HdrExtLen:=0;
  for I:=1 to 65535 do FHdrExt[I]:=0;
  for I:=1 to 65535 do begin
    FPnt[I].Az:=0.0;
    FPnt[I].El:=0.0;
  end;
end;

function TUst.Read(UstName:string): boolean;
var UstHdr:TUstBinHdr;
    I:U16;
    UstPnt:TUstPkdPnt;
begin
  F:=FileOpen(UstName,fmOpenRead);
  if F < 0 then begin
     FileClose(F);
     Result:=False;
     Exit;
  end;

  FileRead(F,UstHdr,SizeOf(TUstBinHdr));

  // �������� ��� ���
  FHdr.SatName:='';
  for I:=1 to 32 do begin
    if UstHdr.SatName[I] = #0 then Break
    else FHdr.SatName[I]:=UstHdr.SatName[I];
  end;
  FHdr.SatName[0]:=Char(I-1);

  // �������� ����.����� ������
  with UstHdr do FHdr.StartTime:=EncodeDateTime(Year,Month,Day,Hour,Min,Sec,0);

  // �������� �������� ��� � ����
  FHdr.TimeStep:=UstHdr.TimeStep;
  FHdr.NumOfPnts:=UstHdr.NumOfPnts;

  FHdr.HdrExtLen:=0;
  if UstHdr.ExtraBytes <> 0 then begin
    FHdr.HdrExtLen:=UstHdr.ExtraBytes;
    FileRead(F,FHdrExt,UstHdr.ExtraBytes);
  end;

  for I:=1 to UstHdr.NumOfPnts do begin
    FileRead(F,UstPnt,SizeOf(TUstPkdPnt));
    FPnt[I].Az:=UstPnt.Az*UstToDeg;
    FPnt[I].El:=UstPnt.El*UstToDeg;
  end;
  //FileRead(F,FPnt,(UstHdr.NumOfPnts)*SizeOf(TUstPnt));
  for I:=UstHdr.NumOfPnts to 65535 do FPnt[I]:=FPnt[UstHdr.NumOfPnts];

  FileClose(F);
  Result:=True;  
end;

procedure TUst.Write(UstName:string);
var UstHdr:TUstBinHdr;
    UstPnt:TUstPkdPnt;
    I,MSecs:U16;
begin
  for I:=1 to Length(FHdr.SatName) do UstHdr.SatName[I]:=FHdr.SatName[I];
  UstHdr.SatName[Length(FHdr.SatName)+1]:=#0;

  with UstHdr do DecodeDateTime(FHdr.StartTime,Year,Month,Day,Hour,Min,Sec,MSecs);

  UstHdr.TimeStep:=FHdr.TimeStep;
  UstHdr.NumOfPnts:=FHdr.NumOfPnts;
  for I:=1 to 12 do UstHdr.Reserv[I]:=0;

  UstHdr.ExtraBytes:=FHdr.HdrExtLen;

  F:=FileOpen(UstName,fmOpenWrite);
  //F:=FileOpen(UstName,fmCreate);
  if F < 0 then begin
     FileClose(F);
     Exit;
  end;

  FileWrite(F,UstHdr,SizeOf(TUstBinHdr));
  if UstHdr.ExtraBytes <> 0 then FileWrite(F,FHdrExt,UstHdr.ExtraBytes);
  for I:=1 to Length(FHdr.SatName) do begin
    UstPnt.Az:=Floor(FPnt[I].Az*DegToUst);
    UstPnt.El:=Floor(FPnt[I].El*DegToUst);
    FileWrite(F,UstPnt,SizeOf(TUstPkdPnt));
  end;
  //FileWrite(F,FPnt,UstHdr.NumOfPnts*SizeOf(TUstPnt));
  FileClose(F);
end;

function TUst.GetPnt(I:U16):TUstPnt;
begin
  Result:=FPnt[I];
end;

destructor TUst.Destroy;
begin
  inherited Destroy;
end;

procedure TUst.AddHdrExt(var AHdrExt; const AHdrExtLen:U16);
var I:U16;
begin
  for I:=1 to AHdrExtLen do FHdrExt[I]:=TUstHdrExtBuf(AHdrExt)[I];
  FHdr.HdrExtLen:=AHdrExtLen;
end;

function TUst.GetHdrExt(AHdrExt:PTR):BOOL;
begin
  Result:=False;
  if FHdr.HdrExtLen = 0 then Exit;
  Move(FHdrExt,AHdrExt^,FHdr.HdrExtLen);
  Result:=True;
end;

procedure TUst.AddHdr(const ASatName:TSatName; const AStartTime:TDateTime; const ATimeStep:F32);
begin
  FHdr.SatName:=ASatName;
  FHdr.StartTime:=AStartTime;
  FHdr.TimeStep:=ATimeStep;
end;

procedure TUst.AddPnt(const APnt:TUstPnt);
begin
  Inc(FHdr.NumOfPnts);
  FPnt[FHdr.NumOfPnts]:=APnt;
end;

procedure TUst.AddPnt(const Az,El:F32);
begin
  Inc(FHdr.NumOfPnts);
  FPnt[FHdr.NumOfPnts].Az:=Az;
  FPnt[FHdr.NumOfPnts].El:=El;
end;

procedure TUst.Clear();
var I:U16;
begin
  FHdr.SatName:='';
  FHdr.StartTime:=0.0;
  FHdr.TimeStep:=0.0;
  FHdr.NumOfPnts:=0;
  FHdr.HdrExtLen:=0;
  for I:=1 to 65535 do FHdrExt[I]:=0;
  for I:=1 to 65535 do begin
    FPnt[I].Az:=0.0;
    FPnt[I].El:=0.0;
  end;
end;

END.




