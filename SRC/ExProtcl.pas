UNIT ExProtcl;
INTERFACE uses RxAlias;

type TSetTime = packed record
  Cmd:U8;
  Time:TDateTime;
end;

type TMoveBySpd = packed record
  Cmd:U8;
  Spd:array[1..3] of F64;
end;

type TSetCorr = packed record
  Cmd:U8;
  Az,El:F64;
  Time:S32;
end;

type TMoveTo = packed record
  Cmd:U8;
  Pos,Spd:F64;
end;

type TSaveZP = packed record
  Cmd:U8;
  Dir:S8;
end;

type TSetLimits = packed record
  Cmd:U8;
  LSP,LSN:F64;
end;

type TSetPos = packed record
  Cmd:U8;
  Pos:F64;
end;

const
  CMD_MOVETO = 10;
  CMD_RESTOREZP = 11;
  CMD_SAVEZP = 12;
  CMD_SETPOS = 13;
  CMD_GETLIMITS = 14;
  CMD_SETLIMITS = 15;
  CMD_POWERON = 16;
  CMD_POWEROFF = 17;

  CMD_STOP = 0;
  CMD_SETTIME = 1;
  CMD_SETCORR = 2;
  CMD_MOVEBYSPD = 3;

type TOutPacket = packed record
  Cmd:U8;
  case Byte of
    CMD_MOVETO:(MoveTo:TMoveTo;);
    CMD_SETTIME:(SetTime:TSetTime;);
    CMD_MOVEBYSPD:(MoveBySpd:TMoveBySpd;);
    CMD_SETCORR:(SetCorr:TSetCorr;);
    CMD_SETLIMITS:(SetLimits:TSetLimits;);
    CMD_SAVEZP:(SaveZP:TSaveZP;);
    CMD_SETPOS:(SetPos:TSetPos;);
end;

IMPLEMENTATION
END.