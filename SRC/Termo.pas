UNIT Termo;
INTERFACE uses  SysUtils, Classes, QExtCtrls,
  Windows,
  RxStrPktCom,
  RxUtils,
  RxAlias;

type
  TBTMDataAvail = procedure() of object;


type TTermoCtrl = class(TRxStrPktCom)
  private
    FOnDataAvail:TBTMDataAvail;
    FLoad2: BOOL;
    FLoad1: BOOL;
    FT1,FT2:S16;
    FAlm2:BOOL;
    FAlm1:BOOL;
  protected
    FExState:TExchangeState;
    FExCnt:U16;
    FS:TFormatSettings;
    //
    function  DecodePacket():U8; override;
    procedure ProcessData(); virtual;
    procedure SignalTimeout(); override;
  public
    IPacket,OPacket:string;
    //
    constructor Create(AOwner:TComponent); override;
    destructor  Destroy(); override;
    function    Connect():BOOL; virtual;
    function   Disconnect():BOOL; virtual;
    //
    property ExState:TExchangeState read FExState;
    property Load1:BOOL read FLoad1;
    property Load2:BOOL read FLoad2;
    property T1:S16 read FT1;
    property T2:S16 read FT2;
    property Alm1:BOOL read FAlm1;
    property Alm2:BOOL read FAlm2;
    property OnDataAvail:TBTMDataAvail read FOnDataAvail write FOnDataAvail default NIL;
end;

IMPLEMENTATION  uses StrUtils;

constructor TTermoCtrl.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  Chnl.Baud:=9600;
  Chnl.Parity:=EVENPARITY;
  Chnl.DataBits:=DATABITS_8;
  Chnl.StopBits:=ONESTOPBIT;
  Chnl.StartChar:='!';
  Chnl.PacketSize:=14;
  Chnl.Timeout:=1000;
  //with FPort do begin
  //  Baud:=9600;
  //  Parity:=pEven;
    {$IFDEF WIN32}
  //  DataBits:=8;
  //  StopBits:=1;
  //  TraceName:= 'BTM.TRC';
  //  LogName:= 'BTM.LOG';
    //
    //LogAllHex:=True;
    //LogHex:=True;
    //TraceAllHex:=True;
    //Logging:=tlOn;
    //Tracing:=tlOn;
    {$ENDIF}
    {$IFDEF LINUX}
  //  DataBits:=dbEight;
  //  StopBits:=sbOne;
    {$ENDIF}
  //end;

  //CreateDataPacketsList(1);
  //with FData[0] do begin
  //  StartCond:=scString;
  //  StartString:='!';
  //  PacketSize := 14;
  //  EndCond:=[ecPacketSize];
    //EndCond:=[ecString];
  //  EndString:=CRLF;
  //  IncludeStrings:=True;
  //end;

  FS.DecimalSeparator:=',';
  TimerInit(1500);
  FExState:=exsDisconnected;
end;

function TTermoCtrl.DecodePacket():U8;
begin
  //ReStartTriggerTimer();
  IPacket:=InBuffer.FIPacket;

  // 1  2  3  4  5  6  7  8  9  10  11  13 14
  // !  L  L  D  D     +  T  T       +   T  T
  FLoad1:=IPacket[2] <> '0';
  FLoad2:=IPacket[3] <> '0';
  FAlm1:=IPacket[4] <> '1';
  FAlm2:=IPacket[5] <> '1';

  FT1:=StrToInt(MidStr(IPacket, 7, 3));
  FT2:=StrToInt(MidStr(IPacket,11, 3));

  //WriteLn(F,IPacket);

  FExState:=exsAnswer;
  ProcessData();
  if Assigned(OnDataAvail) then OnDataAvail();
end;

procedure TTermoCtrl.ProcessData();
begin
end;

destructor TTermoCtrl.Destroy();
begin
  inherited Destroy();
end;

function TTermoCtrl.Connect():BOOL;
begin
  Result:=inherited Connect();
  if not Result then Exit;
  //InitTriggerTimer(5*18);
  //StartTriggerTimer();
  //AssignFile(F,'BTM.log'); ReWrite(F);
end;

function TTermoCtrl.Disconnect():BOOL;
begin
  FExState:=exsDisconnected;
  Result:=inherited Disconnect();
  //StopTriggerTimer();
  //CloseFile(F);
  if Assigned(OnDataAvail) then OnDataAvail();
end;

procedure TTermoCtrl.SignalTimeout();
begin
  FExState:=exsNoAnswered;
  if Assigned(OnDataAvail) then OnDataAvail();
end;

END.

