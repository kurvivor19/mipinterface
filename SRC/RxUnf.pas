UNIT RxUnf;
INTERFACE uses  SysUtils, Classes, QExtCtrls,
  RxBinPktCom,
  Windows,
  RxStrPktCom,
  Nmea0183,
  RxUtils,
  RxAlias;

type
  TUnfDataAvail = procedure(Sender:TObject; S:TNmeaSentences ) of object;


type TRxUnf = class(TRxStrPktCom)
  private
    FOnDataAvail:TUnfDataAvail;
  protected
    FTalkerId:TTalkerId;
    FSentenceId:TSentenceId;
    FSentence:TNmeaSentences;
    FExState:TExchangeState;
    FDataAvail:BOOL;
    function CheckSumValid(const Data:string):BOOL;
    //
    function DecodeGGA():BOOL; virtual;
    function DecodeGLL():BOOL; virtual;
    function DecodeGSV():BOOL; virtual;
    function DecodeGSA():BOOL; virtual;
    function DecodeRMC():BOOL; virtual;
    function DecodeZDA():BOOL; virtual;
    function DecodeHDT():BOOL; virtual;
    function DecodeProprietary():BOOL; virtual;
    //
    procedure SendPacket(const Packet:string); override;
    function  DecodePacket():U8; override;
    procedure ControlThread(); override;
    procedure ProcessData(); virtual;
    procedure SignalTimeout(); override;
  public
    IPacket,OPacket:string;
    //
    constructor Create(AOwner:TComponent); override;
    destructor  Destroy(); override;
    function    Connect():BOOL; virtual;
    function   Disconnect():BOOL; virtual;
    //procedure SelectNextPort(); override;
    //
    property ExState:TExchangeState read FExState;
    property GnssId:TTalkerId read FTalkerId;
    property GnssMsgId:TSentenceId read FSentenceId;
    property OnDataAvail:TUnfDataAvail read FOnDataAvail write FOnDataAvail default NIL;
end;

IMPLEMENTATION  uses StrUtils;

constructor TRxUnf.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  Chnl.Baud:=115200;
  Chnl.Parity:=NOPARITY;
  Chnl.DataBits:=DATABITS_8;
  Chnl.StopBits:=ONESTOPBIT;
  Chnl.StartChar:='$';
  Chnl.StopChar:=LF;
  Chnl.Timeout:=1000;
  //with FPort do begin
  //  Baud:=115200;
    {$IFDEF WIN32}
  //  DataBits:=8;
  //  Parity:=pNone;
  //  StopBits:=1;
  //  TraceName:= 'UNF.TRC';
  //  LogName:= 'UNF.LOG';
    //
    //LogAllHex:=True;
    //LogHex:=True;
    //TraceAllHex:=True;
    //Logging:=tlOn;
  //  Tracing:=tlOn;
    {$ENDIF}
    {$IFDEF LINUX}
  //  DataBits:=dbEight;
  //  Parity:=pNone;
  //  StopBits:=sbOne;
    {$ENDIF}
  //end;

  //CreateDataPacketsList(1);
  //with FData[0] do begin
  //  StartCond:=scString;
  //  StartString:='$';
  //  EndCond:=[ecString];
  //  EndString:=CRLF;
    //IncludeStrings:=False;
  //  IncludeStrings:=True;
  //end;
  InBuffer.FWriteQueue:=true;
  TimerInit(1000);

  FExState:=exsDisconnected;


  //Chnl[1].Baud:=Chnl[0].Baud;
end;

function TRxUnf.DecodePacket():U8;
var
    TalkerId,SentenceId:string[3];
begin
  //IPacket:='$' + FIPacket + '<CR><LF>';
  if GetPacketCount(InBuffer) > 0 then begin
    GetFromBuffer(InBuffer, IPacket);
    InBuffer.FWriteQueue:=InBuffer.FWriteQueue and (GetPacketCount(InBuffer) < 5);
  end
  else begin
    IPacket:=InBuffer.FIPacket;
    InBuffer.FWriteQueue:=true;
  end;
  //Field.Text:=AnsiReplaceStr(FIPacket, ',', #13#10);
  //Field.Text:=AnsiReplaceStr(Field.Text, '*', #13#10+'*');
  Field.Text:=AnsiReplaceStr(IPacket, ',', CRLF);
  Field.Text:=AnsiReplaceStr(Field.Text, '*', CRLF+'*');
  Field.Text:=AnsiReplaceStr(Field.Text, '.', DecimalSeparator);

  if not CheckSumValid(IPacket) then Exit;

  FSentence.Sentence:=IPacket;

  //WriteLn(F,IPacket);

  FSentence.SentenceID:=UnknownSentenceId;
  FSentence.TalkerId:=UnknownTalkerId;
  if (Field[0][1] <> '$') then begin
    Exit;
  end;

  if (Field[0][2] = PROPRIETARY_MSG) then begin
    //FSentence.SentenceID:=Proprietary;
    DecodeProprietary();
    Exit;
  end;
  if ((Length(Field[0]) <> 6)) then begin
    FSentence.SentenceID:=Invalid;
    Exit;
  end;

  TalkerId:=MidStr(Field[0],2,2);
  case TalkerId[1] of
    'G':case TalkerId[2] of
        'P':begin
           FSentence.TalkerId:=GP;
          end;
        'N':begin
           FSentence.TalkerId:=GN;
          end;
        'L':begin
           FSentence.TalkerId:=GL;
          end;
    end;

  end;

  SentenceId:=MidStr(Field[0],4,3);
  case SentenceId[1] of

    'G':case SentenceId[2] of
        'S':case SentenceId[3] of
            'A':begin
                DecodeGSA();
            end;
            'V':begin
                DecodeGSV();
            end;
        end;
        'G':case SentenceId[3] of
            'A':begin
                DecodeGGA();
                //OutputDebugString(PAnsiChar('Decoding GGA'));
            end;
        end;
        'L':case SentenceId[3] of
            'L':begin
                DecodeGLL();
            end;
        end;
    end;
    'R':case SentenceId[2] of
        'M':case SentenceId[3] of
            'C':begin
                DecodeRMC();
            end;
        end;
    end;
    'Z':case SentenceId[2] of
        'D':case SentenceId[3] of
            'A':begin
                DecodeZDA();
            end;
        end;
     end;
    'H':case SentenceId[2] of
        'D':case SentenceId[3] of
            'T':begin
                DecodeHDT();
                //OutputDebugString(PAnsiChar('Decoding HDT'));
            end;
        end;
    end

    else begin
    end;

  end;

  FExState:=exsAnswer;

  //ReStartTriggerTimer();
  FDataAvail:=True;
  
  ProcessData();

  if Assigned(OnDataAvail) then OnDataAvail(Self, FSentence);

  FSentence.SentenceID:=UnknownSentenceId;
end;

procedure TRxUnf.ProcessData();
begin
end;

function TRxUnf.CheckSumValid(const Data: string): BOOL;
var I,CS,MCS:U8;
    C:Char;
begin
  //Result:=False;
  Result:=True;

  if Field[Field.Count-1][1] <> '*' then Exit;
  C:=Field[Field.Count-1][2];
  if (U8(C) - U8('0')) <= 9 then MCS:=(U8(C) - U8('0')) shl 4
                            else MCS:=(U8(C) - U8('A') + 10) shl 4;
  C:=Field[Field.Count-1][3];
  if (U8(C) - U8('0')) <= 9 then MCS:=MCS or (U8(C) - U8('0'))
                            else MCS:=MCS or (U8(C) - U8('A') + 10);
  CS:=0;
  for I:=1 to Length(Data)-3 do CS:=CS xor U8(Data[I]);


  if (CS <> MCS) then Exit;
  Result:=True;
end;

procedure TRxUnf.SendPacket(const Packet: string);
var I,CS:U8;
begin
  CS:=0;
  for I:=1 to Length(Packet) do CS:=CS xor U8(Packet[I]);
  OPacket:='$' + Packet + '*' + IntToHex(CS,2) + CRLF;
  //FPort.PutString(OPacket);
end;

// GGA   Global Positioning System Fix Data. Time, Position and fix related data for a GPS receiver
function TRxUnf.DecodeGGA():BOOL;
begin
  // 0      1          2        3  4         5  6  7   8    9    10 11   12 13   14   15
  //$--GGA, hhmmss.ss, llll.ll, a, yyyyy.yy, a, x, xx, x.x, x.x, M, x.x, M, x.x, xxxx *hh
  Result:=False;  
  FSentence.SentenceID:=GGA;
  with FSentence.GGA do begin
    try
      // 1. hhmmss.ss - Time (UTC)
      if Trim(Field[1]) = '' then Exit;
      //if (Length(Field[1]) <> 9) then Exit;
      Hour:=StrToInt(MidStr(Field[1], 1, 2)); //  hh '00'..'23'
      Min:= StrToInt(MidStr(Field[1], 3, 2)); //  mm '00'..'59'
      Sec:= StrToInt(MidStr(Field[1], 5, 2)); //  ss '00'..'59'
      MSec:=StrToInt(MidStr(Field[1], 8, 2)); // .ss '00'..'99'
      if (Hour > 23) or (Min > 59) or (Sec > 59) or (MSec > 99) then Exit;

      // 2,3. ddmm.mm , N/S - Latitude  [deg] N/S (S neg) (-90..0..+90)
      //if Trim(Field[2]) = '' then Exit;
      //Latitude:=StrToFloat(Field[3]);
      Latitude:=StrToFloat(LeftStr(Field[2],2)) + StrToFloat(MidStr(Field[2], 3, 8))/60.0;
      //Latitude:=StrToInt(LeftStr(Field[2],2)) + StrToFloat(MidStr(Field[2], 3, 8))/60.0;
      if Field[3][1] = 'S' then Latitude:=-Latitude;

      // 4,5. dddmm.mm , E/W - Longitude [deg]  E/W (W neg)
      //if Trim(Field[4]) = '' then Exit;
      Longitude:=StrToFloat(LeftStr(Field[4],3)) + StrToFloat(MidStr(Field[4], 4, 8))/60.0;
      if Field[5][1] = 'W' then Longitude:=-Longitude;
      //if Field[5][1] = 'W' then Longitude:=360.0 - Longitude;

      // 6. GPS Quality Indicator  0-fix not available, 1-GPS fix, 2-Differential GPS fix
      //if (Length(Field[6]) <> 1) then Exit;
      GPSQuality:=Ord(Field[6][1]);
      
      // 7. Number of satellites in view, 00 - 12
      //if (Length(Field[7]) <> 1) then Exit;
      NumOfSat:=StrToInt(Field[7]);


      // 8. Horizontal Dilution of precision

      // 9,10. Antenna Altitude above/below mean-sea-level (geoid) Units of antenna altitude, meters)
      //if Trim(Field[9]) = '' then Exit;
      Altitude:=StrToFloat(Field[9]);

      // 11. Geoidal separation, the difference between the WGS-84 earth ellipsoid and mean-sea-level (geoid),
      // "-" means mean-sea-level below ellipsoid

      // 12. Units of geoidal separation, meters

      // 13. Age of differential GPS data, time in seconds since last SC104
      // type 1 or 9 update, null field when DGPS is not used
      // 14. Differential reference station ID, 0000-1023

    except
      on EConvertError do Exit;
    else
      Exit;
    end;
  end;

  Result:=True;
end;

// HDT Heading � True
function TRxUnf.DecodeHDT(): BOOL;
begin
  //  0      1    2  3
  // $--HDT, x.x, T *hh
  Result:=False;
  FSentence.SentenceID:=HDT;
  with FSentence.HDT do begin
    try
      Valid:=False;
      if Field[1] <> '' then begin
        Heading:=StrToFloat(Field[1]); //  x.x '0.0'..'360.0'
        Valid:=True;
      end;
    except
      on EConvertError do Exit;
    else
      Exit;
    end;
  end;
  Result:=True;
end;

// RMC - Recommended Minimum Navigation Information
function TRxUnf.DecodeRMC():BOOL;
begin
  //  0      1          2  3        4  5         6  7    8    9     10   11  12
  // $--RMC, hhmmss.ss, A, llll.ll, a, yyyyy.yy, a, x.x, x.x, xxxx, x.x, a  *hh
  Result:=False;

  //if (Field.Count <> 14) then Exit;

  FSentence.SentenceID:=RMC;

  with FSentence.RMC do begin
    try
      // 1. hhmmss.ss - Time (UTC)
      if Trim(Field[1]) = '' then Exit;
      if (Length(Field[1]) <> 9) then Exit;
      Hour:=StrToInt(MidStr(Field[1], 1, 2)); //  hh '00'..'23'
      Min:= StrToInt(MidStr(Field[1], 3, 2)); //  mm '00'..'59'
      Sec:= StrToInt(MidStr(Field[1], 5, 2)); //  ss '00'..'59'
      MSec:=StrToInt(MidStr(Field[1], 8, 2)); // .ss '00'..'99'
      if (Hour > 23) or (Min > 59) or (Sec > 59) or (MSec > 99) then Exit;
      // 2. A - Status V-Navigation reciver warning
      if (Length(Field[2]) <> 1) then Exit;
      Status:=Field[2][1];
      // 3,4. llll.ll , N/S - Latitude  [deg] N/S (S neg)
      if Trim(Field[3]) = '' then Exit;
      //Latitude:=StrToFloat(Field[3]);
      Latitude:=StrToInt(LeftStr(Field[3],2)) + StrToFloat(MidStr(Field[3], 3, 8))/60.0;
      if Field[4][1] = 'S' then Latitude:=-Latitude;
      // 5,6. yyyyy.yy , E/W - Longitude [deg]  E/W (W neg)
      if Trim(Field[5]) = '' then Exit;
      Longitude:=StrToFloat(Field[5]);
      if Field[6][1] = 'W' then Longitude:=-Longitude;
      // 7. x.x - Speed Over Ground [knots]
      if Trim(Field[7]) = '' then Exit;
      SOG:=StrToFloat(Field[7]);
      // 8. x.x - Course Over Ground (Trackmade good) [deg] TRUE
      if Trim(Field[8]) = '' then Exit;
      COG:=StrToFloat(Field[8]);
      // 9. ddmmyy - Date
      if Trim(Field[9]) = '' then Exit;
      if (Length(Field[9]) <> 6) then Exit;
      Day:=  StrToInt(MidStr(Field[9], 1, 2)); //  dd '01'..'31'
      Month:=StrToInt(MidStr(Field[9], 3, 2)); //  mm '01'..'12'
      Year:= StrToInt(MidStr(Field[9], 5, 2)); //  yy ????
      if ((Day < 1) and (Day > 31)) or ((Month < 1) and (Month > 12)) then Exit;
      // 10,11. x.x - Magnetic Variation [deg] E/W
      if Trim(Field[10]) = '' then Exit;
      MagneticVariation:=StrToFloat(Field[5]);
      if Field[11][1] = 'W' then MagneticVariation:=-MagneticVariation;
    except
      on EConvertError do Exit;
    else
      Exit;
    end;

  end;

end;

// ZDA - Time & Date (UTC, Day, Month, Year and Local Time Zone)
function TRxUnf.DecodeZDA():BOOL;
begin
  //  0      1          2   3   4      5   6    7
  // $--ZDA, hhmmss.ss, dd, mm, yyyy, -hh, mm  *hh
  Result:=False;

  //if (Field.Count <> 14) then Exit;


  FSentence.SentenceID:=ZDA;

  with FSentence.ZDA do begin
    try
      // 1. hhmmss.ss - Time (UTC)
      if Trim(Field[1]) = '' then Exit;
      if (Length(Field[1]) <> 9) then Exit;
      Hour:=StrToInt(MidStr(Field[1], 1, 2)); //  hh '00'..'23'
      Min:= StrToInt(MidStr(Field[1], 3, 2)); //  mm '00'..'59'
      Sec:= StrToInt(MidStr(Field[1], 5, 2)); //  ss '00'..'59'
      MSec:=StrToInt(MidStr(Field[1], 8, 2)); // .ss '00'..'99'
      if (Hour > 23) or (Min > 59) or (Sec > 59) or (MSec > 99) then Exit;
      // 2. dd - Day
      if Trim(Field[2]) = '' then Exit;
      Day:=StrToInt(Field[2]); //  dd '01'..'31'
      if ((Day < 1) and (Day > 31)) then Exit;
      // 3. mm - Month
      if Trim(Field[3]) = '' then Exit;
      Month:=StrToInt(Field[3]); //  mm '01'..'12'
      if ((Month < 1) and (Month > 12)) then Exit;
      // 4. yyyy - Year
      if Trim(Field[4]) = '' then Exit;
      Year:=StrToInt(Field[4]); //  mm '01'..'12'
      // 5,6. -hh mm - Time Zone
      if Trim(Field[5]) = '' then Exit;
      if Trim(Field[6]) = '' then Exit;
      TZHour:=StrToInt(Field[5]); //  hh '-/+00'..'-/+13'
      TZMin:= StrToInt(Field[6]); //  mm '00'..'59'
      if (Hour > 13) or (Min > 59) then Exit;      
    except
      on EConvertError do Exit;
    else
      Exit;
    end;

  end;
end;

function TRxUnf.DecodeGSV():BOOL;
var I,K:U8;
begin
  //  0      1  2  3   4   5   6    7
  // $--GSV, N, N, NN, NN, NN, NNN, NN, .....,NN, NN, NNN, NN *hh
  Result:=False;

  FSentence.SentenceID:=GSV;

  with FSentence.GSV do begin
    try
      TotalMsg:=StrToInt(Field[1]);
      CurrentMsg:=StrToInt(Field[2]);
      NumOfSat:=StrToInt(Field[3]);
      K:=4;
      for I:=1 to 4 do begin
        if Field[K][1] = '*' then Break;
        Satelite[I].PRN:=StrToInt(Field[K]);
        Satelite[I].El:= StrToInt(Field[K+1]);
        Satelite[I].Az:= StrToInt(Field[K+2]);
        Satelite[I].SNR:=StrToIntDef(Field[K+3],0);
        Inc(K,4);
      end;
    except
      on EConvertError do Exit;
    else
      Exit;
    end;

  end;

end;

function TRxUnf.DecodeGLL():BOOL;
begin
  FSentence.SentenceID:=GLL;
end;

function TRxUnf.DecodeGSA():BOOL;
begin
  FSentence.SentenceID:=GSA;
end;

function TRxUnf.DecodeProprietary():BOOL;
begin
end;

destructor TRxUnf.Destroy();
begin
  inherited Destroy();
end;

function TRxUnf.Connect():BOOL;
begin
  //AssignFile(F,'SPS55H.log'); ReWrite(F);
  Result:=inherited Connect();
  if not Result then Exit;
  FDataAvail:=False;
  //TimerStart();
  //InitTriggerTimer(5*18);
  //StartTriggerTimer();
end;

function TRxUnf.Disconnect():BOOL;
begin
  FExState:=exsDisconnected;
  //StopTriggerTimer();
  Result:=inherited Disconnect();
  //CloseFile(F);
  if Assigned(OnDataAvail) then OnDataAvail(Self, FSentence);
end;

procedure TRxUnf.SignalTimeout();
begin
  FExState:=exsNoAnswered;
  FSentence.SentenceID:=UnknownSentenceId;
  //SelectNextPort();
  if Assigned(OnDataAvail) then OnDataAvail(Self, FSentence);
end;

procedure TRxUnf.ControlThread();
begin
  if FDataAvail then begin
     FDataAvail:=False;
     Exit;
  end;
  FExState:=exsNoAnswer;
  FSentence.SentenceID:=UnknownSentenceId;
  //SelectNextPort();
  if Assigned(OnDataAvail) then OnDataAvail(Self, FSentence);
  FDataAvail:=False;
end;

//procedure TRxUnf.SelectNextPort();
//var I:U8;
//begin
//  TimerStop();
//  for I:=0 to High(FData) do FData[I].Enabled:=False;
//  if FPort.Open then begin
//    FPort.FlushInBuffer();
//    FPort.FlushOutBuffer();
//  end;
//  FPort.Open:=False;

//  case FCurChnl of
//    0:if(Chnl[1].DeviceName <> '') then FCurChnl:=1;
//    1:if(Chnl[0].DeviceName <> '') then FCurChnl:=0;
//  end;

//  SetDeviceName(Chnl[FCurChnl].DeviceName);
//  SetBaud(Chnl[FCurChnl].Baud);

//  FPort.Open:=True;
//  for I:=0 to High(FData) do FData[I].Enabled:=True;
//  FDataAvail:=False;
//  TimerStart();
//end;


END.
