UNIT IPCUdp;
INTERFACE uses SysUtils, Classes, RxAlias, Windows, WinSock;

type
  TIPCUdp = class(TComponent)
  private
    fSendSocket: TSocket;
    Tracing: Boolean;
  protected
  public
    constructor Create(AOwner:TComponent; Tracing:BOOL); // override;
    destructor Destroy; override;
    procedure Connect;
    procedure Disconnect;
    procedure SendTo(const AIPAddr:string; const APort:U16; var ABuffer; const AByteCount:U32);

  end;
function GetErrorString: string;
IMPLEMENTATION uses StrUtils;  

{ TIPCUdp }

// ������� GetErrorString ���������� ��������� �� ������, ��������������
// �������� �� ������ ��������, ������� ������� ������� WSAGetLastError.
// ��� ��������� ��������� ������������ ��������� ������� FormatMessage.
function GetErrorString: string;
var
  Buffer: array[0..2047] of Char;
begin
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, nil, WSAGetLastError, $400,
    @Buffer, SizeOf(Buffer), nil);
  Result := Buffer;
end;

constructor TIPCUdp.Create(AOwner: TComponent; Tracing: BOOL);
var WSAData : TWSAData;
begin
  // ������������� ���������� ������� 1.1
  if WSAStartup($101, WSAData) <> 0 then
  begin
    OutputDebugString('������ ��� ������������� ���������� WinSock');
  end
  else
  begin
    // �������� ������
    fSendSocket := socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if fSendSocket = INVALID_SOCKET then
    begin
      OutputDebugString(PChar('������ ��� �������� ������������� ������:'#13#10 + GetErrorString));
    end
  end;
end;

destructor TIPCUdp.Destroy;
begin
  closesocket(fSendSocket);
  fSendSocket := 0;
  WSACleanup;
end;

procedure TIPCUdp.Connect;
var Addr : TSockAddr; // �����, � �������� ������������� ����� ��� �������� ���������
  AddrLen : Integer;
begin
  //Socket.LocalHost := Socket.LocalHostName();
  FillChar(Addr.sin_zero, SizeOf(Addr.sin_zero), 0);
  Addr.sin_family := AF_INET;
  Addr.sin_addr.S_addr := INADDR_ANY;
  Addr.sin_port := 0;
  // �������� ������ � ������
  // ��������� ���� ���������� ��������
  if bind(fSendSocket, Addr, SizeOf(Addr)) = SOCKET_ERROR then
  begin
    OutputDebugString(PChar('������ ��� ��������� ������ ������������� ������:'#13#10 + GetErrorString));
  end
  else
  begin
    // �����, ����� ����� ������� ��������� ������.
    // ��� ����� ��� ������ ���������� ��� ������������
    AddrLen := SizeOf(Addr);
    if getsockname(fSendSocket, Addr, AddrLen) = SOCKET_ERROR then
    begin
      OutputDebugString(PChar('������ ��� ��������� ����� ������������� ������:'#13#10 + GetErrorString));
    end
    else
      OutputDebugString(PChar('�������� ����������� � ����� ' + IntToStr(ntohs(Addr.sin_port))));
  end;
end;

procedure TIPCUdp.Disconnect;
begin
  // ��������� �����
  // ����� ����� ���������� ����� ���� ����������� ������ ����� ����������
  closesocket(fSendSocket);

  // �������� ������
  fSendSocket := socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if fSendSocket = INVALID_SOCKET then
  begin
    OutputDebugString(PChar('������ ��� �������� ������������� ������:'#13#10 + GetErrorString));
  end
end;

procedure TIPCUdp.SendTo(const AIPAddr:string; const APort:U16; var ABuffer; const AByteCount: U32);
var //pBuffer: ^U8;
  SendAddr: TSockAddr;  // ����� ����������
  SendRes : Integer;    // ��������� ��������
begin
  FillChar(SendAddr.sin_zero, SizeOf(SendAddr.sin_zero), 0);
  SendAddr.sin_family := AF_INET;
  SendAddr.sin_addr.S_addr := inet_addr(PChar(AIPAddr));
  if SendAddr.sin_addr.S_addr = INADDR_NONE then
  begin
    OutputDebugString(PChar('"' + AIPAddr + '" �� �������� IP-�������'));
    Exit;
  end;
  SendAddr.sin_port := htons(APort);
  //pBuffer := @ABuffer;
  SendRes := WinSock.sendto(fSendSocket,ABuffer, AByteCount, 0, SendAddr, SizeOf(SendAddr));
  //if SendRes < 0 then
  //  OutputDebugString(PChar('������ ��� �������� ���������:'#13#10 + GetErrorString))
  //else
    //OutputDebugString(PChar(Format('���������� %d ������ (�� %d)', [SendRes, AByteCount])));
end;

END.

