UNIT KMath;
INTERFACE uses Ust, RxAlias;

type TTrType = (trLo,trMid,trHi);

var
  PointsCount:U16;
  Az1,El1:array[1..65535] of F64;
  K1,K2,K3:array[1..65535] of F64;
  TimeStep:single;
  //
  //CTime:U32;
  StartTime,StopTime,EndTrackTime,TRackTime:U32;
  StartDate:U32;
  TrType:TTrType;
  TrakLoLimit:F64;


type TAEPnt = packed record
  Az,El:F32;
end;

procedure TrimTrack(const AK1,AK2,AK3:F64; const AStartTime,ACTime:U32; var AStartTimeOffset:U32; var AFstPnt:U16);
//
procedure AEToKinematic(const AUstName:string); stdcall;
procedure AEToKinematicAndTrim(const AUstName:string; const AK1,AK2,AK3:F64; const ACTime:U32; var AStartTime:U32; var AFstPnt:U16); stdcall;
procedure KinematicToAE(const AK1,AK2,AK3:F64; var AAz,AEl:F64); stdcall;

procedure GetFstPnt(const APNum:U16; var AK1,AK2,AK3:F64); stdcall;
function  GetPnt(const ATime:U32; var AK1,AK2,AK3:F64):BOOL; stdcall;
procedure GetPntEx(const ATime:U32; var AK1,AK2,AK3:F64; var AAz,AEl:F64); stdcall;

procedure CK1ToCK1R(var Az,El:F64; const RotX,RotY,RotZ:F64); stdcall;
procedure CK1RToCK1(var Az,El:F64; const RotX,RotY,RotZ:F64); stdcall;

procedure SetRotX(const ARotX:F64); stdcall;
procedure SetRotY(const ARotY:F64); stdcall;
procedure SetRotZ(const ARotZ:F64); stdcall;
procedure SetTimeZone(const ATimeZone:S32); stdcall;

IMPLEMENTATION uses Math, SysUtils, DateUtils, RxUtils;

procedure InitPnt; forward;
procedure WriteTrToFile; forward;
procedure InterpTrajectory; forward;

const
  CUNum = 65535;
  //TrakLoLimit:Single = 78.5;
  //TrakLoLimit:Single = 90.0;
  pi = 3.1415926535897932385;
  dskrRad = pi/100000.0;
  radDskr = 100000.0/pi;
  radDeg = 180.0/pi;
  degRad = pi/180.0;
  dskrDeg = 360.0/200000.0;
  degDskr = 200000.0/360.0;
  dpsDskr  = 2000000.0/6.0;
  dskrDPS  = 6.0/2000000.0;
  hourSec = 3600; minSec = 60;
  fullSDay = 23*3600 + 59*60 + 59;
  Zf = 2000000;
  GradRad : Extended = 0.017453292519943295769236907684886;
  RadGrad : Extended = 57.295779513082320876798154814105;
  PI_2      =  1.570796326794896619231322;   { pi / 2 }
  //


type
  CUArr = array[1..CUNum] of S32;
  TTim = record
     hour,min,sec:word; days:S32;
  end;
  TAx2Dir = (Minus,Plus);
  InpArr = array[0..CUNum-1] of F32;

var
  //PointsCount:U16;
  //Az,El:CUArr;
  //TimeStep:single;
  //
  FRotX,FRotY,FRotZ:F64;
  FTimeZone:S32;
  Az,El:array[1..65535] of S32;
  //
  _Az,_El:CUArr;
  RotateDir: boolean;
  CulmEl,CulmAz,CulmAz3,CulmI:S32;
  stAz,stEl,stpAz,stpEl:S32;
  culTim,stTim:TTim;
  Mas_E,Mas_VE,Mas_AE:InpArr;
  Mas_B,Mas_VB,Mas_AB:InpArr;
  A1IP,A2IP,A3IP:S32;
  POSITION,ZPOSITION:array [1..4] of S32;
  curLT,stLT,stpLT,curLTD:S32;
  gtH,gtM,gtS,gtS100:U16;
  CorAz1,CorAz3,CorEl:S32; CorTm,OldCorTm:double;
  SpMax: array[1..3] of S32;
  start:array[1..3] of bool;
  SCurTime:double;
  Delta_StTm:Integer;
  //
  Ust:TUst;
  NumOfPnts:U16;

const
  DskrtPerRot = 200000;

function PosDegToDskrt(const APos:F64):S32;
begin
  Result:=Round(APos*DskrtPerRot/360.0);
end;

procedure CK1ToCK1R(var Az,El:F64; const RotX,RotY,RotZ:F64); stdcall;
var
  Real_Az,Real_El,Ant_Az,Ant_El,X,H,Y : F80;
  A11,A12,A13,A21,A22,A23,A31,A32,A33 : F80;
  X1,H1,Y1,D : F80;
  Alf,Tet,Psi:F80;
begin

  Real_Az:=Az;
  Real_El:=El;
  Alf:=RotZ;
  Tet:=RotX;
  Psi:=RotY;
  D:=100.0;

  //if Real_Az > 180 then Real_Az:=Real_Az-360;
  //if Alf > 180.0 then Alf:=360.0 - Alf else Alf:=-Alf;
  //if Psi < 0 then Psi:=abs(Psi) else Psi:=-Psi;

  Tet:=-Tet;
  Psi:=-Psi;
  if Ant_Az > 180.0 then Ant_Az:=Ant_Az-360.0;
  if Alf > 180.0 then Alf:=360.0 - Alf else Alf:=-Alf;

  X:=D*cos(degRad*Real_El)*cos(degRad*Real_Az);
  H:=D*sin(degRad*Real_El);
  Y:=D*cos(degRad*Real_El)*sin(degRad*Real_Az);

  A11:=cos(degRad*Psi)*cos(degRad*Alf);
  A21:=sin(degRad*Psi)*cos(degRad*Alf)*cos(degRad*Tet)-sin(degRad*Tet)*sin(degRad*Alf);
  A31:=sin(degRad*Psi)*cos(degRad*Alf)*sin(degRad*Tet)+cos(degRad*Tet)*sin(degRad*Alf);
  A12:=-sin(degRad*Psi);
  A22:=cos(degRad*Tet)*cos(degRad*Psi);
  A32:=cos(degRad*Psi)*sin(degRad*Tet);
  A13:=-cos(degRad*Psi)*sin(degRad*Alf);
  A23:=-cos(degRad*Tet)*sin(degRad*Psi)*sin(degRad*Alf)-sin(degRad*Tet)*cos(degRad*Alf);
  A33:=-sin(degRad*Tet)*sin(degRad*Psi)*sin(degRad*Alf)+cos(degRad*Tet)*cos(degRad*Alf);

  X1:=A11*X + A12*H + A13*Y;
  H1:=A21*X + A22*H + A23*Y;
  Y1:=A31*X + A32*H + A33*Y;

  D:=sqrt(X1*X1+H1*H1+Y1*Y1);
  Ant_El:=arcsin(H1/D);
  if (X1>0.0000001) then Ant_Az:=arctan(Y1/X1);
  if (X1<-0.0000001) then begin
     Ant_Az:=Pi*sign(Y1)+arctan(Y1/X1);
     if sign(Y1)=0 then Ant_Az:=Pi;
  end;
  if (X1>=-0.0000001) and (X1<=0.0000001) then begin
    if (Y1>0.0000001)  then Ant_Az:=Pi/2;
    if (Y1<-0.0000001) then Ant_Az:=-Pi/2;
    if (Y1>=-0.0000001) and (Y1<=0.0000001) then Ant_Az:=0;
  end;

  Ant_Az:=Ant_Az*RadDeg;
  Ant_El:=Ant_El*RadDeg;

  //if Ant_El < 0 then Ant_El:=0;

  Az:=Ant_Az;
  El:=Ant_El;

  if Az < 0 then Az:=Az + 360;

end;

//procedure CK1RToCK1(var Az,El,Dist:F64; const RotX,RotY,RotZ:F64); stdcall;
procedure CK1RToCK1(var Az,El:F64; const RotX,RotY,RotZ:F64); stdcall;
var
  Ant_Az,Ant_El,D : Extended;
  X1,H1,Y1,X,H,Y : Extended;
  A11,A12,A13,A21,A22,A23,A31,A32,A33,Real_Az,Real_El : Extended;
  Alf,Tet,Psi:Extended;
begin
  Ant_Az:=Az;
  Ant_El:=El;
  Alf:=RotZ;
  Tet:=RotX;
  Psi:=RotY;
  D:=100.0;

  //if Ant_Az > 180 then Ant_Az:=Ant_Az-360;
  //if Alf > 180.0 then Alf:=360.0 - Alf else Alf:=-Alf;
  //if Psi < 0 then Psi:=abs(Psi) else Psi:=-Psi;

  Tet:=-Tet;
  Psi:=-Psi;
  if Ant_Az > 180.0 then Ant_Az:=Ant_Az-360.0;
  if Alf > 180.0 then Alf:=360.0 - Alf else Alf:=-Alf;


  X1:=D*cos(degRad*Ant_El)*cos(degRad*Ant_Az);
  H1:=D*sin(degRad*Ant_El);
  Y1:=D*cos(degRad*Ant_El)*sin(degRad*Ant_Az);

  A11:=cos(degRad*Psi)*cos(degRad*Alf);
  A21:=sin(degRad*Psi)*cos(degRad*Alf)*cos(degRad*Tet)-sin(degRad*Tet)*sin(degRad*Alf);
  A31:=sin(degRad*Psi)*cos(degRad*Alf)*sin(degRad*Tet)+cos(degRad*Tet)*sin(degRad*Alf);
  A12:=-sin(degRad*Psi);
  A22:=cos(degRad*Tet)*cos(degRad*Psi);
  A32:=cos(degRad*Psi)*sin(degRad*Tet);
  A13:=-cos(degRad*Psi)*sin(degRad*Alf);
  A23:=-cos(degRad*Tet)*sin(degRad*Psi)*sin(degRad*Alf)-sin(degRad*Tet)*cos(degRad*Alf);
  A33:=-sin(degRad*Tet)*sin(degRad*Psi)*sin(degRad*Alf)+cos(degRad*Tet)*cos(degRad*Alf);

  X:=A11*X1 + A21*H1 + A31*Y1;
  H:=A12*X1 + A22*H1 + A32*Y1;
  Y:=A13*X1 + A23*H1 + A33*Y1;

  D:=sqrt(X*X+H*H+Y*Y);
  Real_El:=arcsin(H/D);

  if (X>0.0000001) then Real_Az:=arctan(Y/X);
  if (X<-0.0000001) then begin
   Real_Az:=Pi*sign(Y)+arctan(Y/X);
   if sign(Y)=0 then Real_Az:=Pi;
  end;
  if (X>=-0.0000001) and (X<=0.0000001) then begin
    if (Y>0.0000001) then
      Real_Az:=Pi/2;
    if (Y<-0.0000001) then
      Real_Az:=-Pi/2;
    if (Y>=-0.0000001) and (Y<=0.0000001) then
      Real_Az:=0;   // Real_Az[i-1]
  end;

  Real_Az:=Real_Az*RadDeg;
  Real_El:=Real_El*RadDeg;

  //if Ant_El < 0 then Ant_El:=0;

  Az:=Real_Az;
  El:=Real_El;

  if Az < 0 then Az:=Az+360;

end;

procedure AEToKinematic(const AUstName:string); stdcall;
var I:U16; LocalStartTime:TDateTime; D:U32;
begin
  Ust:=TUst.Create(NIL);
  Ust.Read(AUstName);
  {}
  LocalStartTime:=Ust.Hdr.StartTime;
  //LocalStartTime:=IncMilliSecond(LocalStartTime,FTimeZone);
  StartTime:=DateTimeToTimeStamp(LocalStartTime).Time;
  StartDate:=DateTimeToTimeStamp(LocalStartTime).Date;
  //LocalStartTime:=Now();
  //if StartDate > DateTimeToTimeStamp(LocalStartTime).Date then Inc(StartTime,MSecsPerDay);
  {}

  {
  //LocalStartTime:=Now;
  LocalStartTime:=EncodeDateTime(2015,2,15,18,0,0,0);
  LocalStartTime:=IncMilliSecond(LocalStartTime,FTimeZone);
  StartTime:=DateTimeToTimeStamp(LocalStartTime).Time;
  StartDate:=DateTimeToTimeStamp(LocalStartTime).Date;
  LocalStartTime:=IncDay(LocalStartTime,-1);
  if StartDate > DateTimeToTimeStamp(LocalStartTime).Date then Inc(StartTime,MSecsPerDay);
  {}

  TimeStep:=Ust.Hdr.TimeStep;
  NumOfPnts:=Ust.Hdr.NumOfPnts;
  for I:=1 to NumOfPnts do begin
    Az1[I]:=Ust.Pnt[I].Az;
    El1[I]:=Ust.Pnt[I].El;
  end;
  Ust.Free;
  PointsCount:=NumOfPnts;
  InitPnt;
end;

procedure AEToKinematicAndTrim(const AUstName:string; const AK1,AK2,AK3:F64; const ACTime:U32; var AStartTime:U32; var AFstPnt:U16); stdcall;
begin
  AEToKinematic(AUstName);
  TrimTrack(AK1,AK2,AK3,StartTime,ACTime,AStartTime,AFstPnt);
end;  


procedure itimh(var tim:TTim; cnt:word);
var i:word;
begin
 if cnt = 0 then exit;
 for i:=1 to cnt do begin
     inc(tim.hour);
     if tim.hour >= 24 then begin
        tim.hour:=tim.hour - 24;
        inc(tim.days,1);
     end;
 end;
end;  

procedure itimm(var tim:TTim; cnt:word);
var i:word;
begin
 if cnt = 0 then exit;
 for i:=1 to cnt do begin
     inc(tim.min);
     if tim.min >= 60 then begin
        tim.min:=tim.min - 60;
        itimh(tim,1);
     end;
 end;
end;  

procedure itim(var tim:TTim; cnt:word);
var i:word;
begin
 if cnt = 0 then exit;
 for i:=1 to cnt do begin
     inc(tim.sec);
     if tim.sec >= 60 then begin
        tim.sec:=tim.sec - 60;
        itimm(tim,1);
     end;
 end;
end;

function sign_dskr(x:longint):shortint;
begin
 if abs(x)<2 then sign_dskr:=0
             else if x>0 then sign_dskr:= 1
                         else sign_dskr:=-1;
end;

////////----------------------------
Procedure GetRotateDir;
var
 i,Cnt : integer;
begin
 Cnt:=0;
 for i:=1 to PointsCount-1 do begin
  if Az[i+1]>Az[i] then inc(Cnt);
  if Az[i+1]<Az[i] then dec(Cnt);
 end;
 if Cnt>0 then RotateDir:=True
          else RotateDir:=False;
end;

procedure GetTrType;
var i:word;
    Pl90,Min90:S32;
    perehod: boolean;
    Cul3_Sum:LongInt;
begin
 CulmEl:=El[1];
 for i:=2 to PointsCount-2 do begin
   if El[i]>=CulmEl{abs(El[i])} then begin
     CulmEl:=El[i];
     CulmI:=i;
   end;
 end;
//                                                   else break;
 if abs(CulmEl) < round(TrakLoLimit*degDskr) then TrType:=trLo
                                             else TrType:=trHi;

 for i:=1 to PointsCount do begin
   if (Az[i]>round(180.0*degDskr)) and (Az[i]<=round(360.0*degDskr)) then
     Az[i]:=Az[i]-round(360.0*degDskr);
 end;

 stAz:=Az[1]; stEl:=El[1];
 stpAz:=Az[pointscount]; stpEl:=El[pointscount];

{        textbackground(lightgray);
        textcolor(white);gotoxy(9,1);
        write('  CulmEl=');write((CulmEl*DskrDeg):5:5);}

 CulmAz:=Az[CulmI];
// CulmI:=i;
 CulmAz3:=Az[CulmI];

 culTim:=stTim; itim(culTim,CulmI);
// TranslateCoord;

// if abs(CulmEl) <= round(83.5*degDskr) then exit;
 If ((CulmI-20)<0) or ((CulmI+20)>PointsCount) then Exit;

 perehod:=False;
 For i:=(CulmI-23) to (CulmI+23) do begin
   if Abs(Az[i]-Az[i-1])>round(200.0*degDskr) then perehod:=true;
 end;

 Cul3_Sum:=0;

 if (not perehod) then begin
   for i:=(CulmI-20) to (CulmI+20) do Cul3_Sum:=Cul3_Sum+Az[i];
   CulmAz3 := round(Cul3_Sum / 41.0);
 end;

 if perehod then begin
   for i:=(CulmI-20) to (CulmI+20) do begin
     if Az[i]>0 then
       Cul3_Sum:=Cul3_Sum+Az[i]
     else
       Cul3_Sum:=Cul3_Sum+Az[i]+round(360.0*degDskr);
   end;
   CulmAz3 := round(Cul3_Sum / 41.0);
   if CulmAz3>=round(360.0*degDskr) then CulmAz3:=CulmAz3-round(360.0*degDskr);
 end;

end;

{--------- INIT POINT ----------}
procedure InitPnt;
var T1,T2,T3,TMAX:S32;
var CT:S32;
var i:S32;
var Ax2Dir:TAx2Dir;
var E1,E,Max_no_min,Max_yes_min:extended;
var MaxA3 : extended;
var NeedMinus : Boolean;
const TZapas:word = 10;
const A1Div = 6;
const A1Spd = 3333 div A1Div;
begin
 for i:=1 to PointsCount do begin
   CK1ToCK1R(Az1[i],El1[i], FRotX,FRotY,FRotZ);
   Az[i]:=Round(Az1[i]*degDskr);
   El[i]:=Round(El1[i]*degDskr);
   if El[i] < 0 then El[i]:=0;
 end;

 GetRotateDir;
 GetTrType;

 if TrType=TrLo then begin
   GetRotateDir;
   if (Az[1]>=round(-180.0*degDskr)) and (Az[1]<0.0) and RotateDir then begin
     Ax2Dir:=Minus;
   end;
   if (Az[1]>=0) and (Az[1]<=round(180.0*degDskr)) and (not RotateDir) then begin
     Ax2Dir:=Minus;
   end;
   if (Az[1]>=round(-180.0*degDskr)) and (Az[1]<0.0) and (not RotateDir) then begin
     Ax2Dir:=Plus;
     for i:=1 to PointsCount do begin
       if Az[i]>=0.0 then Az[i]:=Az[i]-round(180.0*degDskr)
       else Az[i]:=Az[i]+round(180.0*degDskr);
     end;
   end;
   if (Az[1]>=0) and (Az[1]<=round(180.0*degDskr)) and RotateDir then begin
     Ax2Dir:=Plus;
     for i:=1 to PointsCount do begin
       if Az[i]>=0.0 then Az[i]:=Az[i]-round(180.0*degDskr)
       else Az[i]:=Az[i]+round(180.0*degDskr);
     end;
   end;
   
   for i:=1 to PointsCount do begin
     Mas_B[i-1]:=0;
     if Ax2Dir=Minus then Mas_E[i-1]:=-(round(90.0*degDskr)-El[i])
     else Mas_E[i-1]:=round(90.0*degDskr)-El[i];
     Mas_B[i-1]:=Az[i];
   end;
   for i:=PointsCount+1 to PointsCount+5 do begin      // �� ������ ��������
     Mas_B[i-1]:=Mas_B[PointsCount-1];
     Mas_E[i-1]:=Mas_E[PointsCount-1];
   end;
   
   for i:=1 to PointsCount do begin
      K3[I]:=Mas_B[i-1]*DskrDeg;
      K2[I]:=Mas_E[i-1]*DskrDeg;
      K1[I]:=0;
   end;
   
   InterpTrajectory;
 end;

 if TrType=TrHi then begin
   GetRotateDir;
   GetTrType;
   A3IP:=Az[1];
   if RotateDir then begin
     A3IP:=CulmAz3-round(90.0*degDskr);
     if A3IP<round(-180.0*degDskr) then A3IP:=A3IP+round(360.0*degDskr);
   end;
   if (not RotateDir) then begin
     A3IP:=CulmAz3+round(90.0*degDskr);
     if A3IP>round(180.0*degDskr) then A3IP:=A3IP-round(360.0*degDskr);
   end;
   NeedMinus:=False;
   for i:=1 to PointsCount do begin
     E1:=A3IP; E1:=E1*dskrRad;
     E:=El[i]; E:=E*dskrRad;
     Mas_B[i-1]:=ArcSin(cos(E)*sin(Az[i]*dskrRad-E1));
     Mas_E[i-1]:=(ArcSin( sin(E)/cos(Mas_B[i-1]) ))*radDskr-(radDskr*pi/2);
     if (abs(Mas_E[i-1])*DskrDeg < 3.0) and (not NeedMinus) then
     begin
       if abs(Mas_E[i-1])>abs(Mas_E[i-2]) then NeedMinus:=True;
       if abs(Mas_E[i-1])>abs(Mas_E[i-3]) then Mas_E[i-2]:=-Mas_E[i-2];
     end;
     if NeedMinus then Mas_E[i-1]:=-Mas_E[i-1];
     Mas_B[i-1]:=Mas_B[i-1]*radDskr;
   end;
   for i:=PointsCount+1 to PointsCount+5 do begin      // �� ������ ��������
     Mas_B[i-1]:=Mas_B[PointsCount-1];
     Mas_E[i-1]:=Mas_E[PointsCount-1];
   end;
   MaxA3:=0;
   for i:=1 to PointsCount do begin
     if abs(Mas_B[i-1])>MaxA3 then
     begin
       MaxA3:=abs(Mas_B[i-1]);
     end;
   end;
   if (MaxA3*DskrDeg)>14.0 then begin
     A3IP:=Az[1];
     NeedMinus:=False;
     for i:=1 to PointsCount do begin
       E1:=A3IP; E1:=E1*dskrRad;
       E:=El[i]; E:=E*dskrRad;
       Mas_B[i-1]:=ArcSin(cos(E)*sin(Az[i]*dskrRad-E1));
       Mas_E[i-1]:=(ArcSin( sin(E)/cos(Mas_B[i-1]) ))*radDskr-(radDskr*pi/2);
       if (abs(Mas_E[i-1])*DskrDeg < 3.0) and (not NeedMinus) then
       begin
         if abs(Mas_E[i-1])>abs(Mas_E[i-2]) then NeedMinus:=True;
         if abs(Mas_E[i-1])>abs(Mas_E[i-3]) then Mas_E[i-2]:=-Mas_E[i-2];
       end;
       if NeedMinus then Mas_E[i-1]:=-Mas_E[i-1];
       Mas_B[i-1]:=Mas_B[i-1]*radDskr;
     end;
     for i:=PointsCount+1 to PointsCount+5 do begin      // �� ������ ��������
       Mas_B[i-1]:=Mas_B[PointsCount-1];
       Mas_E[i-1]:=Mas_E[PointsCount-1];
     end;
   end;

   for i:=1 to PointsCount do begin
      K1[I]:=Mas_B[i-1]*DskrDeg;
      K2[I]:=Mas_E[i-1]*DskrDeg;
      K3[I]:=A3IP*DskrDeg;
   end;

   InterpTrajectory;
 end;

 WriteTrToFile;

end;

procedure TrimTrack(const AK1,AK2,AK3:F64; const AStartTime,ACTime:U32; var AStartTimeOffset:U32; var AFstPnt:U16);
var T1,T2,T3,TMAX:S32;
var CT:S32;
var i:S32;
var Ax2Dir:TAx2Dir;
var E1,E,Max_no_min,Max_yes_min:extended;
var MaxA3 : extended;
var NeedMinus : Boolean;
const TZapas:word = 0;
const A1Div = 6;
const A1Spd = 3333 div A1Div;
begin
 //
 POSITION[1]:=PosDegToDskrt(AK1); POSITION[2]:=PosDegToDskrt(AK2); POSITION[3]:=PosDegToDskrt(AK3);
 curLT:=ACTime div 1000;
 StLT:=AStartTime div 1000; 
 //
 A2IP:=round(Mas_E[0]);
 case TrType of
  trLo: begin A3IP:=round(Mas_B[0]); A1IP:=0; end;
  trHi: begin A1IP:=round(Mas_B[0]); end;
 end;
 if (POSITION[1] < 0) and (A1IP < 0) or (POSITION[1] > 0) and (A1IP > 0)
 then T1:= abs( POSITION[1] - A1IP )  div A1Spd
 else T1:=( abs(POSITION[1]) + abs(A1IP) ) div A1Spd;

 if (POSITION[2] < 0) and (A2IP < 0) or (POSITION[2] > 0) and (A2IP > 0)
 then T2:= abs( POSITION[2] - A2IP )  div 3333
 else T2:=( abs(POSITION[2]) + abs(A2IP) ) div 3333;

 if (POSITION[3] < 0) and (A3IP < 0) or (POSITION[3] > 0) and (A3IP > 0)
 then T3:= abs( POSITION[3] - A3IP )  div 3333
 else T3:=( abs(POSITION[3]) + abs(A3IP) ) div 3333;

 TMAX:=T1;
 if T2 > TMAX then TMAX:=T2;
 if T3 > TMAX then TMAX:=T3;
 TMAX:=TMAX + TZapas;

 //curLT:=gtH*3600+gtM*60+gtS;
 
 if (StLT > 24*3600) then begin
   StLT:=StLT - 24*3600;
   StpLT:=StpLT - 24*3600;
 end;

 CT:=curLT - stLT;      // vremia, kotoroe proshlo so starta

 corAz1:=0; corAz3:=0; corEl:=0; corTm:=0; OldCorTm:=0;

  if curLT+TMAX < stLT then begin            // esli traektoriu nado zhdat'
     //SpMax[1]:=Zf div 2; SpMax[2]:=Zf; SpMax[3]:= Zf;
     //ZPOSITION[1]:=A1IP; ZPOSITION[2]:=A2IP; ZPOSITION[3]:=A3IP;
     //Start[1]:=true; Start[2]:=true; Start[3]:=true;
     //SCurTime:=0;
     AFstPnt:=1; AStartTimeOffset:=AStartTime;
     Exit;
  end;

  {
  if curLT+TMAX > stpLT then begin            // esli opozdali perenosim na sled sutki
     //if stLT < 24*3600 then stLT:=stLT + 24*3600;
     //SpMax[1]:=Zf div 2; SpMax[2]:=Zf; SpMax[3]:= Zf;
     //ZPOSITION[1]:=A1IP; ZPOSITION[2]:=A2IP; ZPOSITION[3]:=A3IP;
     //Start[1]:=true; Start[2]:=true; Start[3]:=true;
     //SCurTime:=0;
     AFstPnt:=1; AStartTimeOffset:=AStartTime;
     Exit;
  end;
  }
  for i:=1 to PointsCount do begin

      A2IP:=round(Mas_E[i-1]);
      case TrType of
        trLo: begin A3IP:=round(Mas_B[i-1]); A1IP:=0; end;
        trHi: begin A1IP:=round(Mas_B[i-1]); end;
      end;

      if (POSITION[1] < 0) and (A1IP < 0) or (POSITION[1] > 0) and (A1IP > 0)
      then T1:= abs( POSITION[1] - A1IP )  div A1Spd
      else T1:=( abs(POSITION[1]) + abs(A1IP) ) div A1Spd;

      if (POSITION[2] < 0) and (A2IP < 0) or (POSITION[2] > 0) and (A2IP > 0)
      then T2:= abs( POSITION[2] - A2IP )  div 3333
      else T2:=( abs(POSITION[2]) + abs(A2IP) ) div 3333;

      if (POSITION[3] < 0) and (A3IP < 0) or (POSITION[3] > 0) and (A3IP > 0)
      then T3:= abs( POSITION[3] - A3IP )  div 3333
      else T3:=( abs(POSITION[3]) + abs(A3IP) ) div 3333;

      TMAX:=T1;
      if T2 > TMAX then TMAX:=T2;
      if T3 > TMAX then TMAX:=T3;
      TMAX:=TMAX + TZapas;

      if (curLT + TMAX) <= (stLT+i-1) then begin // esli uspeem dognat'  etu to4ku

         StLT:=StLT+i-1;

         Delta_StTm:=i;
         {
         if StLT >= StpLT then begin // perenosim na sled sutki
           if stLT < 24*3600 then stLT:=stLT + 24*3600;
         end;

         SpMax[1]:=Zf div 2; SpMax[2]:=Zf; SpMax[3]:=Zf;
         ZPOSITION[1]:=A1IP; ZPOSITION[2]:=A2IP; ZPOSITION[3]:=A3IP;
         Start[1]:=true; Start[2]:=true; Start[3]:=true;
         SCurTime:=i;    // esli 4to izmeniat zdes
         }
         AFstPnt:=I;  AStartTimeOffset:=AStartTime + I*1000;
         Break;
      end;

  end;
end;

function round_for_4(x: extended): longint;
begin
  round_for_4:=round(x*10000);
end;



{ ------------------- InterpTrajectory -----------------------}
procedure InterpTrajectory;
var i:integer;
begin
 for i:=0 to (PointsCount-2) do begin
  Mas_VE[i]:=(-(0.5*Mas_E[i+2]-2.0*Mas_E[i+1]+1.5*Mas_E[i])/TimeStep);
  Mas_AE[i]:=((Mas_E[i+2]-2*Mas_E[i+1]+Mas_E[i])/(TimeStep*TimeStep));
  Mas_VB[i]:=(-(0.5*Mas_B[i+2]-2.0*Mas_B[i+1]+1.5*Mas_B[i])/TimeStep);
  Mas_AB[i]:=((Mas_B[i+2]-2*Mas_B[i+1]+Mas_B[i])/(TimeStep*TimeStep));
 end;

  Mas_VE[PointsCount-1]:=((0.5*Mas_E[PointsCount]-0.5*Mas_E[PointsCount-2])/TimeStep);
  Mas_AE[PointsCount-1]:=Mas_AE[PointsCount-2];
  Mas_VB[PointsCount-1]:=((0.5*Mas_B[PointsCount]-0.5*Mas_B[PointsCount-2])/TimeStep);
  Mas_AB[PointsCount-1]:=Mas_AB[PointsCount-2];
end;
{ --------------------- GetNextPoint ---------------------------------}
procedure GetNextPoint(t_tek:extended;
                       var E_ext_long,B_ext_long:longint;
                       var VE_ext_long,VB_ext_long:longint);
var i_tek:longint;
    tt_tek,DT_tek:extended;
    T_ext,T_ext2:extended;
begin
  tt_tek:=t_tek;
  i_tek:=trunc(tt_tek/TimeStep);
  DT_tek:=tt_tek-i_tek*TimeStep;

  T_ext:=DT_tek;
  T_ext2:=T_ext*T_ext/2;

  E_ext_long:=round(Mas_E[i_tek]+Mas_VE[i_tek]*T_ext+Mas_AE[i_tek]*T_ext2);
  B_ext_long:=round(Mas_B[i_tek]+Mas_VB[i_tek]*T_ext+Mas_AB[i_tek]*T_ext2);

  VE_ext_long:=round(Mas_VE[i_tek]+Mas_AE[i_tek]*T_ext);
  VB_ext_long:=round(Mas_VB[i_tek]+Mas_AB[i_tek]*T_ext);
end;

procedure WriteTrToFile;
var CosE1,CosE2,SinE2,AzCoord,ElCoord,Ax1,Ax2,Ax3: Extended;
var i:word;
var OK:Boolean;
var MaxAx1:extended;
var Failed_cnt:integer;
F:Text;
begin
  MaxAx1:=0;
  Failed_cnt:=0;

  Assign(F,'tr_plan.txt');
  Rewrite(F);

  if TrType=TrHi then begin
    writeln(F,'TrHi');
    writeln(F,'CulmAz=',CulmAz*DskrDeg:3:4);
    writeln(F,'CulmAz3=',CulmAz3*DskrDeg:3:4);
    writeln(F,'CulmEl=',CulmEl*DskrDeg:3:4);
    writeln(F,'CulmI=',CulmI:4);
    writeln(F,'A3IP=',A3IP*DskrDeg:3:4);
  end;

  if TrType=TrLo then begin
    writeln(F,'TrLo');
    writeln(F,'CulmAz=',CulmAz*DskrDeg:3:4);
    writeln(F,'CulmEl=',CulmEl*DskrDeg:3:4);
    writeln(F,'CulmI=',CulmI:4);
  end;

  if RotateDir then writeln(F,' CW') else writeln(F,' CCW');

  OK := true;

  for i:=1 to PointsCount do begin

    if TrType=TrHi then begin
      Ax1:=Mas_B[i-1]*DskrDeg;
      Ax2:=Mas_E[i-1]*DskrDeg;
      Ax3:=A3IP*DskrDeg;
    end;
    if TrType=TrLo then begin
      Ax3:=Mas_B[i-1]*DskrDeg;
      Ax2:=Mas_E[i-1]*DskrDeg;
      Ax1:=0;
    end;
   {
    write(F,i:4,' ','a1=',Ax1:3:4,' ');
    write(F,'a2=',Ax2:3:4,' ');
    write(F,'a3=',Ax3:3:4,'  ');
    }

    write(F,I:6,Ax1:10:2,Ax2:10:2,Ax3:10:2);

    if abs(Ax1) > abs(MaxAx1) then MaxAx1:=Ax1;

    COSE1:=cos(Ax1*GradRad);
    COSE2:=cos(Ax2*GradRad + pi_2);
    SINE2:=0;
    if Ax2<0 then SINE2:=sin(Ax2*GradRad + pi_2);
    if Ax2>0 then SINE2:=sin(pi_2 - Ax2*GradRad);
    ElCoord:=arcsin(COSE1*SINE2)*RadGrad;
    AzCoord:=Ax3;
    if Ax2<0 then AzCoord:=AzCoord +
                             arctan( tan(Ax1*GradRad)/COSE2 )*RadGrad;
    if Ax2=0 then AzCoord:=AzCoord + 90.0*sign_dskr(round(Ax1*DskrDeg));
    if Ax2>0 then AzCoord:=AzCoord + 180.0 +
                             arctan( tan(Ax1*GradRad)/COSE2 )*RadGrad;
    if AzCoord >  360.0 then AzCoord:=AzCoord-360.0;
    if AzCoord < -360.0 then AzCoord:=AzCoord+360.0;
    if AzCoord <    0   then AzCoord:=360.0+AzCoord;
    {
    write(F,AzCoord:10:2,ElCoord:10:2,(_Az[i]*DskrDeg):10:2,(_El[i]*DskrDeg):10:2);

    if abs(Ax1)>15.0 then begin
      writeln(F,'Incorrect line Ax1');
      inc(Failed_cnt);
      writeln(F,Ax1:3:4);
    end;

    if abs(Ax2)>90.0 then begin
      writeln(F,'Incorrect line Ax2');
      inc(Failed_cnt);
      writeln(F,Ax2:3:4);
    end;

    if abs(Ax3)>180 then begin
      writeln(F,'Incorrect line Ax3');
      inc(Failed_cnt);
      writeln(F,Ax3:3:4);
    end;

    if (round_for_4(AzCoord)-round_for_4(_Az[i]*DskrDeg))>1 then begin
      writeln(F,'Incorrect line Az');
      inc(Failed_cnt);
      writeln(F,AzCoord:3:7);
      writeln(F,(_Az[i]*DskrDeg):3:7);
      OK:=false;
    end;

    if ((round_for_4(ElCoord)-round_for_4(_El[i]*DskrDeg))>1) and (round_for_4(ElCoord)<>0.0000) then begin
      writeln(F,'Incorrect line El');
      inc(Failed_cnt);
      writeln(F,ElCoord:3:7);
      writeln(F,(_El[i]*DskrDeg):3:7);
      OK:=false;
    end;
     }

     writeln(F);
  end;

  //if OK then writeln(F,'OK') else writeln(F,'FAILED ',Failed_Cnt:5); writeln(F,'MaxAx1 = ',MaxAx1:3:4);

  Close(F);
end;

(*
procedure WriteTrToFile;
var CosE1,CosE2,SinE2,AzCoord,ElCoord,Ax1,Ax2,Ax3: Extended;
var i:word;
var OK:Boolean;
var MaxAx1:extended;
var Failed_cnt:integer;
F:Text;
begin
  MaxAx1:=0;
  Failed_cnt:=0;

  Assign(F,'tr_plan.txt');
  Rewrite(F);

  if TrType=TrHi then begin
    writeln(F,'TrHi');
    writeln(F,'CulmAz=',CulmAz*DskrDeg:3:4);
    writeln(F,'CulmAz3=',CulmAz3*DskrDeg:3:4);
    writeln(F,'CulmEl=',CulmEl*DskrDeg:3:4);
    writeln(F,'CulmI=',CulmI:4);
    writeln(F,'A3IP=',A3IP*DskrDeg:3:4);
  end;

  if TrType=TrLo then begin
    writeln(F,'TrLo');
    writeln(F,'CulmAz=',CulmAz*DskrDeg:3:4);
    writeln(F,'CulmEl=',CulmEl*DskrDeg:3:4);
    writeln(F,'CulmI=',CulmI:4);
  end;

  if RotateDir then
    writeln(F,' CW')
  else
    writeln(F,' UCW');

  OK := true;

  for i:=1 to {SatTrk.Pnts}PointsCount do begin

    if TrType=TrHi then begin
      Ax1:=Mas_B[i-1]*DskrDeg;
      Ax2:=Mas_E[i-1]*DskrDeg;
      Ax3:=A3IP*DskrDeg;
    end;
    if TrType=TrLo then begin
      Ax3:=Mas_B[i-1]*DskrDeg;
      Ax2:=Mas_E[i-1]*DskrDeg;
      Ax1:=0;
    end;

    write(F,i:4,' ','a1=',Ax1:3:4,' ');
    write(F,'a2=',Ax2:3:4,' ');
    write(F,'a3=',Ax3:3:4,'  ');

    if abs(Ax1)>abs(MaxAx1) then MaxAx1:=Ax1;

    COSE1:=cos(Ax1*GradRad);
    COSE2:=cos(Ax2*GradRad + pi_2);
    SINE2:=0;
    if Ax2<0 then SINE2:=sin(Ax2*GradRad + pi_2);
    if Ax2>0 then SINE2:=sin(pi_2 - Ax2*GradRad);
    ElCoord:=arcsin(COSE1*SINE2)*RadGrad;
    AzCoord:=Ax3;
    if Ax2<0 then AzCoord:=AzCoord +
                             arctan( tan(Ax1*GradRad)/COSE2 )*RadGrad;
    if Ax2=0 then AzCoord:=AzCoord + 90.0*sign_dskr(round(Ax1*DskrDeg));
    if Ax2>0 then AzCoord:=AzCoord + 180.0 +
                             arctan( tan(Ax1*GradRad)/COSE2 )*RadGrad;
    if AzCoord >  360.0 then AzCoord:=AzCoord-360.0;
    if AzCoord < -360.0 then AzCoord:=AzCoord+360.0;
    if AzCoord <    0   then AzCoord:=360.0+AzCoord;

    write(F,'Az=',AzCoord:3:4,' ');
    write(F,'El=',ElCoord:3:4,' ');

    write(F,'U_az=',(_Az[i]*DskrDeg):3:4,' ');
    writeln(F,'U_el=',(_El[i]*DskrDeg):3:4,' ');

    if abs(Ax1)>15.0 then begin
      writeln(F,'Incorrect line Ax1');
      inc(Failed_cnt);
      writeln(F,Ax1:3:4);
    end;

    if abs(Ax2)>90.0 then begin
      writeln(F,'Incorrect line Ax2');
      inc(Failed_cnt);
      writeln(F,Ax2:3:4);
    end;

    if abs(Ax3)>180 then begin
      writeln(F,'Incorrect line Ax3');
      inc(Failed_cnt);
      writeln(F,Ax3:3:4);
    end;

    if (round_for_4(AzCoord)-round_for_4(_Az[i]*DskrDeg))>1 then begin
      writeln(F,'Incorrect line Az');
      inc(Failed_cnt);
//      writeln(F,'round_for_4(AzCoord)=',round_for_4(AzCoord):7);
//      writeln(F,'round_for_4(SatTrk.Pnt[i].Az*45.0/8192.0)=',round_for_4(SatTrk.Pnt[i].Az*45.0/8192.0):7);
      writeln(F,AzCoord:3:7);
      writeln(F,(_Az[i]*DskrDeg):3:7);
      OK:=false;
    end;

    if ((round_for_4(ElCoord)-round_for_4(_El[i]*DskrDeg))>1) and (round_for_4(ElCoord)<>0.0000) then begin
      writeln(F,'Incorrect line El');
      inc(Failed_cnt);
//      writeln(F,'round_for_4(ElCoord)=',round_for_4(ElCoord):7);
//      writeln(F,'round_for_4(SatTrk.Pnt[i].El*45.0/8192.0)=',round_for_4(SatTrk.Pnt[i].El*45.0/8192.0):7);
      writeln(F,ElCoord:3:7);
      writeln(F,(_El[i]*DskrDeg):3:7);
      OK:=false;
    end;

  end;

  if OK then
    writeln(F,'OK')
  else
    writeln(F,'FAILED ',Failed_Cnt:5);

  writeln(F,'MaxAx1 = ',MaxAx1:3:4);

  Close(F);
end;
*)


procedure KinematicToAE(const AK1,AK2,AK3:F64; var AAz,AEl:F64); stdcall;
var SINE2:F64;
    E1,E2,E3:F64;
    CurAz,CurEl:F64;
begin
  E1:=DegToRad(AK1);
  E2:=DegToRad(AK2);
  E3:=DegToRad(AK3);

  SINE2:=0;
  if AK2 < 0 then SINE2:=sin(E2 + PI/2.0);
  if AK2 > 0 then SINE2:=sin(PI/2.0 - E2);

  CurEl:=RadToDeg( ArcSin(cos(E1)*SINE2) );

  CurAz:=AK3;

  if AngDegToSec(AK2) < 0 then
          CurAz:=CurAz + RadToDeg( ArcTan(tan(E1)/cos(E2 + PI/2.0)) );
  if AngDegToSec(AK2) = 0 then
          CurAz:=CurAz + 90.0*Sign(E1);
  if AngDegToSec(AK2) > 0 then
          CurAz:=CurAz + RadToDeg( ArcTan(tan(E1)/cos(E2 + PI/2.0)) ) + 180.0;
  if CurAz >  360.0 then CurAz:=CurAz - 360.0;
  if CurAz < -360.0 then CurAz:=CurAz + 360.0;
  if AngDegToSec(CurAz) < 0 then CurAz:=360.0 + CurAz;

  CK1RToCK1(CurAz,CurEl, FRotX,FRotY,FRotZ);

  AAz:=CurAz;
  AEl:=CurEl;
end;


function GetPnt(const ATime:U32; var AK1,AK2,AK3:F64):BOOL; stdcall;
var N:U16;
begin
  Result:=True;
  {
  N:=(ATime div MSecsPerSec) + 3;
  if N > PointsCount then begin Result:=False; Exit; end;
  AK1:=K1[N];
  AK2:=K2[N];
  AK3:=K3[N];
  {}
  {}
  N:=ATime;
  if N > (PointsCount + 1) then begin Result:=False; Exit; end;
  if N <= PointsCount then begin AK1:=K1[N]; AK2:=K2[N]; AK3:=K3[N]; end;
  {}
end;

procedure GetFstPnt(const APNum:U16; var AK1,AK2,AK3:F64); stdcall;
begin
  AK1:=K1[APNum]; AK2:=K2[APNum]; AK3:=K3[APNum];
end;

procedure GetPntEx(const ATime:U32; var AK1,AK2,AK3:F64; var AAz,AEl:F64); stdcall;
begin
end;

procedure SetRotX(const ARotX:F64); stdcall;
begin
  FRotX:=ARotX;
end;

procedure SetRotY(const ARotY:F64); stdcall;
begin
  FRotY:=ARotY;
end;

procedure SetRotZ(const ARotZ:F64); stdcall;
begin
  FRotZ:=ARotZ;
end;

procedure SetTimeZone(const ATimeZone:S32); stdcall;
begin
  FTimeZone:=ATimeZone;
end;


INITIALIZATION
  FRotX:=0.0; FRotY:=0.0; FRotZ:=0.0;
  TrakLoLimit:=78.5;

FINALIZATION

END.
