UNIT TrkCtrlInternal;
INTERFACE uses RxBinPktCom, ExProtclInternal,
  AxisInternal,
  Windows,  
  RxAlias;

type TTrkCtrlInternal = class(TRxBinPktCom)
  private
    IPacket:TIPacket;
    OPacket:TOPacket;
    IPacketLen,OPacketLen:U16;
    FTime:U32;
    FAz,FEl:F64;
    FPwr,FBrk,FHold,FTrk:BOOL;
    FTrackMode:U8;
    FSigLev:F64;
    FID:U8;
    FAdr:U8;
    FTheta:F64;
    FPsi:F64;
    FAlfa:F64;
    FNP:BOOL;
    FNextPnt:U16;
    procedure SetAlfa(const Value:F64);
    procedure SetPsi(const Value:F64);
    procedure SetTheta(const Value:F64);
  public
    Axis:array[1..3] of TAxisInternal;
    constructor Create();
    destructor Destroy(); override;
    function DecodePacket():U8; override;
    //
    procedure GetStat();
    procedure Stop();
    procedure PowerOn;
    procedure PowerOff;
    procedure MoveBy3Speed(ASpd1,ASpd2,ASpd3: F64);
    procedure SetTime(ATime:TDateTime);
    procedure LoadTrackHeader(AutoTrak,AutoSearch:BOOL; AStartTime:TDateTime; ANumOfPnts:U16; AAz,AEl:F64);
    procedure LoadTrackPoint(ANumOfPnt:U16; AAz,AEl:F64);
    procedure LoadTrackEnd();
    procedure SetCorrection(const AzCor,ElCor:F64; const TimeCor:S32);
    procedure LoadTrkHdr(const AutoTrak,AutoSearch:BOOL; const ATrkType:U8; const AStartTime:U32; const K10,K20,K30, K11,K21,K31, K12,K22,K32:F32; const AFstPnt:U16);
    procedure LoadTrkPnt(const AK1,AK2,AK3:F32);
    //
    procedure LoadTrkHdrToFile(const ATrkFileName:string; const AutoTrak,AutoSearch:BOOL; const ATrkType:U8; const AStartTime:U32; const ANumOfPnts: U16; const AK1,AK2,AK3:F32);
    procedure LoadTrkPntToFile(const  ANumOfPnt:U16; const AK1,AK2,AK3:F32);
    procedure LoadTrkEndToFile;
    //
    procedure TracingOn();
    procedure TracingOff();
    //
    procedure GetData(var Time:U32; var Az,El,SigLev:F64; var Pwr,Brk,Hold,NP:BOOL; var TrackMode:U8; var NextPnt:U16);
    property AnsID:U8 read FID;
    //
    property Alfa:F64 read FAlfa write SetAlfa;
    property Theta:F64 read FTheta write SetTheta;
    property Psi:F64 read FPsi write SetPsi;
end;

IMPLEMENTATION uses SysUtils, DateUtils; //, KMath;

constructor TTrkCtrlInternal.Create;
var I:Byte;
begin
  inherited Create();

  Chnl.Parity:=NOPARITY;
  Chnl.DataBits:=DATABITS_8;
  Chnl.StopBits:=ONESTOPBIT;
  Chnl.Timeout:=100;
  Buffer.FWriteQueue:=True;
  for I:=1 to 3 do begin
    Axis[I]:=TAxisInternal.Create(I);
    Axis[I].SendPacket:=SendPacket;
  end;
  FAdr:=0;
end;

function TTrkCtrlInternal.DecodePacket():U8;
var I:Byte;
begin
  Result:=0;
  
  if not GetPacket(@IPacket,IPacketLen, SizeOf(IPacket)) then Exit;

  case IPacket.Ans of
    ANS_STAT:begin
       Result:=1;
       for I:=1 to 3 do Axis[I].DecodePacket(IPacket,IPacketLen);
       with IPacket.Stat do begin
          FTime:=Stat.Time;
          //DecodeStat(Stat.Stat,FPwr,FBrk,FHold,FTrk,FTrackMode);
          DecodeStat(Stat.Stat,FPwr,FBrk,FHold,FNP,FTrackMode);
          FSigLev:=Stat.SigLev;
          FNextPnt:=Stat.NextPnt;
          //KinematicToAE(Axis[1].Pos,Axis[2].Pos,Axis[3].Pos,FAz,FEl);
          //HCK_To_CCK(Alfa,Theta,Psi,FAz,FEl);
       end;
    end;
    ANS_LIMITS:begin
      Result:=2;
      for I:=1 to 3 do Axis[I].DecodePacket(IPacket,IPacketLen);
    end;
    ANS_PARAM:begin
      Result:=3;
      for I:=1 to 3 do Axis[I].DecodePacket(IPacket,IPacketLen);
    end;
  end;
end;

destructor TTrkCtrlInternal.Destroy;
var I:Byte;
begin
  for I:=1 to 3 do Axis[I].Destroy();
  inherited Destroy();
end;

//procedure TTrkCtrl.GetData(var Time:TDateTime; var Az,El:F64; var SigLev:F64);
procedure TTrkCtrlInternal.GetData(var Time:U32; var Az,El,SigLev:F64; var Pwr,Brk,Hold,NP:BOOL; var TrackMode:U8; var NextPnt:U16);
begin
  Time:=FTime;
  Az:=FAz;
  El:=FEl;
  SigLev:=FSigLev;
  Pwr:=FPwr;
  Brk:=FBrk;
  Hold:=FHold;
  NP:=FNP;
  TrackMode:=FTrackMode;
  NextPnt:=FNextPnt;
end;

procedure TTrkCtrlInternal.GetStat();
begin
  with OPacket.GetStat do begin
    Cmd:=CMD_GETSTAT;
  end;
  OPacketLen:=SizeOf(TGetStat);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.Stop();
begin
  with OPacket.Stop do begin
    Cmd:=CMD_STOP;
  end;
  OPacketLen:=SizeOf(TStop);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.SetTime(ATime:TDateTime);
begin
  with OPacket.SetTime do begin
    Cmd:=CMD_SETTIME;
    Time:=Abs(DateTimeToTimeStamp(ATime).Time);
  end;
  OPacketLen:=SizeOf(TSetTime);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.MoveBy3Speed(ASpd1,ASpd2,ASpd3:F64);
begin
  with OPacket.MoveBy3Speed do begin
    Cmd:=CMD_MOVEBY3SPEED;
    Spd[1]:=ASpd1;
    Spd[2]:=ASpd2;
    Spd[3]:=ASpd3;
  end;
  OPacketLen:=SizeOf(TMoveBy3Speed);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.LoadTrackHeader(AutoTrak,AutoSearch:BOOL; AStartTime:TDateTime; ANumOfPnts:U16; AAz,AEl:F64);
var A,E:F64;
begin
  with OPacket.LoadTrackHeader do begin
    Cmd:=CMD_AELOADTRKHDR;
    AutoTrk:=$00;
    if AutoTrak   then AutoTrk:=AutoTrk or ATS_AT;
    if AutoSearch then AutoTrk:=AutoTrk or ATS_AS;
    StartTime:=Abs(DateTimeToTimeStamp(AStartTime).Time);
    NumOfPnts:=ANumOfPnts;
    A:=AAz;
    E:=AEl;
    //CCK_To_HCK(Alfa,Theta,Psi,A,E);
    Az:=A;
    El:=E;
  end;
  OPacketLen:=SizeOf(TLoadTrackHeader);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.LoadTrackPoint(ANumOfPnt:U16; AAz,AEl:F64);
var A,E:F64;
begin
  with OPacket.LoadTrackPoint do begin
    Cmd:=CMD_AELOADTRKPNT;
    NumOfPnt:=ANumOfPnt;
    A:=AAz;
    E:=AEl;
    //CCK_To_HCK(Alfa,Theta,Psi,A,E);
    Az:=A;
    El:=E;
  end;
  OPacketLen:=SizeOf(TLoadTrackPoint);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.LoadTrackEnd();
begin
  with OPacket.LoadTrackEnd do begin
    Cmd:=CMD_AELOADTRKEND;
  end;
  OPacketLen:=SizeOf(TLoadTrackEnd);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.SetCorrection(const AzCor,ElCor:F64; const TimeCor:S32);
begin
  with OPacket.SetCorrection do begin
    Cmd:=CMD_SETCORRECTION;
    Az:=AzCor;
    El:=ElCor;
    Time:=TimeCor;
  end;
  OPacketLen:=SizeOf(TSetCorrection);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.PowerOff;
begin
  with OPacket.PwrOnOff do begin
     Cmd:=CMD_POWEROFF;
  end;
  OPacketLen:=SizeOf(TPwrOnOff);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.PowerOn;
begin
  with OPacket.PwrOnOff do begin
     Cmd:=CMD_POWERON;
  end;
  OPacketLen:=SizeOf(TPwrOnOff);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.SetAlfa(const Value:F64);
begin
  FAlfa := Value;
end;

procedure TTrkCtrlInternal.SetPsi(const Value:F64);
begin
  FPsi := Value;
end;

procedure TTrkCtrlInternal.SetTheta(const Value:F64);
begin
  FTheta := Value;
end;

procedure TTrkCtrlInternal.TracingOff;
begin
 //FPort.Tracing:=tlOff;
end;

procedure TTrkCtrlInternal.TracingOn;
begin
 //FPort.Tracing:=tlOn;
end;

procedure TTrkCtrlInternal.LoadTrkHdr(const AutoTrak,AutoSearch:BOOL; const ATrkType:U8; const AStartTime:U32; const K10, K20, K30, K11, K21, K31, K12, K22, K32:F32; const AFstPnt:U16);
begin
  with OPacket.LoadTrackHdr do begin
    Cmd:=CMD_LOADTRKHDR;
    AutoTrk:=$00;
    if AutoTrak   then AutoTrk:=AutoTrk or ATS_AT;
    if AutoSearch then AutoTrk:=AutoTrk or ATS_AS;
    TrkType:=ATrkType;
    StartTime:=AStartTime;
    K1.Pnt[1]:=K10; K1.Pnt[2]:=K11; K1.Pnt[3]:=K12;
    K2.Pnt[1]:=K20; K2.Pnt[2]:=K21; K2.Pnt[3]:=K22;
    K3.Pnt[1]:=K30; K3.Pnt[2]:=K31; K3.Pnt[3]:=K32;
    FstPnt:=AFstPnt;
  end;
  OPacketLen:=SizeOf(TLoadTrackHdr);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.LoadTrkPnt(const AK1, AK2, AK3:F32);
begin
  with OPacket.LoadTrackPnt do begin
    Cmd:=CMD_LOADTRKPNT;
    K1:=AK1; K2:=AK2; K3:=AK3;
  end;
  OPacketLen:=SizeOf(TLoadTrackPnt);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.LoadTrkHdrToFile(const ATrkFileName:string; const AutoTrak,AutoSearch:BOOL; const ATrkType:U8; const AStartTime:U32; const ANumOfPnts:U16; const AK1,AK2,AK3:F32);
var I:U8;
begin
  with OPacket.LoadTrkHdrToFile do begin
    Cmd:=CMD_LOADTRKHDRTOFILE;
    //for I:=1 to 7 do FileName[I]:=#0;
    AutoTrk:=$00;
    if AutoTrak   then AutoTrk:=AutoTrk or ATS_AT;
    if AutoSearch then AutoTrk:=AutoTrk or ATS_AS;
    TrkType:=ATrkType;
    StartTime:=AStartTime;
    NumOfPnts:=ANumOfPnts;
    K1:=AK1; K2:=AK2; K3:=AK3;
  end;
  OPacketLen:=SizeOf(TLoadTrkHdrToFile);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.LoadTrkPntToFile(const ANumOfPnt:U16; const AK1,AK2,AK3:F32);
begin
  with OPacket.LoadTrkPntToFile do begin
    Cmd:=CMD_LOADTRKPNTTOFILE;
    NumOfPnt:=ANumOfPnt;
    K1:=AK1; K2:=AK2; K3:=AK3;
  end;
  OPacketLen:=SizeOf(TLoadTrkPntToFile);
  SendPacket(OPacket,OPacketLen);
end;

procedure TTrkCtrlInternal.LoadTrkEndToFile;
begin
  with OPacket.LoadTrkEndToFile do begin
    Cmd:=CMD_LOADTRKENDTOFILE;
  end;
  OPacketLen:=SizeOf(TLoadTrkEndToFile);
  SendPacket(OPacket,OPacketLen);
end;

END.
