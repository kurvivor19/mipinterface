UNIT SmartWinner;
INTERFACE uses  SysUtils, Classes,
  Windows,
  //CentralionProtcl,
  RxBinPktCom,
  RxStrPktCom,
  RxUtils,
  RxAlias;
  
type
  TUPSMode = ( upsmNormal, upsmAVR, upsmNone );
  TUPSType = ( upstOnLine, upstLineInteractive, upstNone );

type TStatusInquire = record
  IPV:F32;    // Input Voltage [V]
  IPFV:F32;   // Input Fault Voltage [V]
  OPV:F32;    // Output Voltage [V]
  OPC:U8;     // Output Current(Load) [%]
  OPF:F32;    // Output Frequency [Hz]
  BATV:F32;   // Battery Voltage [V]
  TEMPC:F32;  // Temperature [deg C]
  // <U> = B[7..0] 
  UF:BOOL;        // UtilityFail
  BATLOW:BOOL;    // BatteryLow:
  UPSM:TUPSMode;  // UPS Mode 0-Normal 1-AVR (Automatic Voltage Regulation)
  UPSF:BOOL;      // UPSFailed
  UPST:TUPSType;  // UPSType  0-Line-Interactive 1-On Line
  TST:BOOL;       // TestInProgress  (Test for 10 sec)
  SHTDWN:BOOL;    // ShutdownActive
  BEEPON:BOOL;    // BeeperOn
  SOC:U8;
end;

type TRatingInfo = record
  RV:F32;   // Rating Voltage [V]
  RC:U8;    // Rating Current(Load) [%]
  BATV:F32; // Battery Voltage [V]
  FREQ:F32; // Frequency [Hz]
end;

const
  UPS_GET_STATUS_INQUIRY          = 'Q1' + CR;
  UPS_GET_BATTERY_LEVEL           = 'BL?'+ CR;
  UPS_TEST_FOR_10_SECONDS         = 'T' + CR;
  UPS_SHUTDOWN_NOW                = 'S.2R0000' + CR; // S<n>R<m> 8sec -Shutdown NoRestore
  UPS_CANCEL_SHUTDOWN             = 'C' + CR;
  //
  UPS_DETECT_CENTRALION_PROTOCOL  = 'M' + CR;
  UPS_GET_RATING_INFORMATION      = 'F' + CR;
  UPS_TURN_ON_OFF_BEEP            = 'Q' + CR;
  // N/A
  UPS_TEST_UNTIL_BATTTERY_LOW     = 'TL'+ CR;
  UPS_CANCEL_TEST                 = 'CT'+ CR;  

type
  TUPSState = ( upsDisconnected, upsConnected, upsOff, upsOn, upsWaitShutdown, upsShutdown, upsNoAnswered );
  TSmartWinnerDataAvaliable = procedure(Sender:TObject; ET:U16 ) of object;

const
  UPS_MAX_STATE = U16(High(TUPSState));

type TSmartWinner = class(TRxStrPktCom)
  private
    FOnDataAvaliable:TSmartWinnerDataAvaliable;
    FAutoDetect: BOOL;
    FET: U16;
  protected
    //State:U8;
    FCnt,FSRCnt:U16;
    ACmd:U8;
    AnsID:U8;
    //
    //IPacket:string;
    FStatusInquire:TStatusInquire;
    FRatingInfo:TRatingInfo;
    FUPSState:TUPSState;
    procedure ProcessData(); virtual;
    function  DecodePacket():U8; override;
  public
    State:U8;
    IPacket,OPacket:string;
    constructor Create(AOwner:TComponent; AStandAlone:BOOL); // override;
    function  Connect():BOOL; override;
    function Disconnect():BOOL; override;
    procedure ControlThread(); override;
    //
    procedure Test();
    procedure ToggleBeeper();
    procedure Shutdown();
    procedure Restore();
    //
    property ET:U16 read FET;
    property StatusInquire:TStatusInquire read FStatusInquire;
    property RatingInfo:TRatingInfo read FRatingInfo;
    property AutoDetect:BOOL read FAutoDetect write FAutoDetect default False;
    property UPSState:TUPSState read FUPSState;
    property OnDataAvaliable:TSmartWinnerDataAvaliable read FOnDataAvaliable write FOnDataAvaliable default NIL;         
end;

const
  ANS_NOANSWER                      = 0;
  ANS_CENTRALION_PROTOCOL_DETECTED  = 1;
  ANS_RATING_INFORMATION            = 2;
  ANS_STATUS_INQUIRY                = 3;
  ANS_STATUS_INQUIRY_DATA_ERROR     = 4;
  ANS_RATING_INFORMATION_DATA_ERROR = 5;
  ANS_UNKNOWN                       = 6;
  ANS_NONEEDED                      = 7;

const
  NO_ANSWERE                        = 10;
  NO_ANSWERED                       = 11;


IMPLEMENTATION uses StrUtils; 

const
  UPS_MAX_REQUESTED = 15;

constructor TSmartWinner.Create(AOwner:TComponent; AStandAlone:BOOL);
begin
  inherited Create(AOwner);
  Chnl.Baud:=2400;
  Chnl.Parity:=NOPARITY;
  Chnl.DataBits:=DATABITS_8;
  Chnl.StopBits:=ONESTOPBIT;
  Chnl.StopChar:=CR;
  Chnl.Timeout:=500;
  //with FPort do begin
    {$IFDEF WIN32}
  //  DataBits:=8;
  //  Parity:=pNone;
  //  StopBits:=1;
    //Logging:=tlOn;
    //Tracing:=tlOn;
    {$ENDIF}
    {$IFDEF LINUX}
  //  DataBits:=dbEight;
  //  Parity:=pNone;
  //  StopBits:=sbOne;
    {$ENDIF}
  //end;

  //CreateDataPacketsList(1);
  //with FData[0] do begin
  //  StartCond:=scAnyData;
  //  EndCond:=[ecString];
  //  EndString:=CR;
  //  IncludeStrings:=True;
  //end;

  //if AStandAlone then TimerInit(1000);
  if AStandAlone then TimerInit(1000);

  State:=0;
  FCnt:=0;
  ACmd:=0;
  FUPSState:=upsDisconnected;
end;

procedure TSmartWinner.ControlThread();
var ET:U16;
begin
  ET:=ANS_NOANSWER;
  FOPacket:='';
  //if not FPort.Open then Exit;

  case State of
    0:begin
      //OData:=UPS_DETECT_CENTRALION_PROTOCOL;
      //State:=1;
      FOPacket:=UPS_GET_RATING_INFORMATION;
      State:=2;
      //State:=9;
    end;
    1:begin
      if AnsID = ANS_CENTRALION_PROTOCOL_DETECTED then begin
         FOPacket:=UPS_GET_RATING_INFORMATION;
         ET:=AnsID;
         FUPSState:=upsConnected;
         State:=2;
      end else AnsID:=ANS_NOANSWER;
    end;
    2:begin
      if AnsID = ANS_RATING_INFORMATION then begin
         ET:=AnsID;
         FOPacket:=UPS_GET_STATUS_INQUIRY;
         FUPSState:=upsConnected;
         //State:=3; if AutoDetect then State:=9;
         State:=9;
      end else AnsID:=ANS_NOANSWER;
    end;
    // Status Inquire (General Loop)
    3:begin
      //FOPacket:=UPS_GET_STATUS_INQUIRY;
      FOPacket:=UPS_GET_BATTERY_LEVEL; State:=10;
      if AnsID <> ANS_STATUS_INQUIRY then AnsID:=ANS_NOANSWER
      else begin
        case FUPSState of
          upsOff:if not FStatusInquire.SHTDWN then FUPSState:=upsOn;
          upsOn: if FStatusInquire.SHTDWN then FUPSState:=upsShutdown;
          upsShutdown: if not FStatusInquire.SHTDWN then FUPSState:=upsOn;
        end;
      end;
      ET:=AnsID;
      if ACmd <> 0 then begin
         FOPacket:=FAOPacket;
         case ACmd of
           1,2:State:=4; // T, Q
           3:if FUPSState = upsOff then FOPacket:=UPS_GET_STATUS_INQUIRY
                                   else State:=5;   // S.2R0000
           4:State:=7;   // C
         end;
         ACmd:=0;
      end;
    end;
    // Battery Level
    10:begin
      FOPacket:=UPS_GET_STATUS_INQUIRY; State:=3;
      if ACmd <> 0 then begin
         FOPacket:=FAOPacket;
         case ACmd of
           1,2:State:=4; // T, Q
           3:if FUPSState = upsOff then FOPacket:=UPS_GET_STATUS_INQUIRY
                                   else State:=5;   // S.2R0000
           4:State:=7;   // C
         end;
         ACmd:=0;
      end;      
    end;
    // Test, ToggleBeeper
    4:begin;
      AnsID:=ANS_NONEEDED;
      ET:=AnsID;
      FOPacket:=UPS_GET_STATUS_INQUIRY;
      State:=3;
    end;
    // ShutDown
    5:begin;
      FSRCnt:=0;
      AnsID:=ANS_NONEEDED;
      ET:=AnsID;
      FOPacket:=UPS_GET_STATUS_INQUIRY;
      FUPSState:=upsWaitShutdown;
      State:=6;
    end;        
    6:begin
      Inc(FSRCnt);
      FOPacket:=UPS_GET_STATUS_INQUIRY;
      if AnsID <> ANS_STATUS_INQUIRY then AnsID:=ANS_NOANSWER;
      ET:=AnsID;
      if (FStatusInquire.SHTDWN) then begin
         State:=3;
         FUPSState:=upsShutdown;
      end else
        if FSRCnt > 14 then begin
          FOPacket:=UPS_SHUTDOWN_NOW;
          State:=5;
        end;
    end;
    // Resore (Cancel ShutDown)
    7:begin;
      FSRCnt:=0;
      AnsID:=ANS_NONEEDED;
      ET:=AnsID;
      FOPacket:=UPS_GET_STATUS_INQUIRY;
      State:=8;
    end;
    8:begin
      Inc(FSRCnt);
      FOPacket:=UPS_GET_STATUS_INQUIRY;
      if AnsID <> ANS_STATUS_INQUIRY then AnsID:=ANS_NOANSWER;
      ET:=AnsID;
      if not (FStatusInquire.SHTDWN) then begin
        FUPSState:=upsOn;
        State:=3;
      end else begin
        if FSRCnt > ANS_NOANSWER then begin
           FSRCnt:=ANS_NOANSWER;
           FUPSState:=upsOff;
        end;
        FOPacket:=UPS_CANCEL_SHUTDOWN;
        State:=7;
      end;
    end;

    // Detect UPS State
    9:begin
      FOPacket:=UPS_GET_STATUS_INQUIRY;
      if AnsID <> ANS_STATUS_INQUIRY then AnsID:=ANS_NOANSWER
      else begin 
        if not (FStatusInquire.SHTDWN) then FUPSState:=upsOn
                                       else FUPSState:=upsShutdown;
        State:=3;
       end;
      ET:=AnsID;
    end;

  end;

  if AnsID = ANS_NOANSWER then begin
    if FCnt < UPS_MAX_REQUESTED then Inc(FCnt) else FCnt:=UPS_MAX_REQUESTED;
  end else
    if AnsID <> ANS_NONEEDED then FCnt:=0;

  if FCnt = UPS_MAX_REQUESTED then begin
    State:=0;
    ACmd:=0;
    ET:=NO_ANSWERED;
    FUPSState:=upsNoAnswered;
  end;

  ProcessData();

  FET:=ET;

  if Assigned(OnDataAvaliable) then OnDataAvaliable(Self,ET);

  AnsID:=ANS_NOANSWER;
  OPacket:=FOPacket;
  if FOPacket <> '' then SendPacket(FOPacket);
end;

procedure TSmartWinner.ProcessData();
begin
end;


function TSmartWinner.DecodePacket():U8;
begin
  AnsID:=ANS_NOANSWER;
  IPacket:=InBuffer.FIPacket;
  if IPacket = '' then Exit;
  case IPacket[1] of
    '(':begin
      if Length(IPacket) <> 47 then begin
        AnsID:=ANS_STATUS_INQUIRY_DATA_ERROR;
        IPacket:='';
        Exit;
      end;
      Field.Text:=RightStr(IPacket, Length(IPacket)-1);
      Field.Text:=AnsiReplaceStr(Field.Text, '.', DecimalSeparator);
      Field.Text:=AnsiReplaceStr(Field.Text, SPC, CRLF);
      if (Field.Count <> 8) then begin
        AnsID:=ANS_STATUS_INQUIRY_DATA_ERROR;
        IPacket:='';
        Exit;
      end;
      try
        with FStatusInquire do begin
          IPV:=   StrToFloat(Field[0]);
          IPFV:=  StrToFloat(Field[1]);
          OPV:=   StrToFloat(Field[2]);
          OPC:=   StrToInt(  Field[3]);
          OPF:=   StrToFloat(Field[4]);
          BATV:=  StrToFloat(Field[5]);
          TEMPC:= StrToFloat(Field[6]);
          //
          UF:=    (StrToInt(Field[7][1])) <> 0;
          BATLOW:=(StrToInt(Field[7][2])) <> 0;
          UPSM:=  TUPSMode(StrToInt(Field[7][3]));
          UPSF:=  (StrToInt(Field[7][4])) <> 0;
          UPST:=  TUPSType(StrToInt(Field[7][5]));
          TST:=   (StrToInt(Field[7][6])) <> 0;
          SHTDWN:=(StrToInt(Field[7][7])) <> 0;
          BEEPON:=(StrToInt(Field[7][8])) <> 0;
        end;
      except
        on EConvertError do begin
           AnsID:=ANS_STATUS_INQUIRY_DATA_ERROR;
           IPacket:='';
        end;
      end;
      //IPacket:=FIPacket;
      AnsID:=ANS_STATUS_INQUIRY;
    end;
    '#':begin
      if Length(IPacket) <> 22 then begin
        AnsID:=ANS_RATING_INFORMATION_DATA_ERROR;
        IPacket:='';
        Exit;
      end;
      Field.Text:=RightStr(IPacket, Length(IPacket)-1);
      Field.Text:=AnsiReplaceStr(Field.Text, '.', DecimalSeparator);
      Field.Text:=AnsiReplaceStr(Field.Text, SPC, CRLF);
      if (Field.Count <> 4) then begin
        AnsID:=ANS_RATING_INFORMATION_DATA_ERROR;
        IPacket:='';
        Exit;
      end;
      try
        with FRatingInfo do begin
          RV:=   StrToFloat(Field[0]);
          RC:=   StrToInt(  Field[1]);
          BATV:= StrToFloat(Field[2]);
          FREQ:= StrToFloat(Field[3]);
        end;
      except
        on EConvertError do begin
           AnsID:=ANS_RATING_INFORMATION_DATA_ERROR;
           IPacket:='';
        end;
      end;
      //IPacket:=FIPacket;
      AnsID:=ANS_RATING_INFORMATION;
    end;
    'C':begin
      if (Length(IPacket) <> 1)  then begin
        IPacket:='';
        Exit;
      end;
      AnsID:=ANS_CENTRALION_PROTOCOL_DETECTED;
    end;
    '0','1':begin
     if (Length(IPacket) = 4) and (IPacket[2] in ['0'..'9']) and (IPacket[3] in ['0'..'9']) then begin
       try FStatusInquire.SOC:=StrToInt(LeftStr(IPacket, Length(IPacket)-1));
       except
         on EConvertError do IPacket:='';
       end;  
     end else begin
        IPacket:='';
        Exit;
     end;
    end
      
    else begin
      AnsID:=ANS_UNKNOWN;
    end;
  end;

  InBuffer.FIPacket:='';
end;

procedure TSmartWinner.Test();
begin
  FAOPacket:=UPS_TEST_FOR_10_SECONDS;
  ACmd:=1;
end;

procedure TSmartWinner.ToggleBeeper();
begin
  FAOPacket:=UPS_TURN_ON_OFF_BEEP;
  ACmd:=2;
end;

procedure TSmartWinner.Shutdown();
begin
  FAOPacket:=UPS_SHUTDOWN_NOW;
  ACmd:=3;
end;

procedure TSmartWinner.Restore();
begin
  FAOPacket:=UPS_CANCEL_SHUTDOWN;
  ACmd:=4;
end;

function TSmartWinner.Connect(): BOOL;
begin
  Result:=inherited Connect();
end;

function TSmartWinner.Disconnect():BOOL;
begin
  Result:=inherited Disconnect();
  State:=0;
  FCnt:=0;
  ACmd:=0;
  FUPSState:=upsDisconnected;
  if Assigned(OnDataAvaliable) then OnDataAvaliable(Self,NO_ANSWERED); 
end;

END.
