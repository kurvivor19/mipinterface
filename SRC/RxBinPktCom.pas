UNIT RxBinPktCom;
INTERFACE uses SysUtils, Classes, QExtCtrls, ExProtclInternal,
  Windows,
  Contnrs,
  RxChkSum,
  RxUtils,
  RxAlias;

type PRxIOPacket = ^TRxIOPacket;
     TRxIOPacket = packed array[0..1023*2] of U8;

     TRxPktRcvState = ( tbSOP, tbFIRSTCHAR, tbBYTESTUFF, tbEOP );

     TChnlRcvState = ( csDisconnected, csConnected );

type TComChnl = record
  Num:U8;
  DeviceName:TDeviceName;
  Baud:U32;
  Parity:U32;   // must use system constant!
  DataBits:U8;  // must use system constant!
  StopBits:U8;  // must use system constant!
  Timeout:Cardinal;
  StartChar:Char;
  PacketSize:U8;
  StopChar:Char;
  RcvState:TChnlRcvState;
  IsActive:BOOL;
end;

const
  MaxInputQueueSize=10;

type TComBuffer = record
  HasDataAvaiable: boolean;
  TimeoutOccured: boolean;
  FIPacket: string;
  FIPackets: array[1..MaxInputQueueSize] of string;
  ReadIndex, WriteIndex: integer;
  FWriteQueue: boolean;
end;

function AddToBuffer(var Buffer:TComBuffer; Item: string): boolean;
function GetFromBuffer(var Buffer:TComBuffer; var Item: string): boolean;
function GetPacketCount(var Buffer:TComBuffer): integer;

type TReadBinCommThread = class(TThread)
  public
    CommHandle:DWORD;                   // COM port handle
    ComMutex:DWORD;
    Buffer: ^TComBuffer;
    ChnlData: ^TComChnl;
  protected
    procedure Execute; override;
end;

type TRxBinPktCom = class(TComponent)
  private
    procedure SetReqPerSec(const Value: U16);
    // procedure SetTrace(const Value: string);
  protected
    CommHandle:DWORD;                    // COM port handle
    ComMutex:DWORD;
    DCB:TDCB;
    hEvent: THandle;
    ReadThread: TReadBinCommThread;      // TThread that performs reasing
    OvrWrite: TOverlapped;               // OVERLAPPED structure for writing
    FTimer:TTimer;
    Buffer: TComBuffer;
    FIPacket,FOPacket:TRxIOPacket;
    FIPacketLen,FOPacketLen:U16;
    FEnabled:BOOL;
    FCSType:TCSType;
    RcvState:TRxPktRcvState;
    DataCnt:U16;
    FReqPerSec:U16;

    FCurComIndex:Integer;
    FDiscconected:BOOL;
    FStandAlone:BOOL;
    function GetCurChnl:TComChnl;
    function GetCheckSum(var Packet; const Len:U16):U8;
    function GetBaud:U32;
    function GetDeviceName:TDeviceName;
    procedure SetDeviceName(const Value:TDeviceName);
    procedure SetBaud(const Value:U32);
    //procedure FPortTriggerTimer(CP:TObject; TriggerHandle:Word);
    procedure FTimerTimer(Sender: TObject);
    //
    procedure TimerInit(Value:U16);
    procedure TimerStart();
    procedure TimerStop();
    //
    function  GetPacket(Packet:Pointer; var Len:U16; MaxLen:U16):BOOL;
    function  DecodePacket():U8; virtual; abstract;
    //
    property Enabled:BOOL read FEnabled write FEnabled default False;
  public
    Chnl:TComChnl;
    constructor Create(); //override;
    destructor  Destroy(); override;
    function  Connect():BOOL; virtual;
    //
    procedure SendLastPacket();
    procedure Disconnect(); virtual;
    procedure ControlThread(); virtual;
    procedure SignalTimeout(); virtual;
    procedure SendPacket(var Packet; const Len:U16);
    //
    property CSType:TCSType read FCSType write FCSType default csCRC8;
    property CurChnl:TComChnl read GetCurChnl;
    property Baud:U32 read GetBaud write SetBaud;
    property DeviceName:TDeviceName read GetDeviceName write SetDeviceName;
    property ReqPerSec:U16 read FReqPerSec write SetReqPerSec;

end;

//function GetOverlappedResultEx(hFile: THandle; const lpOverlapped: TOverlapped;
//  var lpNumberOfBytesTransferred: DWORD; dwMilliseconds:DWORD; bWait: BOOL): BOOL; stdcall;

IMPLEMENTATION //uses RxChkSum;

const
  SOP = $7E;
  EOP = $7E;
  STUFFBYTE = $7D;

const
  kernel32  = 'kernel32.dll';
//function GetOverlappedResultEx; external kernel32 name 'GetOverlappedResultEx';

function AddToBuffer(var Buffer:TComBuffer; Item: string): boolean;
begin
  Result:=False;
  if Buffer.ReadIndex mod MaxInputQueueSize = (Buffer.WriteIndex + 1) mod MaxInputQueueSize then Exit;
  Result:=True;
  Buffer.FIPackets[Buffer.WriteIndex]:=Item;
  Buffer.WriteIndex:=(Buffer.WriteIndex+1) mod MaxInputQueueSize;
  if Buffer.WriteIndex = 0 then Buffer.WriteIndex:=MaxInputQueueSize;
end;

function GetFromBuffer(var Buffer:TComBuffer; var Item: string): boolean;
begin
  Result:=False;
  if Buffer.ReadIndex = Buffer.WriteIndex then Exit;
  Result:=True;
  Item:=Buffer.FIPackets[Buffer.ReadIndex];
  Buffer.ReadIndex:=(Buffer.ReadIndex+1) mod MaxInputQueueSize;
  if Buffer.ReadIndex = 0 then Buffer.ReadIndex:=MaxInputQueueSize;
end;

function GetPacketCount(var Buffer:TComBuffer): integer;
var Temp:integer;
begin
  Temp:=Buffer.WriteIndex;
  if Temp < Buffer.ReadIndex then Temp:=Temp + MaxInputQueueSize;
  Result:=Temp - Buffer.ReadIndex;
end;

procedure TReadBinCommThread.Execute;
var
  Recieve, Processed: TRxIOPacket;
  PRecieve: PChar;
  TransMask, ComStatErrs, Kols, Waiter: DWORD;
  Offset, PacketEnd: Integer;
  ReadForward: Cardinal;
  SysErrsFlag: LongBool;
  OvrComm, OvrRead: Overlapped;
  evHandles: TWOHandleArray;//array [0..1] of DWORD;
  Stat : TComStat;
  I, DataCnt, J:Integer;
  TempString: ^String;
  RcvState: TRxPktRcvState;
begin
  //get mutex for synchronious termination
  WaitForSingleObject(ComMutex, INFINITE);
  Buffer^.HasDataAvaiable:=False;
  Buffer^.TimeoutOccured:=False;
  //create event handles
  evHandles[0]:=CreateEvent(nil, False, False, nil);
  evHandles[1]:=CreateEvent(nil, False, False, nil);
  FillChar(OvrComm, SizeOf(OvrComm), #0);
  FillChar(OvrRead, SizeOf(OvrRead), #0);
  FillChar(Recieve, SizeOf(Recieve), #0);
  FillChar(Processed, SizeOf(Processed), #0);
  OvrComm.hEvent:=evHandles[0];
  OvrRead.hEvent:=evHandles[1];
  TransMask:=0;
  SysErrsFlag:=WaitCommEvent(CommHandle,TransMask,@OvrComm); //����
  //if SysErrsFlag = False then begin
  //  ComStatErrs:=GetLastError;
  //  OutputDebugString(Pansichar(SysErrorMessage(ComStatErrs)));
  //  case ComStatErrs of
  //    ERROR_IO_PENDING:
  //  end;
  //end;
  ReadForward:=256;
  Offset:=0;
  RcvState := tbSOP;
  DataCnt := 0;
  // here we wait and error is discarded
  repeat
    Waiter:=WaitForMultipleObjects(2, @evHandles, False, ChnlData^.Timeout);
    case Waiter of
    WAIT_OBJECT_0 + 0: begin //State retrieval completed
      if TransMask = 0 then // error
      begin
        // handle error properly?..
        // wait timeout in particular
        SysErrsFlag:=ClearCommError(CommHandle,ComStatErrs,@Stat);
        if SysErrsFlag = False then begin
          ComStatErrs:=GetLastError;
          //OutputDebugString(Pansichar(SysErrorMessage(ComStatErrs)));
          case ComStatErrs of
            ERROR_IO_PENDING: continue; // now we should not get in here
            else begin
              // todo: hande particluar error
              // ideally, we only get here in case of timeout
              Buffer^.TimeoutOccured:=True;

            end;
          end;
        end;
        // now we continue to wait
        WaitCommEvent(CommHandle,TransMask,@OvrComm);
        continue;
      end
      else begin
        // presumably this is a correct event
        Buffer^.TimeoutOccured:=False;
        SysErrsFlag:=ClearCommError(CommHandle,ComStatErrs,@Stat);
        PRecieve:=@Recieve[Offset];
        if SysErrsFlag = False then begin
          ComStatErrs:=GetLastError;
          //OutputDebugString(Pansichar(SysErrorMessage(ComStatErrs)));
          case ComStatErrs of
            ERROR_IO_PENDING: continue; // now we should not get in here
            else begin
              // todo: hande particluar error
              // ideally, we only get here in case of timeout
              Buffer^.TimeoutOccured:=True;
            end;
          end;
        end;
        ReadFile(CommHandle,PRecieve^,Stat.cbInQue,Kols,@OvrRead);// start to read
        continue;
      end; // end case of WaitCommEvent
    end;
    WAIT_OBJECT_0 + 1: begin
      GetOverlappedResult(CommHandle, OvrRead, Kols, True);
      //OutputDebugString(PChar('From COM port ' + IntToStr(Kols) + ' bytes were read'));
      if (Kols = 0) and (Offset = 0) then begin
        // now we continue to wait
        WaitCommEvent(CommHandle,TransMask,@OvrComm);
        continue;
      end
      else begin
        // move forward with offset we have
        Offset:=Offset + Kols;
        // we search from the beginning
        repeat
          for I:=0 to Offset-1 do begin
            case RcvState of
            tbSOP:begin
              if Recieve[I] = SOP then begin
                DataCnt:=0;
                RcvState:=tbFIRSTCHAR;
              end;
            end;
            tbFIRSTCHAR:begin
              if Recieve[I] <> SOP then begin
                if Recieve[I] = STUFFBYTE then RcvState:=tbBYTESTUFF
                else begin
                  Processed[DataCnt]:=Recieve[I];
                  Inc(DataCnt);
                  RcvState:=tbEOP;
                end;
              end;
            end;

            tbBYTESTUFF:begin
              Processed[DataCnt]:=Recieve[I] xor $20;
              Inc(DataCnt);
              RcvState:=tbEOP;
            end;
            tbEOP:begin
              if DataCnt > SizeOf(TRxIOPacket)-1 then begin
                RcvState:=tbSOP;
                DataCnt := 0;
              end;
              if Recieve[I] <> EOP then begin
                if Recieve[I] = STUFFBYTE then RcvState:=tbBYTESTUFF
                else begin
                  Processed[DataCnt]:=Recieve[I];
                  Inc(DataCnt);
                end;
              end else begin
                RcvState:=tbSOP;
                if DataCnt > 2 then begin; // we have a packet and we should process it
                  // OutputDebugString(Pansichar('Packet recieved'));
                  SetString(Buffer^.FIPacket, PChar(@Processed[0]), DataCnt);
                  if Buffer^.FWriteQueue then begin
                    AddToBuffer(Buffer^, Buffer^.FIPacket);
                  end;
                  for J := 0 to Offset - I - 2 do Recieve[J]:=Recieve[1+J+I];
                  Offset := Offset - I - 1;
                  for J := Offset to Sizeof(Recieve) do Recieve[J]:=0;
                  Buffer^.HasDataAvaiable:=True;
                break;
              end;
            end;
            end;
           end;
         end;
        until I >= (Offset - 1);
        PRecieve:=@Recieve[Offset];
        ReadFile(CommHandle,PRecieve^,ReadForward,Kols,@OvrRead);

        continue;
      end;
    end;// end Read case
    WAIT_TIMEOUT: begin
      Buffer^.TimeoutOccured:=True;
      // OutputDebugString(Pansichar('Timeout occured'));
      // now we continue to wait
      // WaitCommEvent(CommHandle,TransMask,@OvrComm);
      continue;
    end;
  end;// end CASE operator
  until Terminated;//while
  CloseHandle(evHandles[0]);
  CloseHandle(evHandles[1]);
  ReleaseMutex(ComMutex);
end;

function TRxBinPktCom.GetPacket(Packet:Pointer; var Len:U16; MaxLen:U16):BOOL;
var CS:U8;
  Temp: string;
begin
  Result:=False;
  if not Buffer.HasDataAvaiable then Exit;
  if Buffer.FWriteQueue then
  begin
    if not GetFromBuffer(Buffer, Temp) then
    begin
      Buffer.HasDataAvaiable:=GetPacketCount(Buffer) > 0;
      Exit;
    end;
    FIPacketLen:=Length(Temp);
    CopyMemory(@FIPacket[0], PAnsiChar(Temp), FIPacketLen);
  end
  else
  begin
    FIPacketLen:=Length(Buffer.FIPacket);
    CopyMemory(@FIPacket[0], PAnsiChar(Buffer.FIPacket), FIPacketLen);
  end;
  Buffer.HasDataAvaiable:=GetPacketCount(Buffer) > 0;
  CS:=GetCheckSum(FIPacket,FIPacketLen);
  if CS <> FIPacket[FIPacketLen] then Exit;
  // protection from access violations
  // CS is not part of data packet, so it is discarded => -1
  if FIPacketLen - 1 > MaxLen then Exit;
  CopyMemory(Packet, @FIPacket[0], FIPacketLen - 1);
  Len:=FIPacketLen;

  Result:=True;
end;

function TRxBinPktCom.GetCheckSum(var Packet; const Len:U16):U8;
begin
  case FCSType of
    csFFComplement:Result:=$FF - CheckSum(@Packet,Len);
    csCRC8: Result:=CheckSumCRC8(@Packet,Len);
  end;
end;

procedure TRxBinPktCom.SendPacket(var Packet; const Len:U16);
var
  I,K:U16; CS,Buf:U8;
  Written: Cardinal;
  Delay: Word;
begin

  if Len = 0 then Exit;

  // end pending operation
  if not GetOverlappedResult(CommHandle, OvrWrite, Written, False) then begin
    CancelIo(CommHandle);
  end;

  CS:=GetCheckSum(Packet,Len);

  K:=1;
  FOPacket[0]:=SOP;
  for I:=0 to Len-1 do begin
    Buf:=TRxIOPacket(Packet)[I];
    if Buf in [SOP,STUFFBYTE,EOP] then begin
       FOPacket[K]:=STUFFBYTE;
       Inc(K);
       FOPacket[K]:=Buf xor $20;
       Inc(K);
    end else begin
       FOPacket[K]:=Buf;
       Inc(K);
    end;
  end;

  if FOPacket[1] <> 0 then begin
    CS:=CS;
  end;

  if CS in [SOP,STUFFBYTE,EOP] then begin
     FOPacket[K]:=STUFFBYTE;
     Inc(K);
     FOPacket[K]:=CS xor $20;
     Inc(K);
   end else begin
     FOPacket[K]:=CS;
     Inc(K);
  end;
  FOPacket[K]:=EOP;
  FOPacketLen:=K+1;

  FillChar(OvrWrite, SizeOf(OvrWrite), #0);
  if CommHandle <> INVALID_HANDLE_VALUE then begin
    WriteFile(CommHandle, FOPacket, FOPacketLen, Written, POverlapped(@OvrWrite));
    FlushFileBuffers(CommHandle);
  end;
  Enabled:=True;
end;

constructor TRxBinPktCom.Create();
begin
  Chnl.Baud:=2400;
  Chnl.Num:=0;
  FTimer:=TTimer.Create(Self);
  FTimer.Enabled:=False;
  FTimer.OnTimer:=FTimerTimer;

  FCurComIndex:=1;
  CommHandle:=INVALID_HANDLE_VALUE;
  ComMutex:=CreateMutex(nil, False, nil);
  Buffer.ReadIndex:=1;
  Buffer.WriteIndex:=1;
  Buffer.FWriteQueue:=False;
  Buffer.HasDataAvaiable:=False;
  FCSType:=csCRC8;
end;

destructor TRxBinPktCom.Destroy;
begin
  Disconnect();
  inherited Destroy();
end;

procedure TRxBinPktCom.SendLastPacket;
var
  Written: Cardinal;
begin
  if FOPacketLen = 0 then Exit;
  // end pending operation
  if not GetOverlappedResult(CommHandle, OvrWrite, Written, False) then begin
    CancelIo(CommHandle);
  end;

  FillChar(OvrWrite, SizeOf(OvrWrite), #0);
  if CommHandle <> INVALID_HANDLE_VALUE then begin
    WriteFile(CommHandle, FOPacket, FOPacketLen, Written, POverlapped(@OvrWrite));
    FlushFileBuffers(CommHandle);
  end;
  Enabled:=True;
end;

procedure TRxBinPktCom.Disconnect;
var I:U8;
begin
  if FDiscconected then Exit;
  Chnl.RcvState:=csDisconnected;
  Chnl.IsActive:=False;
  if Assigned(ReadThread) then ReadThread.Terminate;
  // it is never wrong to stop the timer
  TimerStop();
  // now, wait until termination is complete
  WaitForSingleObject(ComMutex, INFINITE);
  ReleaseMutex(ComMutex);
  CloseHandle(CommHandle);
  CommHandle:=INVALID_HANDLE_VALUE;
  FDiscconected:=True;
end;

function TRxBinPktCom.Connect():BOOL;
var
  LastErrCode: integer;
  Timeouts: TCommTimeouts;
begin
  Result:=False;
  // to connect, we must first disconnect!
  Disconnect;
  if(Chnl.DeviceName <> '') then begin
     CommHandle:= CreateFile(pansichar(Chnl.DeviceName),GENERIC_READ or GENERIC_WRITE, 0, nil,
	      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL or FILE_FLAG_OVERLAPPED,0); //
     if CommHandle = INVALID_HANDLE_VALUE then begin
       Result:=False;
       LastErrCode:=GetLastError;
       OutputDebugString(Pansichar(SysErrorMessage(LastErrCode)));
       Exit;
     end;

    if True <> GetCommState(CommHandle,DCB) then begin// - �������� ������� DCB.
       LastErrCode:=GetLastError;
       //OutputDebugString(Pansichar(SysErrorMessage(LastErrCode)));
    end;
    DCB.BaudRate:=Chnl.Baud;// - ������������� �������� ������.
    DCB.Parity:=Chnl.Parity;// - ������������� ���������� �������� �� ��������
    DCB.ByteSize:=Chnl.DataBits;// - 8 ��� � ������������ �����.
    DCB.StopBits:=Chnl.StopBits;// - ��������� ����-���.
    DCB.EvtChar:=char(Chnl.StartChar);// - ��� ���������� ������ ������ ��� SetCommMask. � ������ ������ - ������� �������.
    DCB.EofChar:=char(Chnl.StopChar);
    if True <> SetCommState(CommHandle,DCB) then begin // - �� ������ ���������� ����������� ������������ DCB.
       LastErrCode:=GetLastError;
       //OutputDebugString(Pansichar(SysErrorMessage(LastErrCode)));
    end;
    if True <> SetCommMask(CommHandle,EV_RXFLAG or EV_RXCHAR) then begin
       LastErrCode:=GetLastError;
       //OutputDebugString(Pansichar(SysErrorMessage(LastErrCode)));
    end;
    if Chnl.Timeout > 0 then begin
      Timeouts.ReadTotalTimeoutConstant:=Chnl.Timeout;
      SetCommTimeouts(CommHandle, Timeouts);
    end;
    //������� ����������� �����
    //��� ����� ��������� ��������� ������ ������
    //� ����� - ReadComm
    Chnl.IsActive:=True;
    Chnl.RcvState:=csConnected;
    //CommThread := CreateThread(nil,0,@ReadComm,nil,0,ThreadID);
    ReadThread:=TReadBinCommThread.Create(True);
    ReadThread.CommHandle:=CommHandle;
    ReadThread.ComMutex:=ComMutex;
    ReadThread.FreeOnTerminate:=True;
    ReadThread.Buffer:=@Buffer;
    ReadThread.ChnlData:=@Chnl;
    ReadThread.Resume;
    // start timer? if not already started
    //if not Assigned(TimerThread) then TimerInit(300);
    //FTimer.Enabled:=True;
    TimerStart();
    Result:=True;
  end
  else begin
    Result:=False;
  end;
end;

procedure TRxBinPktCom.SetBaud(const Value:LongWord);
begin
  Chnl.Baud:=Value;
end;

function TRxBinPktCom.GetBaud:LongWord;
begin
  Result:=Chnl.Baud;
end;

procedure TRxBinPktCom.FTimerTimer(Sender: TObject);
begin
  FTimer.Enabled:=False;
  begin
    //OutputDebugString(Pansichar('Timer'));
    if Buffer.HasDataAvaiable then begin
      //OutputDebugString(Pansichar('DataTimer'));
      try
        DecodePacket;
      except
        on E: EConvertError do
          OutputDebugString(Pansichar(E.Message));
      end;
      Buffer.HasDataAvaiable:=False;
    end
    else begin
      if Buffer.TimeoutOccured then begin
        SignalTimeout;
        Buffer.TimeoutOccured:=False;
      end;
    end;

  end;
  ControlThread();
  FTimer.Enabled:=True;
end;

procedure TRxBinPktCom.ControlThread;
begin
end;

procedure TRxBinPktCom.SignalTimeout();
begin
end;

procedure TRxBinPktCom.TimerInit(Value:U16);
begin
  if not FStandAlone then Exit;
  FTimer:=TTimer.Create(Self);
  with FTimer do begin
    Enabled:=False;
    Interval:=Value;
    OnTimer:=FTimerTimer;
  end;
end;

procedure TRxBinPktCom.TimerStart();
begin
  if Assigned(FTimer) then FTimer.Enabled:=True;
end;

procedure TRxBinPktCom.TimerStop();
begin
  if Assigned(FTimer) then FTimer.Enabled:=False;
end;

(**)
function TRxBinPktCom.GetDeviceName:TDeviceName;
begin
  Result:=ComNumToDeviceName(FCurComIndex);
end;

procedure TRxBinPktCom.SetDeviceName(const Value:TDeviceName);
begin
  FCurComIndex:=DeviceNameToComNum(Value);
  Chnl.DeviceName:=Value;
end;
(**)

function TRxBinPktCom.GetCurChnl:TComChnl;
begin
  Result:=Chnl;
end;


procedure TRxBinPktCom.SetReqPerSec(const Value: U16);
begin
  FReqPerSec:=Value;
  if Assigned(FTimer) then begin
    if FReqPerSec > 0  then FTimer.Interval:=1000 div FReqPerSec
    else FTimer.Enabled:=False;
  end;
end;


END.