{.$DEFINE TRACING_ON}
UNIT TBinComm;
INTERFACE uses   SysUtils, Classes,
  {$IFDEF WIN32}
   Dialogs, Windows,
   AdPort;
  {$ENDIF}
  {$IFDEF LINUX}
   QDialogs,
   AxPort;
  {$ENDIF}

type PTBinPacket = ^TTBinPacket;
     TTBinPacket = packed array[0..1023*2] of Byte;

     TTBinState = ( tbSOP, tbFIRSTCHAR, tbBYTESTUFF, tbEOP );

     TDeviceName = string;

type TTBinComm = class
  private
  protected
    {$IFDEF WIN32}
      FPort:TApdComPort;
    {$ENDIF}
    {$IFDEF LINUX}
      FPort:TApxComPort;
    {$ENDIF}
    FIPacket,FOPacket:TTBinPacket;
    FIPacketLen,FOPacketLen:Word;
    FEnabled:Boolean;
    State:TTBinState;
    DataCnt:Word;
    FPacketComplete:Boolean;
    function GetCheckSum(var Packet; Len:Word):Byte;
    function GetBaud:LongWord;
    function GetDeviceName: TDeviceName;
    procedure SetBaud(const Value:LongWord);
    procedure SetDeviceName(const Value: TDeviceName);
    procedure FPortTriggerAvail(CP:TObject; Count:Word);
    property Enabled:Boolean read FEnabled write FEnabled default False;
    property PacketComplete:Boolean read FPacketComplete write FPacketComplete default False;
  public
    procedure SendPacket(var Packet; const Len:Word);
    procedure SendLastPacket();
    function GetPacket(Packet:Pointer; var Len:Word):Boolean;
    function DecodePacket():Byte; virtual; abstract;
    //
    constructor Create();
    destructor Destroy(); override;
    function  Connect():Boolean; overload;
    function  Connect(ADeviceName:TDeviceName; ABaud:LongWord):Boolean; overload;
    procedure Disconnect();
    property DeviceName:TDeviceName read GetDeviceName write SetDeviceName;
    property Baud:LongWord read GetBaud write SetBaud;
end;

function ComNumToDeviceName(ComNumber:Word):string;
function DeviceNameToComNum(DeviceName:TDeviceName):Word;

IMPLEMENTATION uses StrUtils, CheckSum;

const
  SOP = $7E;
  EOP = $7E;
  STUFFBYTE = $7D;

procedure TTBinComm.FPortTriggerAvail(CP:TObject; Count:Word);
var I:Word; Buf:Byte;
begin
  //if not Enabled then Exit;
  
  for I:=1 to Count do begin
    Buf:=Byte(FPort.GetChar());

    case State of

      tbSOP:begin
        if Buf = SOP then begin
           PacketComplete:=False;
           DataCnt:=0;
           State:=tbFIRSTCHAR;
        end;
      end;

      tbFIRSTCHAR:begin
        if Buf <> SOP then begin
          if Buf = STUFFBYTE then State:=tbBYTESTUFF
          else begin
            FIPacket[DataCnt]:=Buf;
            Inc(DataCnt);
            State:=tbEOP;
          end;
        end;
      end;

      tbBYTESTUFF:begin
        FIPacket[DataCnt]:=Buf xor $20;
        Inc(DataCnt);
        State:=tbEOP;
      end;

      tbEOP:begin
        if DataCnt > SizeOf(TTBinPacket)-1 then begin
           State:=tbSOP;
           Exit; 
        end;
        if Buf <> EOP then begin
          if Buf = STUFFBYTE then State:=tbBYTESTUFF
          else begin
            FIPacket[DataCnt]:=Buf;
            Inc(DataCnt);
          end;
        end else begin
           State:=tbSOP;
           Enabled:=False;
           if DataCnt < 2 then Exit;
           FIPacketLen:=DataCnt-1;
           PacketComplete:=True;
        end;
      end;

    end;

  end;
end;

function TTBinComm.GetPacket(Packet:Pointer; var Len: Word):Boolean;
var CS:Byte;
begin
  Result:=False;
  Len:=0;
  if not PacketComplete then Exit;
  CS:=GetCheckSum(FIPacket,FIPacketLen);
  if CS <> FIPacket[FIPacketLen] then Exit;
  Len:=FIPacketLen;
  Move(FIPacket,Packet^,Len);
  //for I:=0 to Len do PTBinPacket(Packet)[I]:=FIPacket[I];
  PacketComplete:=False;
  Result:=True;
end;

function TTBinComm.GetCheckSum(var Packet; Len:Word):Byte;
begin
  //Result:=$FF - CheckSum(@Packet,Len);
  Result:=CheckSumCRC8(@Packet,Len);
end;

procedure TTBinComm.SendPacket(var Packet; const Len: Word);
var I,K:Word; CS,Buf:Byte;
begin
  if Len = 0 then Exit;

  CS:=GetCheckSum(Packet,Len);

  K:=1;
  FOPacket[0]:=SOP;
  for I:=0 to Len-1 do begin
    Buf:=TTBinPacket(Packet)[I];
    if Buf in [SOP,STUFFBYTE,EOP] then begin
       FOPacket[K]:=STUFFBYTE;
       Inc(K);
       FOPacket[K]:=Buf xor $20;
       Inc(K);
    end else begin
       FOPacket[K]:=Buf;
       Inc(K);
    end;
  end;

  if CS in [SOP,STUFFBYTE,EOP] then begin
     FOPacket[K]:=STUFFBYTE;
     Inc(K);
     FOPacket[K]:=CS xor $20;
     Inc(K);
   end else begin
     FOPacket[K]:=CS;
     Inc(K);
  end;
  FOPacket[K]:=EOP;
  FOPacketLen:=K+1;
  FPort.PutBlock(FOPacket,FOPacketLen);
  Enabled:=True;
end;

function TTBinComm.Connect(): Boolean;
begin
  //ShowMessage('Connect');
  FPort.Open:=True;
  Result:=True;
end;

constructor TTBinComm.Create();
begin
  {$IFDEF WIN32}
  FPort:=TApdComPort.Create(NIL);
  {$ENDIF}
  {$IFDEF LINUX}
  FPort:=TApxComPort.Create(NIL);
  {$ENDIF}
  with FPort do begin
    Baud:=57600;
    //Baud:=115200;
    AutoOpen:=False;
    DTR:= False;
    RTS:= False;
    UseEventWord:=False;
    OnTriggerAvail:=FPortTriggerAvail;
    {$IFDEF WIN32}
      ComNumber:=1;
      {.$IFDEF TRACING_ON}
      //Tracing:=tlOn;
      //Logging:=tlOn;
      {.$ELSE}
      Tracing:=tlOff;
      Logging:=tlOff;
      {.$ENDIF}
      TraceAllHex:=True;
      TraceName:= 'DEBUG.TRC';
      LogAllHex:=True;
      LogHex:=True;
      LogName:= 'DEBUG.LOG';
    {$ENDIF}
    {$IFDEF LINUX}
      DeviceName:='/dev/ttyS0';
      DebugLog.BufferSize:=65536;
      DebugLog.WriteMode:=wmOverwrite;
      {$IFDEF TRACING_ON}
        DebugLog.Enabled:=True;
      {$ELSE}
        DebugLog.Enabled:=False;
      {$ENDIF}
      DebugLog.FileName:='apro.log';
      DeviceLayer:='dlLinux';
      BufferFull:=0;
      BufferResume:=0;
      HWFlowControl:=False;
    {$ENDIF}
  end;
  State:=tbSOP;
end;

destructor TTBinComm.Destroy;
begin
  Disconnect();
  inherited Destroy();
end;

procedure TTBinComm.Disconnect;
begin
  //ShowMessage('Disconnect');
  FPort.Open:=False;
end;

procedure TTBinComm.SendLastPacket;
begin
  FPort.PutBlock(FOPacket,FOPacketLen);
  Enabled:=True;
end;

function ComNumToDeviceName(ComNumber:Word):string;
begin
  Result:='\\.\COM'+IntToStr(ComNumber);
end;

function DeviceNameToComNum(DeviceName:TDeviceName):Word;
begin
  Result:=StrToInt(MidStr(DeviceName,8,3)); 
end;

function TTBinComm.Connect(ADeviceName:TDeviceName; ABaud:LongWord): Boolean;
begin
  DeviceName:=ADeviceName;
  Baud:=ABaud;
  Result:=Connect();
end;

function TTBinComm.GetDeviceName:TDeviceName;
begin
  {$IFDEF WIN32}
    Result:=ComNumToDeviceName(FPort.ComNumber);
  {$ENDIF}
  {$IFDEF LINUX}
    Result:=FPort.DeviceName;
  {$ENDIF}
end;

procedure TTBinComm.SetDeviceName(const Value:TDeviceName);
begin
  {$IFDEF WIN32}
    FPort.ComNumber:=DeviceNameToComNum(Value);
  {$ENDIF}
  {$IFDEF LINUX}
    FPort.DeviceName:=Value;
  {$ENDIF}
end;

procedure TTBinComm.SetBaud(const Value:LongWord);
begin
  FPort.Baud:=Value;
end;

function TTBinComm.GetBaud:LongWord;
begin
  Result:=FPort.Baud;
end;


END.
