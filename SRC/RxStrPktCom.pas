UNIT RxStrPktCom;
INTERFACE uses SysUtils, Classes, QExtCtrls, Contnrs,
  RxBinPktCom,
  RxUtils,
  RxAlias,
  Windows;
{
type TComBuffer = record
  HasDataAvaiable: boolean;
  TimeoutOccured: boolean;
  FIPacket: string;
  FIPackets: TQueue;
  FWriteQueue: boolean;
end;
}
type TReadCommThread = class(TThread)
  public
    CommHandle:DWORD;                   // COM port handle
    ComMutex:DWORD;
    Buffer: ^TComBuffer;
    ChnlData: ^TComChnl;
  protected
    procedure Execute; override;
end;

type TRxStrPktCom = class(TComponent)
  private
  protected
    CommHandle:DWORD;                    // COM port handle
    ComMutex:DWORD;
    DCB:TDCB;
    hEvent: THandle;
    ReadThread: TReadCommThread;         // TThread that performs reasing
    //---------------------------------
    FTimer:TTimer;
    Field:TStringList;
    FOPacket,FAOPacket:string;
    InBuffer: TComBuffer;
    FCurChnl:U8;
    FCurComIndex:U8;
    FReqPerSec:U16;
    FTriggerTimer:U16;
    FTicks:U32;
    function GetCurChnl:TComChnl;
    procedure SetReqPerSec(const Value: U16);
    function GetDeviceName:TDeviceName;
    procedure SetDeviceName(const Value:TDeviceName);

    //
    procedure TimerInit(Value:U16);
    procedure TimerStart();
    procedure TimerStop();
    procedure ControlThread(); virtual;
    procedure SignalTimeout(); virtual;
    //
    procedure SendPacket(const Packet:string); virtual;
    function  DecodePacket():U8; virtual;
    //
    property ReqPerSec:U16 read FReqPerSec write SetReqPerSec;
  public

    Chnl:TComChnl;
    constructor Create(AOwner:TComponent); override;
    destructor  Destroy(); override;
    function  Connect():RxAlias.BOOL; virtual;
    function Disconnect():RxAlias.BOOL; virtual;
    //procedure SelectNextPort(); virtual;
    property CurChnl:TComChnl read GetCurChnl;
    procedure FTimerEvent(Sender: TObject);
end;

IMPLEMENTATION uses RxChkSum;

const
  MaxInputQueueSize=10;

procedure TReadCommThread.Execute;
var
  Recieve:array [0..255] of char;
  PRecieve: PChar;
  TransMask, ComStatErrs, Kols, Waiter: DWORD;
  PacketStart, Offset, PacketEnd: Integer;
  ReadForward: Cardinal;
  SysErrsFlag: LongBool;
  OvrComm, OvrRead: Overlapped;
  evHandles: TWOHandleArray;//array [0..1] of DWORD;
  Stat : TComStat;
  I:Integer;
  TempString: ^String;
begin
  //get mutex for synchronious termination
  WaitForSingleObject(ComMutex, INFINITE);
  Buffer^.HasDataAvaiable:=False;
  Buffer^.TimeoutOccured:=False;
  //create event handles
  evHandles[0]:=CreateEvent(nil, False, False, nil);
  evHandles[1]:=CreateEvent(nil, False, False, nil);
  FillChar(OvrComm, SizeOf(OvrComm), #0);
  FillChar(OvrRead, SizeOf(OvrRead), #0);
  FillChar(Recieve,256, #0);
  OvrComm.hEvent:=evHandles[0];
  OvrRead.hEvent:=evHandles[1];
  TransMask:=0;
  SysErrsFlag:=WaitCommEvent(CommHandle,TransMask,@OvrComm); //����
  //if SysErrsFlag = False then begin
  //  ComStatErrs:=GetLastError;
  //  OutputDebugString(Pansichar(SysErrorMessage(ComStatErrs)));
  //  case ComStatErrs of
  //    ERROR_IO_PENDING:
  //  end;
  //end;
  if ChnlData^.PacketSize > 0 then
    ReadForward:=ChnlData^.PacketSize
  else
    ReadForward:=256;
  PacketStart:=0;
  Offset:=0;
  // here we wait and error is discarded
  repeat
    Waiter:=WaitForMultipleObjects(2, @evHandles, False, ChnlData^.Timeout);
    case Waiter of
    WAIT_OBJECT_0 + 0: begin //State retrieval completed
      if TransMask = 0 then // error
      begin
        // handle error properly?..
        // wait timeout in particular
        SysErrsFlag:=ClearCommError(CommHandle,ComStatErrs,@Stat);
        if SysErrsFlag = False then begin
          ComStatErrs:=GetLastError;
          //OutputDebugString(Pansichar(SysErrorMessage(ComStatErrs)));
          case ComStatErrs of
            ERROR_IO_PENDING: continue; // now we should not get in here
            else begin
              // todo: hande particluar error
              // ideally, we only get here in case of timeout
              Buffer^.TimeoutOccured:=True;

            end;
          end;
        end;
        // now we continue to wait
        WaitCommEvent(CommHandle,TransMask,@OvrComm);
        continue;
      end
      else begin
        // presumably this is a correct event
        //if ((ChnlData^.StartChar <> #0) and ((TransMask and EV_RXFLAG)=EV_RXFLAG))
        //   or (((ChnlData^.StartChar = #0) and ((TransMask and EV_RXCHAR)=EV_RXCHAR))) then //��������� ������ �������
        // Buffer^.HasDataAvaiable:=True; - this is set after  a read has ompleted
        Buffer^.TimeoutOccured:=False;
        SysErrsFlag:=ClearCommError(CommHandle,ComStatErrs,@Stat);
        PRecieve:=@Recieve[Offset];
        if SysErrsFlag = False then begin
          ComStatErrs:=GetLastError;
          //OutputDebugString(Pansichar(SysErrorMessage(ComStatErrs)));
          case ComStatErrs of
            ERROR_IO_PENDING: continue; // now we should not get in here
            else begin
              // todo: hande particluar error
              // ideally, we only get here in case of timeout
              Buffer^.TimeoutOccured:=True;
            end;
          end;
        end;
        ReadFile(CommHandle,PRecieve^,Stat.cbInQue,Kols,@OvrRead);// start to read
        continue;
      end; // end case of WaitCommEvent
    end;
    WAIT_OBJECT_0 + 1: begin
      GetOverlappedResult(CommHandle, OvrRead, Kols, True);
      //OutputDebugString(PChar('From COM port ' + IntToStr(Kols) + ' bytes were read'));
      if (Kols = 0) and (Offset = 0) then begin
        // now we continue to wait
        WaitCommEvent(CommHandle,TransMask,@OvrComm);
        continue;
      end
      else begin
        // move forward with offset we have
        Offset:=Offset + Kols;

        // search for packet start
        if ChnlData^.StartChar = #0 then
          PacketStart:=0
        else begin

          for I := 0 to Offset do begin
            if Recieve[I] = ChnlData^.StartChar then begin
              PacketStart:=I;
              break;
            end;
          end;
          if Recieve[PacketStart] <> ChnlData^.StartChar then begin // we failed to find start character
            // we try to read further
            if Offset >= 256 - ChnlData^.PacketSize then Offset:=0;
            if ChnlData^.PacketSize > 0 then ReadForward:=ChnlData^.PacketSize
            else ReadForward:=256-Offset;
            PRecieve:=@Recieve[Offset];
            ReadFile(CommHandle,PRecieve^,ReadForward,Kols,@OvrRead);// start to read
            continue;
          end;
        end;
        // align data packet with buffer start
        if PacketStart > 0 then begin
          for I := 0 to Offset - PacketStart do Recieve[I]:=Recieve[I+PacketStart];
          Offset:=Offset - PacketStart;
          PacketStart:=0;
        end;
        if (ChnlData^.PacketSize > Byte(0)) then
        begin
          if Offset >= ChnlData^.PacketSize then begin
            SetString(Buffer^.FIPacket, PChar(@Recieve[0]), ChnlData^.PacketSize);
            if Buffer^.FWriteQueue then
            begin
              AddToBuffer(Buffer^, Buffer^.FIPacket);
            end;
            // overwrite packet we already recieved
            for I := 0 to Offset-ChnlData^.PacketSize do Recieve[I]:=Recieve[I+Offset-ChnlData^.PacketSize];
            Offset := Offset - ChnlData^.PacketSize;
            ReadForward:=ChnlData^.PacketSize;
            Buffer^.HasDataAvaiable:=True;  // we have packet
          end
          else begin
            // we should read the rest
            ReadForward:=ChnlData^.PacketSize - Offset;
            // we read the rest of packet outside of this 'if'
          end;
        end
        else begin
          // search for the end
          PacketEnd:=PacketStart;
          for I:=PacketStart to Offset do begin
            if Recieve[I] = ChnlData^.StopChar then begin
              PacketEnd:=I;
              break;
            end;
          end;
          if PacketEnd <> PacketStart then begin  // we have packet
            // TODO: check on last symbols
            SetString(Buffer^.FIPacket, PChar(@Recieve[0]), 1 + PacketEnd - PacketStart);
            //OutputDebugString(PAnsiChar('Recieved packet: ' + Buffer^.FIPacket));
            if Buffer^.FWriteQueue then begin
              AddToBuffer(Buffer^, Buffer^.FIPacket);
            end;
            for I := 0 to Offset - PacketEnd - 2 do Recieve[I]:=Recieve[1+I+PacketEnd];
            Offset := Offset - PacketEnd - 1;
            for I := Offset to 255 do Recieve[I]:=#0;
            ReadForward:=256 - Offset;
            Buffer^.HasDataAvaiable:=True;
          end
          else begin
            if Offset < 256 then ReadForward:=256 - Offset
            else begin                      // ok, we discard our data
              Offset:=0;
              ReadForward:=256;
            end;
          end;

        end;

        PRecieve:=@Recieve[Offset];
        ReadFile(CommHandle,PRecieve^,ReadForward,Kols,@OvrRead);
        continue;
      end;
    end;// end Read case
    WAIT_TIMEOUT: begin
      Buffer^.TimeoutOccured:=True;
      //OutputDebugString(Pansichar('Timeout occured'));
      // now we continue to wait
      // WaitCommEvent(CommHandle,TransMask,@OvrComm);
      continue;
    end;
  end;// end CASE operator
  until Terminated;//while
  CloseHandle(evHandles[0]);
  CloseHandle(evHandles[1]);
  ReleaseMutex(ComMutex);
end;

constructor TRxStrPktCom.Create(AOwner:TComponent);
begin

  Field:=TStringList.Create();
  Chnl.Baud:=2400;
  Chnl.Num:=0;
  FTimer:=TTimer.Create(Self);
  FTimer.Enabled:=False;
  FTimer.OnTimer:=FTimerEvent;

  FCurComIndex:=1;
  FCurChnl:=0;
  CommHandle:=INVALID_HANDLE_VALUE;
  ComMutex:=CreateMutex(nil, False, nil);
  InBuffer.ReadIndex:=1;
  InBuffer.WriteIndex:=1;
  InBuffer.FWriteQueue:=False;
end;

destructor TRxStrPktCom.Destroy();
var I:U16;
begin
  Disconnect();
  //ReadThread.Free;
  CloseHandle(ComMutex);
  Field.Free();
  inherited Destroy();
end;

function TRxStrPktCom.Disconnect():RxAlias.BOOL;
begin
  Result:=False;
  FCurChnl:=0;
  //for I:=0 to 1 do Chnl[I].RcvState:=csDisconnected;
  Chnl.RcvState:=csDisconnected;
  Chnl.IsActive:=False;
  if Assigned(ReadThread) then ReadThread.Terminate;
  // it is never wrong to stop the timer
  TimerStop();
  // now, wait until termination is complete
  WaitForSingleObject(ComMutex, INFINITE);
  ReleaseMutex(ComMutex);
  CloseHandle(CommHandle);
  CommHandle:=INVALID_HANDLE_VALUE;
  // TODO: return log
  Result:=True;
end;

function TRxStrPktCom.Connect():RxAlias.BOOL;
var
  LastErrCode: integer;
  Timeouts: TCommTimeouts;
begin
  Result:=False;
  // to connect, we must first disconnect!
  Disconnect;
  if(Chnl.DeviceName <> '') then begin
     FCurChnl:=0;
     CommHandle:= CreateFile(pansichar(Chnl.DeviceName),GENERIC_READ or GENERIC_WRITE, 0, nil,
	      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL or FILE_FLAG_OVERLAPPED,0); //
     if CommHandle = INVALID_HANDLE_VALUE then begin
       Result:=False;
       LastErrCode:=GetLastError;
       //OutputDebugString(Pansichar(SysErrorMessage(LastErrCode)));
       Exit;
     end;

    if True <> GetCommState(CommHandle,DCB) then begin// - �������� ������� DCB.
       LastErrCode:=GetLastError;
       //OutputDebugString(Pansichar(SysErrorMessage(LastErrCode)));
    end;
    DCB.BaudRate:=Chnl.Baud;// - ������������� �������� ������.
    DCB.Parity:=Chnl.Parity;// - ������������� ���������� �������� �� ��������
    DCB.ByteSize:=Chnl.DataBits;// - 8 ��� � ������������ �����.
    DCB.StopBits:=Chnl.StopBits;// - ��������� ����-���.
    DCB.EvtChar:=char(Chnl.StartChar);// - ��� ���������� ������ ������ ��� SetCommMask. � ������ ������ - ������� �������.
    DCB.EofChar:=char(Chnl.StopChar);
    if True <> SetCommState(CommHandle,DCB) then begin // - �� ������ ���������� ����������� ������������ DCB.
       LastErrCode:=GetLastError;
       //OutputDebugString(Pansichar(SysErrorMessage(LastErrCode)));
    end;
    if True <> SetCommMask(CommHandle,EV_RXFLAG or EV_RXCHAR) then begin
       LastErrCode:=GetLastError;
       //OutputDebugString(Pansichar(SysErrorMessage(LastErrCode)));
    end;
    if Chnl.Timeout > 0 then begin
      Timeouts.ReadTotalTimeoutConstant:=Chnl.Timeout;
      SetCommTimeouts(CommHandle, Timeouts);
    end;
    //������� ����������� �����
    //��� ����� ��������� ��������� ������ ������
    //� ����� - ReadComm
    Chnl.IsActive:=True;
    Chnl.RcvState:=csConnected;
    //CommThread := CreateThread(nil,0,@ReadComm,nil,0,ThreadID);
    ReadThread:=TReadCommThread.Create(True);
    ReadThread.CommHandle:=CommHandle;
    ReadThread.ComMutex:=ComMutex;
    ReadThread.FreeOnTerminate:=True;
    ReadThread.Buffer:=@InBuffer;
    ReadThread.ChnlData:=@Chnl;
    ReadThread.Resume;
    // start timer? if not already started
    //if not Assigned(TimerThread) then TimerInit(300);
    //FTimer.Enabled:=True;
    TimerStart();
    Result:=True;
  end
  else begin
    Result:=False;
  end;
end;

procedure TRxStrPktCom.FTimerEvent(Sender: TObject);
begin
  FTimer.Enabled:=False;
  begin
    //OutputDebugString(Pansichar('Timer'));
    if InBuffer.HasDataAvaiable then begin
      //OutputDebugString(Pansichar('DataTimer'));
      try
        DecodePacket;
      except
        on E: EConvertError do
          OutputDebugString(Pansichar(E.Message));
      end;
      InBuffer.HasDataAvaiable:=False;
    end
    else begin
      if InBuffer.TimeoutOccured then begin
        SignalTimeout;
        InBuffer.TimeoutOccured:=False;
      end;
    end;

  end;
  ControlThread();
  FTimer.Enabled:=True;
end;

procedure TRxStrPktCom.ControlThread();
begin
end;

procedure TRxStrPktCom.SignalTimeout();
begin
end;

procedure TRxStrPktCom.TimerInit(Value: U16);
begin
  FTimer.Interval:=Value;
end;

procedure TRxStrPktCom.TimerStart;
begin
  FTimer.Enabled:=True;
end;

procedure TRxStrPktCom.TimerStop;
begin
  FTimer.Enabled:=False;
end;

(**)
function TRxStrPktCom.GetDeviceName:TDeviceName;
begin
  Result:=ComNumToDeviceName(FCurComIndex);
end;

procedure TRxStrPktCom.SetDeviceName(const Value:TDeviceName);
begin
  FCurComIndex:=DeviceNameToComNum(Value);
  Chnl.DeviceName:=Value;
end;

function TRxStrPktCom.GetCurChnl:TComChnl;
begin
  Result:=Chnl;
end;

procedure TRxStrPktCom.SetReqPerSec(const Value: U16);
begin
  FReqPerSec:=Value;
  if Assigned(FTimer) then begin
    if FReqPerSec > 0  then FTimer.Interval:=1000 div FReqPerSec
    else FTimer.Enabled:=False;
  end;
end;

function TRxStrPktCom.DecodePacket():U8;
begin
   Result:=0;
end;

procedure TRxStrPktCom.SendPacket(const Packet:string);
var
  OvrWrite: TOverlapped;
  PCPacket: PChar;
  Written: Cardinal;
  strlen: Cardinal;
begin
  PCPacket:= PChar(Packet);
  strlen:= Length(Packet);
  FillChar(OvrWrite, SizeOf(OvrWrite), #0);
  if CommHandle <> INVALID_HANDLE_VALUE then begin
    WriteFile(CommHandle, PCPacket^, strlen, Written, POverlapped(@OvrWrite));
    // this is necessary
    // this makes call synchronously finish
    // otherwise, Overlapped structure will be recycled on function exit
    // and then used when write operation finishes
    // with unpredictable but bad results
    GetOverlappedResult(CommHandle, OvrWrite, Written, True);
  end;
  //FPort.PutString(Packet);
end;

END.
