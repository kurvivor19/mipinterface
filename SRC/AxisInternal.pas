UNIT AxisInternal;
INTERFACE uses ExProtclInternal,
  RxAlias;


type TSendPacket = procedure(var Packet; const Len:Word) of object;

type TAxisInternal = class
  private
    FNum:U8;
    FPos,FSpd:F64;
    FPlsnPos,FPlspPos,FrefPntPos:F64;
    FHlsn,FPlsn,FPlsp,FHlsp,FINP:BOOL;
    FRdy,FAlm:BOOL;
    FSpeedRest:F64;
    FPositivDirRest:F64;
    FNegativeDirRest:F64;
    //
    FZP:F32;
    FParamId:U8;
  public
    OPacket:TOPacket;
    OPacketLen:U16;
    SendPacket:TSendPacket;
    constructor Create(ANum:U8);
    destructor Destroy(); override;
    function DecodePacket(const IPacket:TIPacket;const Len:U16):BOOL;
    //
    procedure MoveTo(APos,ASpd:F64);
    procedure SetLimits(const APlsnPos,APlspPos:F64);
    procedure SetPos(APos:F64);
    procedure GetLimits();
    procedure GetParam(const AParamId:U8);
    procedure SetParam(const AParamId:U8; const AParam:F32);
    procedure RestoreZP();
    procedure SaveZP(const ADir:S32);
    //
    //procedure GetData(const ANum:U8; var APos,ASpd,APlsnPos,ARefPntPos,APlspPos:F64; var AHlsn,APlsn,APlsp,AHlsp:BOOL);
    procedure GetData(const ANum:U8; var APos,ASpd,APlsnPos,APlspPos,AZPPos:F64; var AHlsn,APlsn,APlsp,AHlsp,ARdy,Alm,INP:BOOL);
    procedure GetStatData(var APos,ASpd:F64; var AHlsn,APlsn,APlsp,AHlsp:BOOL);
    procedure GetLimitsData(var APlsnPos,ARefPntPos,APlspPos:F64);
    procedure GetParamData(var AParamId:U8; var AParam:F32);
    property Pos:F64 read FPos;
    property NegativeDirRest:F64 read FNegativeDirRest;
    property PositivDirRest:F64 read FPositivDirRest;
    property SpeedRest:F64 read FSpeedRest;
end;

IMPLEMENTATION

constructor TAxisInternal.Create(ANum: Byte);
const
  NegativeDirRest:array[1..3] of F64 = ( -15.0, -90.0, -270.0 );
  PositivDirRest: array[1..3] of F64 = (  15.0,  90.0,  270.0 );
  SpeedRest:      array[1..3] of F64 = (   3.0,   6.0,    6.0 );
begin
  inherited Create();
  FNum:=ANum;
  FNegativeDirRest:=NegativeDirRest[ANum];
  FPositivDirRest:=PositivDirRest[ANum];
  FSpeedRest:=SpeedRest[ANum];
end;

function TAxisInternal.DecodePacket(const IPacket:TIPacket;const Len:U16):BOOL;
begin
  Result:=False;
  case IPacket.Ans of
    ANS_STAT:begin
      with IPacket.Stat.Axis[FNum] do begin
        FPos:=Pos;
        FSpd:=Spd;
        DecodeAxisStat(Stat, FHlsn,FPlsn,FPlsp,FHlsp,FRdy,FAlm,FINP);
      end;
    end;
    ANS_LIMITS:begin
      with IPacket.Limits.Axis[FNum] do begin
        FPlsnPos:=LSN;
        FPlspPos:=LSP;
        FrefPntPos:=ZP;
      end;
    end;
    ANS_PARAM:begin
      if IPacket.Param.DevId <> FNum then Exit;
      FParamId:=IPacket.Param.Id;
      case IPacket.Param.Id of
        PARAM_AXISZP:FZP:=IPacket.Param.Param.Float32;
      end;
    end;
  end;
end;

destructor TAxisInternal.Destroy;
begin
  inherited Destroy();
end;

procedure TAxisInternal.GetStatData(var APos,ASpd:F64; var AHlsn,APlsn,APlsp,AHlsp:BOOL);
begin
  APos:=FPos;
  ASpd:=FSpd;
  AHlsn:=FHlsn;
  APlsn:=FPlsn;
  APlsp:=FPlsp;
  AHlsp:=FHlsp;
end;

procedure TAxisInternal.GetLimitsData(var APlsnPos,ARefPntPos,APlspPos:F64);
begin
  APlsnPos:=FPlsnPos;
  ARefPntPos:=FRefPntPos;
  APlspPos:=FPlspPos;
end;

procedure TAxisInternal.GetData(const ANum: U8; var APos,ASpd,APlsnPos,APlspPos,AZPPos:F64; var AHlsn,APlsn,APlsp,AHlsp,ARdy,Alm,INP:BOOL);
begin
  APos:=FPos;
  ASpd:=FSpd;
  APlsnPos:=FPlsnPos;
  APlspPos:=FPlspPos;  
  AHlsn:=FHlsn;
  APlsn:=FPlsn;
  APlsp:=FPlsp;
  AHlsp:=FHlsp;
  ARdy:=FRdy;
  Alm:=FAlm;
  INP:=FINP;
  AZPPos:=FRefPntPos;
end;

procedure TAxisInternal.GetParamData(var AParamId: U8; var AParam: F32);
begin
  AParamId:=FParamId;
  AParam:=FZP;
end;

procedure TAxisInternal.MoveTo(APos,ASpd:F64);
begin
  //OPacket.MoveTo.Adr:=0;
  OPacket.AxisMoveTo.Cmd:=CMD_AXISMOVETO + FNum;
  OPacket.AxisMoveTo.Pos:=APos;
  OPacket.AxisMoveTo.Spd:=ASpd;

  OPacketLen:=SizeOf(TAxisMoveTo);
  SendPacket(OPacket,OPacketLen);
end;

procedure TAxisInternal.SetLimits(const APlsnPos,APlspPos:F64);
begin
  with OPacket.AxisSetLimits do begin
    Cmd:=CMD_AXISSETLIMITS + FNum;
    PlsnPos:=APlsnPos;
    PlspPos:=APlspPos;
  end;
  OPacketLen:=SizeOf(TAxisSetLimits);
  SendPacket(OPacket,OPacketLen);
end;

procedure TAxisInternal.SetPos(APos:F64);
begin
  with OPacket.AxisSetPos do begin
    Cmd:=CMD_AXISSETPOS + FNum;
    Pos:=APos;
  end;
  OPacketLen:=SizeOf(TAxisSetPos);
  SendPacket(OPacket,OPacketLen);
end;

procedure TAxisInternal.GetLimits();
begin
  with OPacket.AxisGetLimits do begin
    Cmd:=CMD_AXISGETLIMITS + FNum;
  end;
  OPacketLen:=SizeOf(TAxisGetLimits);
  SendPacket(OPacket,OPacketLen);
end;

procedure TAxisInternal.GetParam(const AParamId:U8);
begin
  with OPacket.GetParam do begin
    Cmd:=CMD_GETPARAM;
    DevId:=FNum;
    Id:=AParamId;
  end;
  OPacketLen:=SizeOf(TGetParam);
  SendPacket(OPacket,OPacketLen);
end;

procedure TAxisInternal.SetParam(const AParamId: U8; const AParam:F32);
begin
  with OPacket.SetParam do begin
    Cmd:=CMD_SETPARAM;
    DevId:=FNum;
    Id:=AParamId;
    Param.Float32:=AParam;
  end;
  OPacketLen:=SizeOf(TSetParam);
  SendPacket(OPacket,OPacketLen);
end;

procedure TAxisInternal.RestoreZP;
begin
  with OPacket.AxisRestoreZP do begin
    Cmd:=CMD_AXISRESTOREZP + FNum;
  end;
  OPacketLen:=SizeOf(TAxisRestoreZP);
  SendPacket(OPacket,OPacketLen);
end;

procedure TAxisInternal.SaveZP(const ADir:S32);
begin
  with OPacket.AxisSaveZP do begin
    Cmd:=CMD_AXISSAVEZP + FNum;
    Dir:=ADir;
  end;
  OPacketLen:=SizeOf(TAxisSaveZP);
  SendPacket(OPacket,OPacketLen);
end;

END.
