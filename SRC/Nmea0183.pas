UNIT Nmea0183;
INTERFACE uses RxAlias;

type TTalkerId = (
    UnknownTalkerId,  // GNSS Unknown
    GP,  // GNSS GPS
    GL,  // GNSS GLONASS
    GN   // GNSS GPS + GLONASS
);

const StrTalkerId: array[UnknownTalkerId..GN] of string = (
    'UNKNOWN',
    'GP', // GNSS GPS
    'GL', // GNSS GLONASS
    'GN'  // GNSS GPS + GLONASS
    );

type TSentenceId = (
    UnknownSentenceId, // Unknown
    GGA, // Global Positioning System Fix Data
    GSA, // Global Positioning System Fix Data
    GLL, // Geographic position, Latitude and Longitude
    GSV, // Satellites in view
    RMA, // Recommended minimum specific GPS/Transit data Loran C
    RMC, // Recommended minimum specific GPS/Transit data
    ZDA, // Date and time
    HDT, // Heading True
    Proprietary,
    Invalid
    );

const
  PROPRIETARY_MSG = 'P';

const StrSentenceId: array[UnknownSentenceId..Invalid] of string = (
    'UNKNOWN',
    'GGA', // Global Positioning System Fix Data
    'GSA', // Global Positioning System Fix Data
    'GLL', // Geographic position, Latitude and Longitude
    'GSV', // Satellites in view
    'RMA', // Recommended minimum specific GPS/Transit data Loran C
    'RMC', // Recommended minimum specific GPS/Transit data
    'ZDA',  // Date and time
    'HDT', // Heading True
    'PROPRIETARY',
    'INVALID'
    );

type TUTCTime = record
  Hour:U8; // 00..23
  Min:u8;  // 00..59
  Sec:U8;  // 00..59
  MSec:U8; // 00..99
end;

type TGeoPosition = record
  Latitude:F64;  // [+/-] Degree
  Longitude:F64; // [+/-] Degree
  Altitude:F64;  // [+/-] Meters
end;

type TStatus = (
    sDataValid,
    sDataInvalid,
    sDiffMode
  );

const TStrStatus:array[sDataValid..sDiffMode] of Char = (
  'A',
  'V',
  'D'
);

type TGxQuality = (
    gxqFixNA,
    gxqFix,
    gxqDiffFix
  );

type TDir = ( North, Sorth, East, West );  

type TLatLon = record
  ADeg:S16;
  AMin:U8;
  ASec:U8;
  Dir:TDir;
end;

type TVisibleSatelite = record
  PRN:U8;  // Part Of Number [1..32]
  SNR:U8;  // Signal Noise Relationship [00..99] [dB]
  Az:U16;  // [0..90] [deg]
  El:U16;  // [0..359] [deg]
end;

type TGSV = record
  TotalMsg:U8;
  CurrentMsg:U8;
  NumOfSat:U8;
  Satelite:array[1..4] of TVisibleSatelite;
end;

// RMC   Recommended Minimum Navigation Information
type TRMC = packed record
  Hour,Min,Sec,MSec:U16;  // Time (UTC)
  Status:Char;            // V -Navigation reciver warning
  Latitude:F32;           // Latitude [deg]  N/S (S neg)
  Longitude:F32;          // Longitude [deg]  E/W (W neg)
  SOG:F32;                // Speed Over Ground [knots]
  COG:F32;                // Course Over Ground (Trackmade good) [deg] TRUE
  Day,Month,Year:U16;     // Date
  MagneticVariation:F32;  // Magnetic Variation [deg] E/W
end;

// ZDA   Time & Date - UTC, Day, Month, Year and Local Time Zone
type TZDA = packed record
  Hour,Min,Sec,MSec:U16;  // Time (UTC)
  Day,Month,Year:U16;     // Date
  TZHour:S16;             // Time Zone Hour (UTC = LocalTime + (+/-)TZTime)
  TZMin:U16;              // Time Zone Min
end;


// GGA   Global Positioning System Fix Data. Time, Position and fix related data for a GPS receiver
type TGGA = packed record
  Hour,Min,Sec,MSec:U16;  // Time (UTC)
  Latitude:F64;           // Latitude [deg]  N/S (S neg)
  Longitude:F64;          // Longitude [deg]  E/W (W neg)
  GPSQuality:U8; // GPS Quality Indicator  0-fix not available, 1-GPS fix, 2-Differential GPS fix
  NumOfSat:U8; // Number of satellites in view, 00 - 12
  // 8. Horizontal Dilution of precision
  Altitude:F64; // 9,10. Antenna Altitude above/below mean-sea-level (geoid) Units of antenna altitude, meters)
  // 11. Geoidal separation, the difference between the WGS-84 earth ellipsoid and mean-sea-level (geoid),
  // "-" means mean-sea-level below ellipsoid
  // 12. Units of geoidal separation, meters
  // 13. Age of differential GPS data, time in seconds since last SC104
  // type 1 or 9 update, null field when DGPS is not used
  // 14. Differential reference station ID, 0000-1023
end;

// HDT Heading � True
type THDT = packed record
  Heading:F32;
  Valid:BOOL;
end;

// Nmea Sentences
type TNmeaSentences = packed record
  Sentence:string;
  //
  TalkerId:TTalkerId;
  SentenceID:TSentenceId;
  case TSentenceId of
    RMC:(RMC:TRMC;);
    ZDA:(ZDA:TZDA;);
    GSV:(GSV:TGSV;);
    GGA:(GGA:TGGA;);
    HDT:(HDT:THDT;);
end;


const DEFAULT_YEAR = '20'; // 2000

function StrTimeToTime(const ATime:string; var Time:TDateTime):BOOL;
function StrDateToDate(const ADate:string; var Date:TDateTime):BOOL;

IMPLEMENTATION uses SysUtils, StrUtils, DateUtils;

function StrDateToDate(const ADate:string; var Date:TDateTime):BOOL;
var DD,NN,YYYY:U16;
    //HH,MM,SS,CC:U16;
    FDate:string;
begin
  // 12 34 56 78
  // DD MM YY YY
  Result:=False;
  //DecodeDateTime(Now(),YYYY, NN, DD, HH, MM, SS, CC);
  if Trim(ADate) = '' then Exit;
  if Length(ADate) = 6 then begin
    FDate:=LeftStr(ADate, 4) + DEFAULT_YEAR + RightStr(ADate, 2);
  end else
  if Length(ADate) = 8 then begin
    FDate:=ADate;
  end else Exit;
  try
    DD:=StrToInt(LeftStr(FDate, 2));   // DD = '01'..'31'
    NN:=StrToInt(MidStr(FDate, 3, 2)); // MM = '01'..'12'
    YYYY:=StrToInt(RightStr(FDate, 4)); // YYYY = ?
    if ((DD < 1) and (DD > 31)) or ((NN < 1) and (NN > 12)) then Exit;
    Date:=EncodeDate(YYYY, NN, DD);
  except
    on EConvertError do Exit;
  else
    Exit;
  end;
  Result:=True;
end;

function StrTimeToTime(const ATime:string; var Time:TDateTime):BOOL;
var HH,MM,SS,CC:Byte;
begin
  // 12 34 56 7 89
  // HH MM SS . CC
  Result:=False;
  Time:=0;
  if Trim(ATime) = '' then Exit;
  if Length(ATime) < 9 then Exit;
  try
    HH:=StrToInt(LeftStr(ATime, 2));   // HH = '00'..'23'
    MM:=StrToInt(MidStr(ATime, 3, 2)); // MM = '00'..'59'
    SS:=StrToInt(MidStr(ATime, 5, 2)); // SS = '00'..'59'
    CC:=StrToInt(RightStr(ATime, 2));  // CC = '00'..'99'
    if (HH > 23) or (MM > 59) or (SS > 59) or (CC > 99) then Exit;
    Time:=EncodeTime(HH, MM, SS, CC);
  except
    //on EConvertError do Exit;
  else
    Exit;
  end;
  Result:=True;
end;

{*
function StrTimeToTime(const ATime:string; var Time:TDateTime):BOOL;
var FTime:string;
begin
  // 1  3  5  7 8
  // HH MM SS . CC
  Result:=False;
  Time:=0;
  if Trim(ATime) <> '' then begin
    FTime:=Format('%s:%s:%s:%s', [LeftStr(ATime, 2),
                                MidStr(ATime, 3, 2),
                                MidStr(ATime, 5, 2),
                                MidStr(ATime, 8, 2)
                                ]);
    //Time:=StrToTime(FTime);
    Time:=StrToTime(FTime);
    Result:=True;
  end;
end;
*}

{*
function TForm1.SplitMsg(const AMsg:string):BOOL;
var I,CS,RcvdCS:U8;
    C:Char;
    Rst:BOOL;
    DataField:string;
    State:TNMEAState;
begin
  State:=NMEA_SOM; Rst:=True;

  for I:=1 to Length(AMsg) do begin
    C:=AMsg[I];

    case State of

      NMEA_SOM:begin
        if (C = StartOfMsg) then State:=NMEA_GETFIELDS;
      end;

      NMEA_GETFIELDS:begin
        if (C = StartOfMsg) then Rst:=True
        else
        if (C = CSDelimiter) then begin
           Field.Add(DataField);
           State:=NMEA_CS1;
        end else
        if (C = CR) then begin
           Field.Add(DataField);
           State:=NMEA_LF;
        end else
        if (C = FieldDelimiter) then begin
           Field.Add(DataField);
           DataField:='';
        end else begin
          DataField:=DataField + C;
          CS:=CS xor U8(C);
        end;
      end;

      NMEA_CS1:begin
        if (U8(C) - U8('0')) <= 9 then RcvdCS:=(U8(C) - U8('0')) shl 4
                                  else RcvdCS:=(U8(C) - U8('A') + 10) shl 4;
        State:=NMEA_CS2;
      end;

      NMEA_CS2:begin
        if (U8(C) - U8('0')) <= 9 then RcvdCS:=RcvdCS or (U8(C) - U8('0'))
                                  else RcvdCS:=RcvdCS or (U8(C) - U8('A') + 10);
        if CS <> RcvdCS then State:=NMEA_SOM
                        else State:=NMEA_CR;
      end;

      NMEA_CR:begin
        if C <> CR then State:=NMEA_SOM
                   else State:=NMEA_LF;
      end;

      NMEA_LF:begin
        if C <> LF then State:=NMEA_SOM
                   else Result:=True;
      end;
      
    end; // case

    if Rst then begin
      CS:=0;
      //IStrMsg:='';
      DataField:='';
      Field.Clear();
      Result:=False;
      Rst:=False;
    end;

    //IStrMsg:=IStrMsg + C;

  end; // for

  if ((Field.Count = 1) and (Field[0] = '')) then begin
    //IStrMsg:='';
    Result:=False;
  end;

end;
*}

//INITIALIZATION
//FINALIZATION
END.
