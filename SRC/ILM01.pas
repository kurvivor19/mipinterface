UNIT ILM01;
INTERFACE uses  SysUtils, Classes, QExtCtrls, Windows,
  RxStrPktCom,
  RxUtils,
  RxAlias;

type
  TLimDataAvail = procedure(Sender:TObject; X,Y:F64 ) of object;


type TILM01 = class(TRxStrPktCom)
  private
    FOnDataAvail:TLimDataAvail;
    FX,FY:F64;
    FReversY: BOOL;
    FReversX: BOOL;
  protected
    FExState:TExchangeState;
    FDataAvail:BOOL;
    FExCnt:U16;
    FS:TFormatSettings;
    //
    function  DecodePacket():U8; override;
    procedure ProcessData(); virtual;
    procedure SignalTimeout(); override;
  public
    IPacket,OPacket:string;
    //
    constructor Create(AOwner:TComponent); override;
    destructor  Destroy(); override;
    function    Connect():BOOL; virtual;
    function   Disconnect():BOOL; virtual;
    //
    property ReversX:BOOL read FReversX write FReversX default False;
    property ReversY:BOOL read FReversY write FReversY default False;
    property ExState:TExchangeState read FExState;
    property OnDataAvail:TLimDataAvail read FOnDataAvail write FOnDataAvail default NIL;
end;

IMPLEMENTATION  uses StrUtils;

var F:Text;

constructor TILM01.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  Chnl.Baud:=9600;
  Chnl.Parity:=EVENPARITY;
  Chnl.DataBits:=DATABITS_8;
  Chnl.StopBits:=ONESTOPBIT;
  Chnl.StartChar:=':';
  Chnl.PacketSize:=14;
  Chnl.Timeout:=1500;
  //with FPort do begin
  //  Baud:=9600;
  //  Parity:=pEven;
    {$IFDEF WIN32}
  //  DataBits:=8;
  //  StopBits:=1;
  //  TraceName:= 'ILM.TRC';
  //  LogName:= 'ILM.LOG';
    //
    //LogAllHex:=True;
    //LogHex:=True;
    //TraceAllHex:=True;
    //Logging:=tlOn;
  //  Tracing:=tlOn;
    {$ENDIF}
    {$IFDEF LINUX}
  //  DataBits:=dbEight;
  //  StopBits:=sbOne;
    {$ENDIF}
  //end;

  //CreateDataPacketsList(1);
  //with FData[0] do begin
  //  StartCond:=scString;
  //  StartString:=':';
  //  EndCond:=[ecPacketSize];
  //  PacketSize := 14;
  //  IncludeStrings:=True;
  //end;

  FS.DecimalSeparator:=',';
  TimerInit(1000);
  FExState:=exsDisconnected;
end;

function TILM01.DecodePacket():U8;
begin
  //ReStartTriggerTimer();
  IPacket:=InBuffer.FIPacket;

  // 1  2  3  4  5  6  7  8  9  10  11  12  13  14
  // :  +  X  X  ,  X  X     +  Y   Y   ,   Y   Y

  FX:=StrToFloat(MidStr(IPacket,2, 7),FS); // +XX,XX  Theta  ����    ���������� �����
  FY:=StrToFloat(MidStr(IPacket,9,14),FS);   // +YY,YY  Psi    ������  ���������� �����

  if FReversX then FX:=-FX;
  if FReversY then FY:=-FY;
  //WriteLn(F,IPacket);

  FExState:=exsAnswer;
  FDataAvail:=True;
  ProcessData();
  if Assigned(OnDataAvail) then OnDataAvail(Self,FX,FY);
end;

procedure TILM01.ProcessData();
begin
end;

destructor TILM01.Destroy();
begin
  inherited Destroy();
end;

function TILM01.Connect():BOOL;
begin
  Result:=inherited Connect();
  if not Result then Exit;
  FDataAvail:=False;
  //InitTriggerTimer(5*18);
  //StartTriggerTimer();
  //AssignFile(F,'ILM.log'); ReWrite(F);
end;

function TILM01.Disconnect():BOOL;
begin
  FExState:=exsDisconnected;
  //StopTriggerTimer();
  Result:=inherited Disconnect();
  //CloseFile(F);
  if Assigned(OnDataAvail) then OnDataAvail(Self,FX,FY);
end;

procedure TILM01.SignalTimeout;
begin
  FExState:=exsNoAnswered;
  if Assigned(OnDataAvail) then OnDataAvail(Self, FX,FY);
end;

END.
