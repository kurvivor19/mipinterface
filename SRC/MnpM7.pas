UNIT MnpM7;
INTERFACE uses  SysUtils, Classes, QExtCtrls,
  RxStrPktCom,
  Nmea0183,
  RxUnf,
  RxUtils,
  RxAlias;

type
  TMnpPortList = ( mnpCOM0, mnpCOM1 );
  TMnpProtocol = ( mnpDisable, mnpMNPBinary, mnpRBinary, mnpRTCM, mnpNMEA0183 );
  TMnpSentenceList = array[0..15] of BOOL;
  TMnpSatList = array[1..32] of BOOL;
  
  TMnpDataAvail = procedure(Sender:TObject; DT:TDateTime ) of object;


type TMnpM7 = class(TRxUnf)
  private
  protected
    OSentence:string;
    function DecodeProprietary():BOOL; override;
    procedure ProcessData(); override;
    procedure DecodePIREA(); virtual;
    procedure DecodePIRFV(); virtual;
    procedure DecodePIRGK(); virtual;
    procedure DecodePIRRA(); virtual;
  public
    constructor Create(AOwner:TComponent); override;
    destructor  Destroy(); override;
    function    Connect():BOOL; override;
    procedure   Disconnect(); override;
    //
    procedure SetPort(const APort:TMnpPortList; const ABaud:U32; const AProtocol:TMnpProtocol; const S:TMnpSentenceList);
    procedure SelectSatelite(const AGP:TMnpSatList; const AGL:TMnpSatList);
    procedure SetParam(const ACS:TCoordynateSystem; const AHour:S16; const AMin:U16);
    procedure SelfTest();
    //
    //property OnDataAvail:TMnpDataAvail read FOnDataAvail write FOnDataAvail default NIL;
end;

IMPLEMENTATION  uses StrUtils, DateUtils;

constructor TMnpM7.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);

  Chnl[0].Baud:=115200;
  Chnl[1].Baud:=Chnl[0].Baud;  
end;

function TMnpM7.DecodeProprietary():BOOL;
var SentenceId:string[3];
begin
  Result:=False;
  
  if MidStr(Field[0],2,2) <> 'IR' then Exit;
  
  SentenceId:=MidStr(Field[0],4,2);
  case SentenceId[1] of

    'E':case SentenceId[2] of
        'A':begin
            DecodePIREA();
        end;
    end;
    'F':case SentenceId[2] of
        'V':begin
            DecodePIRFV();
        end;
    end;
    'G':case SentenceId[2] of
        'K':begin
            DecodePIRGK();
        end;
    end;
    'R':case SentenceId[2] of
        'A':begin
            DecodePIRRA();
        end;
    end;

    else begin
    end;
    
  end;
end;

procedure TMnpM7.ProcessData();
var DT:TDateTime;
begin
  case FSentence.SentenceID of
    RMC:begin
      with FSentence.RMC do begin
        try
          DT:=EncodeDateTime(Year+2000,Month,Day,Hour,Min,Sec,MSec);
        except
          on EConvertError do Exit;
        end;
      end;
    end;
    else Exit;
  end;
end;

destructor TMnpM7.Destroy();
begin
  inherited Destroy();
end;

function TMnpM7.Connect(): BOOL;
begin
  Result:=inherited Connect();
end;

procedure TMnpM7.Disconnect();
begin
  inherited Disconnect();  
end;

procedure TMnpM7.SelfTest();
begin
  SendPacket('PIRER');
end;

procedure TMnpM7.SetPort(const APort:TMnpPortList; const ABaud:U32; const AProtocol:TMnpProtocol; const S:TMnpSentenceList);
var I,SMask:U16;
begin
  SMask:=0;
  for I:=0 to 15 do
    if S[I] then SMAsk:=SMask or (1 shl I);
  SMAsk:=SMask and $0FFF;

  OSentence:='PIRPR';
  OSentence:=OSentence + ',' + IntToStr(U8(APort));
  OSentence:=OSentence + ',' + IntToStr(ABaud);
  OSentence:=OSentence + ',' + IntToStr(U8(AProtocol));
  OSentence:=OSentence + ',' + IntToHex(SMask,4);
  SendPacket(OSentence);
end;

procedure TMnpM7.SelectSatelite(const AGP,AGL:TMnpSatList);
var I,GPMask,GLMask:U32;
begin
  GPMask:=0; GLMask:=0;
  for I:=1 to 32 do begin
    if AGP[I] then GPMAsk:=GPMask or (1 shl I);
    if AGL[I] then GLMAsk:=GLMask or (1 shl I);
  end;  
  GLMAsk:=GLMask and $00FFFFFF;

  OSentence:='PIRSR';
  OSentence:=OSentence + ',' + IntToHex(GPMask,8);
  OSentence:=OSentence + ',' + IntToHex(GLMask,6);
  OSentence:=OSentence + ',' + '';
  SendPacket(OSentence);
end;

procedure TMnpM7.SetParam(const ACS:TCoordynateSystem; const AHour:S16; const AMin:U16);
begin
  OSentence:='PIRTR';
  OSentence:=OSentence + ',' + IntToStr(U8(ACS));
  OSentence:=OSentence + ',' + IntToStr(AHour);
  OSentence:=OSentence + ',' + IntToStr(AMin);
  SendPacket(OSentence);
end;

procedure TMnpM7.DecodePIREA();
begin

end;

procedure TMnpM7.DecodePIRFV();
begin

end;

procedure TMnpM7.DecodePIRGK();
begin

end;

procedure TMnpM7.DecodePIRRA();
begin

end;

END.
