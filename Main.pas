   UNIT Main;

INTERFACE
  uses {$IFDEF WIN32} Windows, {$ENDIF}
  SysUtils, Types, Classes, QTypes, QGraphics, QControls, QForms,
  QDialogs, QExtCtrls, QButtons, QComCtrls, IniFiles, QStdCtrls, StdConvs,
  Ust, SmartWinner, TrkCtrl, IPCUdp, RxAlias, RxUtils, UstDisplay;

const
   COLOR_NUM = 15;
   ColorConst: array [0..COLOR_NUM] of TColor = (clBlack,
     clMaroon, clGreen, clOlive, clNavy,
     clPurple, clTeal, clGray, clSilver, clRed,
     clLime, clYellow, clBlue, clFuchsia,
     clAqua, clWhite);

type
  TMainForm = class(TForm)
    GBTime: TGroupBox;
    LTime: TLabel;
    LSystemTime: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    BSyncKH: TButton;
    GroupBox2: TGroupBox;
    Label19: TLabel;
    LAz: TLabel;
    LEl: TLabel;
    Label28: TLabel;
    FPCTime: TTimer;
    PView: TGroupBox;
    LHLSN1: TLabel;
    LPLSN1: TLabel;
    LPos1: TLabel;
    LPLSP1: TLabel;
    LHLSP1: TLabel;
    LHLSN2: TLabel;
    LHLSN3: TLabel;
    LPLSN2: TLabel;
    LPLSN3: TLabel;
    LPos2: TLabel;
    LPos3: TLabel;
    LPLSP2: TLabel;
    LPLSP3: TLabel;
    LHLSP2: TLabel;
    LHLSP3: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    LSpd1: TLabel;
    LSpd2: TLabel;
    LSpd3: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GBMovingByAxis: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label15: TLabel;
    EMovePos1: TEdit;
    EMoveSpd1: TEdit;
    BMoveTo1: TButton;
    BBMoveLeft1: TBitBtn;
    BBMoveRigth1: TBitBtn;
    BBMoveRigth2: TBitBtn;
    BMoveTo2: TButton;
    EMoveSpd2: TEdit;
    EMovePos2: TEdit;
    BBMoveLeft2: TBitBtn;
    BBMoveLeft3: TBitBtn;
    EMovePos3: TEdit;
    EMoveSpd3: TEdit;
    BMoveTo3: TButton;
    BBMoveRigth3: TBitBtn;
    BStop1: TButton;
    BStop2: TButton;
    BStop3: TButton;
    BMCStop: TButton;
    TabSheet3: TTabSheet;
    GroupBox7: TGroupBox;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    EPLSN1: TEdit;
    EPLSP1: TEdit;
    BSetPLS1: TButton;
    BSetPLS2: TButton;
    EPLSP2: TEdit;
    EPLSN2: TEdit;
    EPLSN3: TEdit;
    EPLSP3: TEdit;
    BSetPLS3: TButton;
    BRestoreRP1: TButton;
    BRestoreRP2: TButton;
    BRestoreRP3: TButton;
    BLoadTrack: TButton;
    BCancelTrack: TButton;
    GBTrackCor: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    BBAzCorPlus: TBitBtn;
    EAzCor: TEdit;
    BBAzCorMinus: TBitBtn;
    EAzCorStep: TEdit;
    BBElCorPlus: TBitBtn;
    EElCor: TEdit;
    BBElCorMinus: TBitBtn;
    EElCorStep: TEdit;
    BResetAzCor: TButton;
    BResetElCor: TButton;
    Label8: TLabel;
    Label11: TLabel;
    PBLoadTrk: TProgressBar;
    LLoadTrack: TLabel;
    GroupBox1: TGroupBox;
    PBSigLev: TProgressBar;
    LSigLev: TLabel;
    LHdr: TLabel;
    Button1: TButton;
    FileOpenDialog: TOpenDialog;
    GBRadar: TGroupBox;
    Button2: TButton;
    GroupBox3: TGroupBox;
    FWait: TLabel;
    RGAutoTrk: TGroupBox;
    CBAS: TCheckBox;
    CBAT: TCheckBox;
    GroupBox5: TGroupBox;
    FBrk: TLabel;
    FSrvDrv1: TLabel;
    FSrvDrv2: TLabel;
    FSrvDrv3: TLabel;
    FTrk: TLabel;
    FPrg: TLabel;
    FAuto: TLabel;
    FSearch: TLabel;
    FHold: TLabel;
    Image1: TImage;
    ImageRadar: TImage;
    LUTC: TLabel;
    Label16: TLabel;
    GroupBox8: TGroupBox;
    Label30: TLabel;
    Label31: TLabel;
    EAz: TEdit;
    EEl: TEdit;
    Button5: TButton;
    Button6: TButton;
    BPosForCol: TButton;
    BParck: TButton;
    TSMount: TTabSheet;
    GBMounting: TGroupBox;
    Label20: TLabel;
    Label29: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    ESetPos1: TEdit;
    BSetPos1: TButton;
    BBSaveRP1Left: TBitBtn;
    BBSaveRP1Right: TBitBtn;
    BBSaveRP2Right: TBitBtn;
    BSetPos2: TButton;
    ESetPos2: TEdit;
    BBSaveRP2Left: TBitBtn;
    BBSaveRP3Left: TBitBtn;
    ESetPos3: TEdit;
    BSetPos3: TButton;
    BBSaveRP3Right: TBitBtn;
    Button3: TButton;
    LZP1: TLabel;
    LZP2: TLabel;
    LZP3: TLabel;
    Button4: TButton;
    LUnfOn: TLabel;
    LIlmOn: TLabel;
    LBTM: TLabel;
    Button9: TButton;
    GBParam: TGroupBox;
    Label24: TLabel;
    EParkPos1: TEdit;
    EParkPos2: TEdit;
    EParkPos3: TEdit;
    ECP3: TEdit;
    ECP2: TEdit;
    ECP1: TEdit;
    Label23: TLabel;
    BSetParkPos: TButton;
    Button7: TButton;
    Button8: TButton;
    LUPS: TLabel;
    INP1: TLabel;
    INP2: TLabel;
    INP3: TLabel;
    LTst: TLabel;
    CBStandAlone: TCheckBox;
    CBKType: TCheckBox;
    LBKH: TLabel;
    TimeAuto: TCheckBox;
    BSyncPC: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FPCTimeTimer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BBMoveRigth1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BBMoveRigth1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure TabSheet3Show(Sender: TObject);
    procedure LTimeDblClick(Sender: TObject);
    procedure RBATClick(Sender: TObject);
    procedure TSOrientationShow(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure RadarRefresh(Sender: TObject);
    procedure BSyncPCClick(Sender: TObject);
    procedure BSetParkPosClick(Sender: TObject);
    procedure BParckClick(Sender: TObject);
    procedure LUnfOnClick(Sender: TObject);
    procedure CBKTypeClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    TrkCtrl:TTrkCtrl;
    IniFile: TIniFile;
    DllHandle: THandle;
    AppPath,DllPath,PredictPath,UstDir,TleDir,InDir,OutDir: string;
    Ust:TUst;
    FPath: string;
    FUstTempDir: string;
    FUstDir: string;
    FormatSettings: TFormatSettings;
    FTleDir: string;
    //
    IPC:TIPCUdp;
    //
    RotX,RotY,RotZ:F64;
    FTrkLoad:BOOL;
    //
    UnfDeviceName:string;
    //
    procedure SetCorAz(ASign:S8);
    procedure SetCorEl(ASign:S8);
  public
    procedure UpsEvent;
    procedure UnfEvent;
    procedure UnfSetup( What:U8 );
    procedure IlmEvent;
    procedure IlmSetup( What:U8 );
    procedure BTMEvent;
    procedure TrkCtrlDataAvail(Sender:TObject);
  end;


type TRadar = record
  NumOfPnts:U16;
  Az,El:array[1..65535] of F64;
  CAz,CEl:F64;
  Track:BOOL;
  Text:BOOL;
  ElRadar,AzRadar:F64;
  ColorRadar,ColorBack,ColorTrack,ColorPoint:TColor;
  SizeTrack,SizePoint:S32;
  Width,Height:S32;
end;

type PRadar = ^TRadar;

var TimeZone:U8; Dist:F64;
procedure DrawRadar(bmp: Tbitmap; Radar: PRadar);

var MainForm: TMainForm;

IMPLEMENTATION

uses Math, StrUtils, DateUtils, Time, FUPS, Orient, FBTM;

type TAWSStat = packed record
  Time:U32;     // ������� ���� � ����� ��� ����
  TrkCtrl:TTrkCtrlStat; // ��������� ��
  Unf:TUnfStat; // ��������� ���
  Ilm:TIlmStat; // ��������� ���-01 (�����������)
  UPS:TUpsStat; // ��������� ���
  BTM:TBTMStat; // ��������� ���
end;

{$R *.xfm}

//��� ���������� ��������� ���� � ����� ����������
//��������� ��������� ����� ���� ��������� ���������� ��������� ���� � ����� ���
//������������� ������ ����������.
//���� � ����� ��������������� ����� ����������� ������������ TDateTimePicker.
//���� � ����� ������������ � ���������� � API �������.
//�� �������� ���� ���������� 2 ��� ��������� ����������� �������.

procedure SyncPCTime(ATime:TDateTime);
var //vsys : _SYSTEMTIME;
    //vsys : TSystemTime;
    vsys : SYSTEMTIME;
    vYear, vMonth, vDay, vHour, vMin, vSec, vMm : Word;
    T:TDateTime;
begin
   //
   T:=Now();
   DecodeDate( T,  vYear, vMonth, vDay);
   DecodeTime( ATime, vHour, vMin, vSec, vMm );
   vMm := 0;
   vsys.wYear := vYear;
   vsys.wMonth := vMonth;
   vsys.wDay := vDay;
   vsys.wHour := vHour;
   vsys.wMinute := vMin;
   vsys.wSecond := vSec;
   vsys.wMilliseconds := vMm;
   vsys.wDayOfWeek := DayOfWeek( T );
   SetSystemTime( vsys );
   {
   DecodeDate( Trunc(DateOf(ATime)), vYear, vMonth, vDay );
   DecodeTime( ATime, vHour, vMin, vSec, vMm );
   vMm := 0;
   vsys.wYear := vYear;
   vsys.wMonth := vMonth;
   vsys.wDay := vDay;
   vsys.wHour := ( vHour - 2 );
   vsys.wMinute := vMin;
   vsys.wSecond := vSec;
   vsys.wMilliseconds := vMm;
   vsys.wDayOfWeek := DayOfWeek( Trunc(DateOf(ATime)) );
   SetSystemTime( vsys );
   }
end;

var AWSStat:TAWSStat;

var
 Radar:TRadar;
 IP:string; Port:U16;


procedure TMainForm.FormCreate(Sender: TObject);


begin
  Dist:=100.0;

  AppPath:= ExtractFilePath(Application.ExeName);
  UstDir:= AppPath + 'UST\';
  TleDir:= AppPath + 'TLE\';
  PredictPath:= AppPath + 'PREDICT\';

  DecimalSeparator:='.';

  //IniFile:= TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  IniFile:= TIniFile.Create(AppPath + 'aws.ini');

  RGAutoTrk.Enabled:= IniFile.ReadBool('TrkCtrl','ATS',True);
  {
  Radar.AzRadar:= IniFile.ReadInteger('RadarOptions','AzRadar',30);
  Radar.ElRadar:= IniFile.ReadInteger('RadarOptions','ElRadar',15);
  Radar.Text:= IniFile.ReadBool('RadarOptions','Text',True);
  Radar.ColorBack:= ColorConst[IniFile.ReadInteger('RadarOptions','BackgroundColor',0)];
  Radar.ColorRadar:= ColorConst[IniFile.ReadInteger('RadarOptions','RadarColor',15)];
  Radar.ColorTrack:= ColorConst[IniFile.ReadInteger('RadarOptions','TrackColor',11)];
  Radar.ColorPoint:= ColorConst[IniFile.ReadInteger('RadarOptions','PointColor',9)];
  Radar.SizeTrack:= IniFile.ReadInteger('RadarOptions','TrackSize',3);
  Radar.SizePoint:= IniFile.ReadInteger('RadarOptions','PointSize',4);
  {}

  Radar.AzRadar:=30;
  Radar.ElRadar:=15;
  Radar.Text:=True;
  Radar.ColorBack:=clBlack;
  Radar.ColorRadar:=clWhite;
  Radar.ColorTrack:=clYellow;
  Radar.ColorPoint:=clRed;
  Radar.SizeTrack:=3;
  Radar.SizePoint:=5;
  Radar.Width:=ImageRadar.Width;
  Radar.Height:=ImageRadar.Height;


  Ust:= TUst.Create(Self);
  //UST.GMT:= IniFile.ReadInteger('TrkCtrl','GMT',0);

  PageControl1.ActivePageIndex:= 0;
  TSMount.TabVisible:=IniFile.ReadBool('Common','Service',False); 

  FormatSettings.DecimalSeparator:='.';

  //
  //
  EAzCorStep.Text:=IniFile.ReadString('TrkCtrl','AzStep','1');
  EElCorStep.Text:=IniFile.ReadString('TrkCtrl','ElStep','1');


  {
  TrkCtrl:=TTrkCtrl.Create(Self, True);
  TrkCtrl.BlkSize:=IniFile.ReadInteger('TrkCtrl','BlkSize',1);
  TrkCtrl.DeviceName:=IniFile.ReadString('Connect','TrkCtrl','');

  TrkCtrl.Axis[1].CP:=IniFile.ReadFloat('TrkCtrl','CP1',0.0);
  TrkCtrl.Axis[2].CP:=IniFile.ReadFloat('TrkCtrl','CP2',-90.0);
  TrkCtrl.Axis[3].CP:=IniFile.ReadFloat('TrkCtrl','CP3',0.0);
  
  TrkCtrl.Axis[1].CP1:=IniFile.ReadFloat('TrkCtrl','SO1',0.0);
  TrkCtrl.Axis[2].CP1:=IniFile.ReadFloat('TrkCtrl','SO2',0.0);
  TrkCtrl.Axis[3].CP1:=IniFile.ReadFloat('TrkCtrl','SO3',0.0);

  TrkCtrl.OnDataAvail:=TrkCtrlDataAvail;

  for I:=1 to 3 do begin
    E:=TEdit(FindComponent('EParkPos'+IntToStr(I)));
    E.Text:=IniFile.ReadString('TrkCtrl','SO'+IntToStr(I),'0.0');
    TrkCtrl.Axis[I].CP1:=StrToFloat(E.Text,FormatSettings);

    E:=TEdit(FindComponent('ECP'+IntToStr(I)));
    E.Text:=IniFile.ReadString('TrkCtrl','CP'+IntToStr(I),'0.0');
    TrkCtrl.Axis[I].CP:=StrToFloat(E.Text,FormatSettings);
  end;
  {}
  IniFile.Free;

  TrkCtrl.TrkLoLimit:=90.0; if CBKType.Checked then TrkCtrl.TrkLoLimit:=78.5;
end;

procedure TMainForm.FormShow(Sender: TObject);
var I:U8; E:TEdit;
begin
  RadarRefresh(MainForm);

  //IniFile:= TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  IniFile:= TIniFile.Create(AppPath + 'aws.ini');

  IPC:=TIPCUdp.Create(Self,False);
  IP:=IniFile.ReadString('IPC','IP','');
  Port:=IniFile.ReadInteger('IPC','Port',0);

  TrkCtrl:=TTrkCtrl.Create(Self, True);
  TrkCtrl.BlkSize:=IniFile.ReadInteger('TrkCtrl','BlkSize',1);
  TrkCtrl.DeviceName:=IniFile.ReadString('Connect','TrkCtrl','');

  TrkCtrl.Axis[1].CP:=IniFile.ReadFloat('TrkCtrl','CP1',0.0);
  TrkCtrl.Axis[2].CP:=IniFile.ReadFloat('TrkCtrl','CP2',-90.0);
  TrkCtrl.Axis[3].CP:=IniFile.ReadFloat('TrkCtrl','CP3',0.0);
  
  TrkCtrl.Axis[1].CP1:=IniFile.ReadFloat('TrkCtrl','SO1',0.0);
  TrkCtrl.Axis[2].CP1:=IniFile.ReadFloat('TrkCtrl','SO2',0.0);
  TrkCtrl.Axis[3].CP1:=IniFile.ReadFloat('TrkCtrl','SO3',0.0);

  TrkCtrl.OnDataAvail:=TrkCtrlDataAvail;

  for I:=1 to 3 do begin
    E:=TEdit(FindComponent('EParkPos'+IntToStr(I)));
    E.Text:=IniFile.ReadString('TrkCtrl','SO'+IntToStr(I),'0.0');
    TrkCtrl.Axis[I].CP1:=StrToFloat(E.Text,FormatSettings);

    E:=TEdit(FindComponent('ECP'+IntToStr(I)));
    E.Text:=IniFile.ReadString('TrkCtrl','CP'+IntToStr(I),'0.0');
    TrkCtrl.Axis[I].CP:=StrToFloat(E.Text,FormatSettings);
  end;  

  UpsForm.DeviceName:=IniFile.ReadString('Connect','UPS','');
  UpsForm.OnEvent:=UpsEvent;

  UnfForm.IlmDeviceName:=IniFile.ReadString('Connect','ILM','');
  UnfForm.UnfDeviceName:=IniFile.ReadString('Connect','UNF','');
  {}
  UnfForm.OnUnfEvent:=UnfEvent;
  UnfForm.OnUnfSetup:=UnfSetup;
  UnfForm.OnIlmEvent:=IlmEvent;
  UnfForm.OnIlmSetup:=IlmSetup;
  {}

  TimeZone:=IniFile.ReadInteger('Position','TimeZone',0);
  UnfForm.TimeZone:=TimeZone;
  TrkCtrl.TimeZone:=TimeZone;

  UnfForm.RotX:=IniFile.ReadFloat('Orientation','RotX',0.0);
  UnfForm.RotXAdjustment:=IniFile.ReadFloat('Orientation','RotXAdjustment',0.0);
  RotX:=UnfForm.RotX + UnfForm.RotXAdjustment;


  UnfForm.RotY:=IniFile.ReadFloat('Orientation','RotY',0.0);
  UnfForm.RotYAdjustment:=IniFile.ReadFloat('Orientation','RotYAdjustment',0.0);
  RotY:=UnfForm.RotY + UnfForm.RotYAdjustment;


  UnfForm.RotZ:=IniFile.ReadFloat('Orientation','RotZ',0.0);
  UnfForm.RotZAdjustment:=IniFile.ReadFloat('Orientation','RotZAdjustment',0.0);
  RotZ:=UnfForm.RotZ + UnfForm.RotZAdjustment;

  UnfForm.Lat:=IniFile.ReadFloat('Position','Latitude',0.0);
  UnfForm.Long:=IniFile.ReadFloat('Position','Longitude',0.0);
  UnfForm.Alt:=IniFile.ReadFloat('Position','Altitude',0.0);

  TrkCtrl.RotX:=RotX;
  TrkCtrl.RotY:=RotY;
  TrkCtrl.RotZ:=RotZ;  


  BtmForm.DeviceName:=IniFile.ReadString('Connect','BTM','');
  BtmForm.OnEvent:=BTMEvent;

  IniFile.Free;



  TrkCtrl.Connect();
  UpsForm.Connect();
  BtmForm.Connect();
  UnfForm.Connect();
  IPC.Connect;
end;

procedure TMainForm.TrkCtrlDataAvail(Sender:TObject);
const
  LSName: array[1..4] of string = ('LHLSN','LPLSN','LPLSP','LHLSP');
var I,N:U8;
   LimStat:array[1..4] of BOOL;
begin

  LTst.Caption:=TCModeStr[U8(TrkCtrl.Mode)] + CRLF +
            Format('%10d',[TrkCtrl.FNextPnt])  + CRLF;

  RadarRefresh(MainForm);

  case TrkCtrl.ET of
    exsAnswer:begin

    LBKH.Color:= clLime;

    LTime.Caption:=FormatDateTime('hh:mm:ss',TrkCtrl.Time); // ������� ����� ��
    LAz.Caption:= FormatFloat('0.00',TrkCtrl.Az); // ������� ��
    LEl.Caption:= FormatFloat('0.00',TrkCtrl.El); // ������� ��
    LSigLev.Caption:= FormatFloat('0.00%',TrkCtrl.SL);
    PBSigLev.Position:=Floor(TrkCtrl.SL);   // ������� �������

    if (TrkCtrl.Brk) then FBrk.Color:= clLime else FBrk.Color:= clGray;
    if (TrkCtrl.Hold) then FHold.Color:= clLime else FHold.Color:= clGray;

    case TrkCtrl.TM of
      tmNone:begin
        FWait.Color:= clGray;
        FTrk.Color:= clGray;
        FPrg.Color:= clGray;
        FAuto.Color:= clGray;
        FSearch.Color:= clGray;
      end;
      tmWait:begin
        FWait.Color:= clLime;
        FTrk.Color:= clGray;
        FPrg.Color:= clGray;
        FAuto.Color:= clGray;
        FSearch.Color:= clGray;
      end;
      tmPrg:begin
        FWait.Color:= clGray;
        FTrk.Color:= clLime;
        FPrg.Color:= clLime;
        FAuto.Color:= clGray;
        FSearch.Color:= clGray;
      end;
      tmSearch:begin
        FWait.Color:= clGray;
        FTrk.Color:= clLime;
        FPrg.Color:= clLime;
        FAuto.Color:= clGray;
        FSearch.Color:= clLime;
      end;
      tmAuto:begin
        FWait.Color:= clGray;
        FTrk.Color:= clLime;
        FPrg.Color:= clGray;
        FAuto.Color:= clLime;
        FSearch.Color:= clGray;
      end;
    end;

   for I:= 1 to 3 do begin

      TLabel(FindComponent('LPos'+IntToStr(I))).Caption:=FormatFloat('0.00',TrkCtrl.Axis[I].Pos);
      TLabel(FindComponent('LSpd'+IntToStr(I))).Caption:=FormatFloat('0.00',Abs(TrkCtrl.Axis[I].Spd));

      //INP
        with TLabel(FindComponent('INP'+IntToStr(I))) do begin
          if (TrkCtrl.Axis[I].INP) then begin Color:=clRed; Font.Color:=clWhite;
          end else begin Color:= clBackground; Font.Color:= clGray; end;
        end;

      // -�� -���  ���+ ��+
      for N:= 1 to 4 do
        with TLabel(FindComponent(LSName[N]+IntToStr(I))) do begin
          if (TrkCtrl.Axis[I].LimStat[N]) then begin Color:=clRed; Font.Color:=clWhite;
          end else begin Color:= clBackground; Font.Color:= clGray; end;
        end;

      with TLabel(FindComponent('FSrvDrv'+IntToStr(I))) do begin
        if (TrkCtrl.Axis[I].Alm) then Color:= clRed
        else
        if (TrkCtrl.Axis[I].Rdy) then Color:= clLime else Color:= clGray;
      end;

      // ���� �������� ���������� -��� , ���+
      if (TrkCtrl.Axis[I].Limits) then begin
        TEdit(FindComponent('EPLSN'+IntToStr(I))).Text:=FormatFloat('0.00',TrkCtrl.Axis[I].PlsnPos);
        TEdit(FindComponent('EPLSP'+IntToStr(I))).Text:=FormatFloat('0.00',TrkCtrl.Axis[I].PlspPos);
        TEdit(FindComponent('LZP'+IntToStr(I))).Text:=FormatFloat('0.00',TrkCtrl.Axis[I].ZPPos);
      end;

    end; {for I}

    Radar.CEl:=TrkCtrl.El;
    Radar.CAz:=TrkCtrl.Az;


    {
      if TrkCtrl.TrackLoading then begin
        PBLoadTrk.Visible:=True; LLoadTrack.Visible:=True;
        PBLoadTrk.Position:= TrkCtrl.LoadPercent;
        LLoadTrack.Caption:= Format('%5d/%5d',[TrkCtrl.NumOfBlk,TrkCtrl.NumOfBlks]);
      end else begin PBLoadTrk.Visible:=False; LLoadTrack.Visible:=False; end;
    }
    if PBLoadTrk.Visible then begin
      if TrkCtrl.TrackLoading then begin
        PBLoadTrk.Position:= TrkCtrl.LoadPercent;
        LLoadTrack.Caption:= Format('%5d/%5d',[TrkCtrl.NumOfBlk,TrkCtrl.NumOfBlks]);
      end else begin PBLoadTrk.Visible:=False; LLoadTrack.Visible:=False; end;
    end;

    end;

    exsNoAnswered,
    exsDisconnected:begin
      LBKH.Color:= clGray;
      FHold.Color:= clGray;
      FWait.Color:= clGray;
      FTrk.Color:= clGray;
      FPrg.Color:= clGray;
      FAuto.Color:= clGray;
      FSearch.Color:= clGray;
      LSigLev.Caption:='0.00%'; PBSigLev.Position:=0;
      FBrk.Color:= clGray;
      for I:= 1 to 3 do TLabel(FindComponent('FSrvDrv'+IntToStr(I))).Color:=clGray;
      TrkCtrl.Stop;
    end;

  end;




  AWSStat.Time:=DateTimeToTimeStamp(Now()).Time;
  AWSStat.TrkCtrl:=TrkCtrl.TrkCtrlStat;
  AWSStat.Unf:=Orient.UnfForm.UNFStat;
  AWSStat.Ilm:=Orient.UnfForm.ILMStat;
  AWSStat.UPS:=FUPS.UpsForm.UPSStat;
  AWSStat.BTM:=FBTM.BtmForm.BTMStat;
  IPC.SendTo(Ip, Port, AWSStat, Sizeof(TAWSStat));

end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Ust.Free();
  //FTimer.Enabled:= False;
  //FTimer.Free;
  //Disconnect();
  TrkCtrl.Disconnect();
  UnfForm.Disconnect();
  UpsForm.Disconnect();
  BtmForm.Disconnect();

  // Ust.Free();
  TrkCtrl.Free;
  IPC.Free;
end;


procedure TMainForm.FPCTimeTimer(Sender: TObject);
begin
  LSystemTime.Caption:= FormatDateTime('hh:mm:ss',Now());
end;

procedure TMainForm.Button1Click(Sender: TObject);
var Btn:U16; ANum:U8; ADir:S8; I:U16;  Az,El:F64;
begin
  Btn:= Abs((Sender as TButton).Tag);
  case Btn of
    // ���������� ������� ����� � ��
    81:TrkCtrl.SetTime(Now());
    // ������� �� ���� ����
    100: begin
      TrkCtrl.Stop();
      Radar.Track:=False;
      TabSheet2.Enabled:=TRUE;
      TabSheet3.Enabled:=TRUE;
    end;
    // ����������� ������ �������
    41,42,43: begin
      ANum:=Btn-40;
      TrkCtrl.Axis[ANum].RestoreZP();
    end;
    // ����������� ������ �������
    51,52,53:begin
      ANum:=Btn-50;
      TrkCtrl.Axis[ANum].SetPos(StrToFloat(TEdit(FindComponent('ESetPos'+IntToStr(ANum))).Text));
    end;
    // ����������� �� ��� � �������� ��������� � �������� ���������
    101,102,103:begin
    ANum:=Btn-100;
    if ((Sender as TButton).Tag < 0) then TrkCtrl.Axis[ANum].MoveTo(0.0,0.0)
    else
    TrkCtrl.Axis[ANum].MoveTo(
      StrToFloat(TEdit(FindComponent('EMovePos'+IntToStr(ANum))).Text),
      StrToFloat(TEdit(FindComponent('EMoveSpd'+IntToStr(ANum))).Text)
      );
    end;
    // ��������� ���
    71,72,73:begin
      ANum:=Btn-70;
      TrkCtrl.Axis[ANum].SetLimits(
        StrToFloat(TEdit(FindComponent('EPLSN'+IntToStr(ANum))).Text),
        StrToFloat(TEdit(FindComponent('EPLSP'+IntToStr(ANum))).Text)
      );
    end;
    // ������ ��������� ����������
    21:begin
      SetCorAz(Sign((Sender as TButton).Tag));
    end;
    22:begin
      SetCorEl(Sign((Sender as TButton).Tag));
    end;
    24: begin
      EAzCor.Text:= FloatToStr(0.0);
      TrkCtrl.SetCorr( StrToFloat(EAzCor.Text),StrToFloat(EElCor.Text),0);
    end;
    25: begin
      EElCor.Text:= FloatToStr(0.0);
      TrkCtrl.SetCorr( StrToFloat(EAzCor.Text),StrToFloat(EElCor.Text),0);
    end;
    // �������� ����������
    230: begin
      FileOpenDialog.InitialDir:= UstDir;
      if (not FileOpenDialog.Execute) then Exit;
      //Ust.Read(FileOpenDialog.FileName);

      UstForm.ReadFile(FileOpenDialog.FileName);
      UstForm.ShowModal();
      if UstForm.ModalResult=mrOk then
      begin
        TrkCtrl.LoadTrack(FileOpenDialog.FileName,CBAT.Checked, CBAS.Checked, CBStandAlone.Checked);

        PBLoadTrk.Visible:=True; PBLoadTrk.Position:=0; LLoadTrack.Visible:=True;

        FTrkLoad:=True;

        Radar.NumOfPnts:=UstForm.Ust.Hdr.NumOfPnts;
        for I:=1 to UstForm.Ust.Hdr.NumOfPnts do begin
          Az:=UstForm.Ust.Pnt[I].Az;
          El:=UstForm.Ust.Pnt[I].El;
          Radar.Az[I]:=Az;
          Radar.El[I]:=El;
        end;
        Radar.Track:=True;
        RadarRefresh(MainForm);
      end;
    end;


    141,142,143:begin
      ANum:=Btn-140;
      if((Sender as TButton).Tag < 0) then ADir:= -1 else ADir:= 1;
      TrkCtrl.Axis[ANum].SaveZP(ADir);
    end;
    250:begin
      TrkCtrl.MoveTo(StrToFloat(EAz.Text),StrToFloat(EEl.Text));
    end;
    255:begin
      TrkCtrl.Axis[1].GetLimits();   // ��������� ���������� -���,���+
      TrkCtrl.Axis[2].GetLimits();
      TrkCtrl.Axis[3].GetLimits();
    end;

    260:begin
      TrkCtrl.PowerOn();
      (Sender as TButton).Caption:='����';
      (Sender as TButton).Tag:=261;
    end;
    261:begin
      TrkCtrl.PowerOff();
      (Sender as TButton).Caption:='���';
      (Sender as TButton).Tag:=260;
    end;

    262:UnfForm.Show;

  end; {case}
end;

procedure TMainForm.BBMoveRigth1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var I,ANum:U8; Spd:array[1..3] of F64;
begin
  // �������� �� ���� ���������
  for I:= 1 to 3 do Spd[I]:= 0.0;
  ANum:=Abs((Sender as TButton).Tag);
  Spd[ANum]:= StrToFloat(TEdit(FindComponent('EMoveSpd'+IntToStr(ANum))).Text);
  if ((Sender as TButton).Tag < 0) then Spd[ANum]:= -Spd[ANum];
  TrkCtrl.MoveBySpd(Spd[1],Spd[2],Spd[3]);
end;

procedure TMainForm.BBMoveRigth1MouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  TrkCtrl.Stop();
end;

procedure TMainForm.TabSheet3Show(Sender: TObject);
var I:U8;
begin
  // ��������� ������� �� ����
  for I:=1 to 3 do TrkCtrl.Axis[I].GetLimits();
end;

procedure TMainForm.LTimeDblClick(Sender: TObject);
begin
  TimeForm.Top:= MainForm.Top + GBTime.Top + LTime.Top;
  TimeForm.Left:= MainForm.Left + GBTime.Left + LTime.Left;
  TimeForm.Time:= Now;
  TimeForm.ShowModal;
  if (TimeForm.ModalResult <> mrOK) then Exit;
  // ��������� ����� �� �������� �������������
  TrkCtrl.SetTime(TimeForm.Time);
end;

procedure TMainForm.RBATClick(Sender: TObject);
begin
  
  if (CBAT.Checked) then begin
    CBAS.Enabled:= True;
    GBTrackCor.Enabled:= False;
  end else begin
    CBAS.Enabled:= False;
    GBTrackCor.Enabled:= True;
  end;

end;

procedure TMainForm.TSOrientationShow(Sender: TObject);
begin
  // ��������� ������� �� ����
  TrkCtrl.Axis[1].GetLimits();
  TrkCtrl.Axis[2].GetLimits();
  TrkCtrl.Axis[3].GetLimits();
end;

// Windows only runnable part
function ExecAndWait(const FileName,Params:ShortString; const WinState:U16):BOOL; export;
var
  StartInfo: TStartupInfo;
  ProcInfo: TProcessInformation;
  CmdLine: ShortString;
begin
  { �������� ��� ����� ����� ���������, � ����������� ���� �������� � ������ Win9x }
  CmdLine:= '"' + Filename + '" ' + Params;
  FillChar(StartInfo, SizeOf(StartInfo), #0);
  with StartInfo do begin
    cb:= SizeOf(StartInfo);
    dwFlags:= STARTF_USESHOWWINDOW;
    wShowWindow:= WinState;
  end;
  Result:= CreateProcess(nil, PChar(String(CmdLine)), nil, nil, false,
                         {CREATE_NEW_CONSOLE}DETACHED_PROCESS or NORMAL_PRIORITY_CLASS, nil,
                         PChar(ExtractFilePath(Filename)),StartInfo,ProcInfo);
  { ������� ���������� ���������� }
  if (Result) then begin
 //   WaitForSingleObject(ProcInfo.hProcess, INFINITE);
    { Free the Handles }
    CloseHandle(ProcInfo.hProcess);
    CloseHandle(ProcInfo.hThread);
  end;
end;

procedure TMainForm.Button2Click(Sender: TObject);
var h: HWND;
begin
  ExecAndWait(PredictPath + 'predict.exe', '', sw_ShowNormal);
  Sleep(100);
  h:= FindWindow(nil,'������ ������ ���� UST');
  if (h <> 0) then
    SetWindowPos(h, HWND_TOP , 100, 100, 740, 480, SWP_SHOWWINDOW);
end;
// Windows only runnable part

///--------
procedure TMainForm.BSyncPCClick(Sender: TObject);
var SysTime:TSystemTime;
    CurDate:TDateTime;
begin
  CurDate:=SysUtils.Date;
  CurDate:=CurDate+TimeOf(UnfForm.UTCTime);
  DateTimeToSystemTime(CurDate,SysTime);
  SetSystemTime(SysTime);
end;

procedure TMainForm.BSetParkPosClick(Sender: TObject);
var I:U8; E:TEdit;
begin
  IniFile:= TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));

  for I:=1 to 3 do begin
    E:=TEdit(FindComponent('EParkPos'+IntToStr(I)));
    TrkCtrl.Axis[I].CP1:=StrToFloat(E.Text,FormatSettings);
    IniFile.WriteString('TrkCtrl','SO'+IntToStr(I),E.Text);

    E:=TEdit(FindComponent('ECP'+IntToStr(I)));
    TrkCtrl.Axis[I].CP:=StrToFloat(E.Text,FormatSettings);
    IniFile.WriteString('TrkCtrl','CP'+IntToStr(I),E.Text);
  end;

  IniFile.Free;
end;

procedure TMainForm.BParckClick(Sender: TObject);
begin
  case (Sender as TButton).Tag of
    0:TrkCtrl.Park;
    1:TrkCtrl.MoveToCollapsePos;
    2:TrkCtrl.MoveToPosForShutterOpen;
  end;
end;

procedure TMainForm.LUnfOnClick(Sender: TObject);
begin
  case (Sender as TLabel).Tag of
    1,2:UnfForm.Show();
    3:UpsForm.Show();
    4:BtmForm.Show();
  end;
end;

procedure TMainForm.UnfEvent;
var T:TDateTime;
begin
  case UnfForm.UnfET of
    exsAnswer:begin
      T:=IncHour(UnfForm.UTCTime,TimeZone);
      LUnfOn.Color:=clLime;
      LUTC.Caption:=TimeToStr(T);
      BSyncPC.Enabled:=True;
      LUTC.Font.Color:=clNavy;
      if TimeAuto.Checked then begin
        TrkCtrl.SetTime(T);
        //T:=EncodeTime(15,10,05,00);
        SyncPCTime(UnfForm.UTCTime);
        TimeAuto.Checked:=False;
      end;
    end;
    exsNoAnswered,
    exsDisconnected:begin
      LUnfOn.Color:=clGray;
      BSyncPC.Enabled:=False;
      LUTC.Caption:='00:00:00'; LUTC.Font.Color:=clGray;
    end;
  end;
end;

procedure TMainForm.UpsEvent;
begin
  LUps.Caption:='��� ';
  case UpsForm.State of
    upsNoAnswered,
    upsDisconnected:begin
      LUps.Color:=clGray;
    end;
    upsConnected,
    upsOff,
    upsOn,
    upsWaitShutdown,
    upsShutdown:begin
      //LUps.Caption:=LUps.Caption + IntToStr(Form3.SOC) + '%';
      LUps.Caption:=Format('���  %3d%% / %3d%%',[UpsForm.Load,UpsForm.SOC]);
      if UpsForm.Alm then LUps.Color:=clRed else LUps.Color:=clLime;
    end;
  end;

end;

procedure TMainForm.BTMEvent;
begin
  case BtmForm.ET of
    exsAnswer:begin
      LBTM.Color:=clLime;
    end;
    exsNoAnswered,
    exsDisconnected:begin
      LBTM.Color:=clGray;
    end;
  end;
end;

procedure TMainForm.IlmEvent;
begin
  case UnfForm.IlmET of
    exsAnswer:begin
      LIlmOn.Color:=clLime;
    end;
    exsNoAnswered,
    exsDisconnected:begin
      LIlmOn.Color:=clGray;
    end;
  end;
end;

procedure TMainForm.UnfSetup(What: U8);
begin
  IniFile:= TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  case What of
    1:begin
      RotZ:=UnfForm.RotZ + UnfForm.RotZAdjustment;
      TrkCtrl.RotZ:=RotZ;
      IniFile.WriteString('Orientation','RotZ',' ' + FormatFloat('0.00',UnfForm.RotZ));
    end;
    3:begin
      IniFile.WriteString('Position','Latitude',' '  + FormatFloat('0.00',UnfForm.Lat));
      IniFile.WriteString('Position','Longitude',' ' + FormatFloat('0.00',UnfForm.Long));
      IniFile.WriteString('Position','Altitude',' '  + FormatFloat('0.00',UnfForm.Alt));
    end;    
    4:begin
      TimeZone:=UnfForm.TimeZone;
      TrkCtrl.TimeZone:=TimeZone;
      IniFile.WriteString('Position','TimeZone',' ' + IntToStr(TimeZone));
    end;
  end;
  IniFile.Free;
end;

procedure TMainForm.IlmSetup(What: U8);
begin
  IniFile:= TIniFile.Create(ChangeFileExt(Application.ExeName,'.ini'));
  case What of
    0:begin
      RotX:=UnfForm.RotX + UnfForm.RotXAdjustment;
      RotY:=UnfForm.RotY + UnfForm.RotYAdjustment;
      TrkCtrl.RotX:=RotX;
      TrkCtrl.RotY:=RotY;
      IniFile.WriteString('Orientation','RotX',' ' + FormatFloat('0.00',UnfForm.RotX));
      IniFile.WriteString('Orientation','RotY',' ' + FormatFloat('0.00',UnfForm.RotY));
    end;
  end;
  IniFile.Free;
end;

procedure DrawRadar(bmp: TBitmap; Radar:PRadar);
var

  d,R,x0,y0,x1,y1,x2,y2,x90,y90,x,y,E,A,N,xT,yT,I:S32;
  Az,El:F64;
begin
  bmp.Width:= Radar^.Width;
  bmp.Height:= Radar^.Height;

  with bmp do begin

    d:= 20; /// ������ �����

    /// ����������
    if (Width > Height) then begin
      x0:= Trunc(Width/2);
      y0:= d;
      x1:= Trunc((Width-Height)/2)+d;
      y1:= d;
      x2:= Trunc((Width+Height)/2)-d;
      y2:= Height-d;
      x90:= Trunc(Width/2);
      y90:= Trunc(Height/2);
      R:= Trunc(Height/2)-d;
    end else begin
      x0:= Trunc(Width/2);
      y0:= Trunc((Height-Width)/2)+d;
      x1:= d;
      y1:= Trunc((Height-Width)/2)+d;
      x2:= Width-d;
      y2:= Trunc((Height+Width)/2)-d;
      x90:= Trunc(Width/2);
      y90:= Trunc(Height/2);
      R:= Trunc(Width/2)-d;
    end;

    /// ������������� �������
    with Canvas do begin
      Pen.Color:= Radar^.ColorBack;
      Brush.Color:= Radar^.ColorBack;
      Rectangle(0,0,Width,Height);
    end;

    /// ����� ������ � �����
    with Canvas do begin
      pen.color:= Radar^.ColorRadar;
      Font.Name:= 'Tahoma';
      Font.Color:= Radar^.ColorRadar;
      Font.Size:= 7;
    end;

    /// ���������� � ����� �� ���� �����
    E:= 0;
    N:= 0;
    while (E < R) do
      with Canvas do begin
        Ellipse(x1+E,y1+E,x2-E,y2-E);

        if (Radar^.Text){ = True} then begin
          if (Radar^.ElRadar*N > 0) and (Radar^.ElRadar*N < 90) then TextOut(xT, yT, FloatToStr(Radar^.ElRadar*N));
          inc(N);
        end;

        inc(E,Round(R*Radar^.ElRadar/90));
        xT:= Trunc(Width/2)+2;
        yT:= y1+E-10;
      end;

    /// ������������ ����� � ������������ �����
    A:= 0;
    N:= 0;
    while (A < 360) do
      with Canvas do begin
        MoveTo(x90,y90);
        x:= x0+Trunc(R*sin(A*pi/180));
        y:= y0+Trunc(R*(1-cos(A*pi/180)));
        LineTo(x,y);

        If (Radar.Text){ = True} then begin
          case A of
            0: begin
              xT:= x-2;
              yT:= y-11;
            end;
            1..89: begin
              xT:= x+2;
              yT:= y-11;
            end;
            90: begin
              xT:= x+2;
              yT:= y-6;
            end;
            91..179: begin
              xT:= x+2;
              yT:= y+1;
            end;
            180: begin
              xT:= x-6;
              yT:= y+1;
            end;
            181..269: begin
              xT:= x-16;
              yT:= y+1;
            end;
            270: begin
              xT:= x-15;
              yT:= y-6;
            end;
            271..359: begin
              xT:= x-16;
              yT:= y-11;
            end;
          end;

          TextOut(xT, yT, FloatToStr(Radar^.AzRadar*N));
          inc(N);//N:=N+1;
        end; {if Radar1.Text...}

        inc(A,Round(Radar^.AzRadar));//A:=A+Round(Radar1.AzRadar);
      end; {with Canvas...}

    /// ����������
    if (Radar^.Track){ = True} then begin
      //Az:=Ust.Az[1];
      //El:=Ust.El[1];
      //Az:=Ust.Pnt[1].Az;
      //El:=Ust.Pnt[1].El;
      //CK1ToCK1R(Az,El, RotX,RotY,RotZ);
      Az:=Radar.Az[1];
      El:=Radar.El[1];
      y:= Round(R*El/90);
      x:= x0 + round((R-y)*sin(Az*pi/180));
      y:= y0 + y + round((R-y)*(1-cos(Az*pi/180)));

      Canvas.MoveTo(x,y);
      //for I:= 2 to Ust.NumOfPnts do begin
      for I:= 2 to UstForm.Ust.Hdr.NumOfPnts do begin
        //Az:=Ust.Az[I];
        //El:=Ust.El[I];
        //Az:=Ust.Pnt[I].Az;
        //El:=Ust.Pnt[I].El;
        //CK1ToCK1R(Az,El, RotX,RotY,RotZ);
        Az:=Radar^.Az[I];
        El:=Radar^.El[I];
        y:= Round(R*El/90);
        x:= x0 + round((R-y)*sin(Az*pi/180));
        y:= y0 + y + round((R-y)*(1-cos(Az*pi/180)));
        with Canvas do begin
          Pen.Color:= Radar.ColorTrack;
          Pen.Width:= Radar.SizeTrack;
          LineTo(x,y);
          Pen.Width:= 1;
        end;
      end;
      y:= Round(R*UstForm.Ust.Pnt[1].El/90);
      x:= x0 + round((R-y)*sin(UstForm.Ust.Pnt[1].Az*pi/180));
      y:= y0 + y + round((R-y)*(1-cos(UstForm.Ust.Pnt[1].Az*pi/180)));
        with Canvas do begin;
          Pen.Color:=clYellow;;
          Brush.Color:=clYellow;;
          Ellipse(x-Radar^.SizePoint,y-Radar^.SizePoint,x+Radar^.SizePoint,y+Radar^.SizePoint);
        end;  

    end;



    /// �������
    //y:= Round(R*Radar1.ElPoint/90);
    y:= Round(R*abs(Radar^.CEl)/90);
    x:= x0 + round((R-y)*sin(Radar^.CAz*pi/180));
    y:= y0 + y + round((R-y)*(1-cos(Radar^.CAz*pi/180)));
    with Canvas do begin
      if Radar^.CEl >= 0 then begin
      Pen.Color:= Radar^.ColorPoint;
      Brush.Color:= Radar^.ColorPoint;
      end else begin
      Pen.Color:= clSilver;
      Brush.Color:= clSilver;
      end;
      Ellipse(x-Radar^.SizePoint,y-Radar^.SizePoint,x+Radar^.SizePoint,y+Radar^.SizePoint);
    end;
  end; {with bmp}


end;


procedure TMainForm.RadarRefresh(Sender: TObject);
var
  bmp: Tbitmap;
begin
  bmp:= Tbitmap.create;
  DrawRadar(bmp, @Radar);
  ImageRadar.Canvas.Draw(0,0,bmp);
  bmp.Free;
end;  

procedure TMainForm.CBKTypeClick(Sender: TObject);
begin
  TrkCtrl.TrkLoLimit:=90.0;
  if(Sender as TCheckBox).Checked then TrkCtrl.TrkLoLimit:=78.5;
end;


procedure TMainForm.FormKeyPress(Sender: TObject; var Key: Char);
begin
  case Key of
    'A','a':SetCorAz(-1);
    'D','d':SetCorAz(1);
    'W','w':SetCorEl(1);
    'S','s':SetCorEl(-1);
  end;
end;

procedure TMainForm.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    4116:SetCorAz(1);
    4114:SetCorAz(-1);
    4115:SetCorEl(1);
    4117:SetCorEl(-1);
  end;
end;

procedure TMainForm.SetCorAz(ASign:S8);
begin
  //EAzCor.Text:= FloatToStr(StrToFloat(EAzCor.Text) + Sign((Sender as TButton).Tag)*StrToFloat(EAzCorStep.Text));
  EAzCor.Text:= FloatToStr(StrToFloat(EAzCor.Text) + ASign*StrToFloat(EAzCorStep.Text));
  TrkCtrl.SetCorr( StrToFloat(EAzCor.Text),StrToFloat(EElCor.Text),0);
end;

procedure TMainForm.SetCorEl(ASign:S8);
begin
  //EElCor.Text:= FloatToStr(StrToFloat(EElCor.Text)  +Sign((Sender as TButton).Tag)*StrToFloat(EElCorStep.Text));
  EElCor.Text:= FloatToStr(StrToFloat(EElCor.Text)  + ASign*StrToFloat(EElCorStep.Text));
  TrkCtrl.SetCorr( StrToFloat(EAzCor.Text),StrToFloat(EElCor.Text),0);
end;


END.



