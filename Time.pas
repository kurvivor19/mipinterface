unit Time;

interface

uses
  SysUtils, Types, Classes, Variants, QTypes, QGraphics, QControls, QForms, 
  QDialogs, QStdCtrls;

type
  TTimeForm = class(TForm)
    EHour: TEdit;
    Label1: TLabel;
    EMin: TEdit;
    Label2: TLabel;
    ESec: TEdit;
    Button1: TButton;
  private
    //FTime:TDateTime;
    function GetTime: TDateTime;
    procedure SetTime(const Value: TDateTime);
  public
    property Time:TDateTime read GetTime write SetTime;
  end;

var
  TimeForm: TTimeForm;

implementation uses StrUtils;

{$R *.xfm}

function TTimeForm.GetTime:TDateTime;
//var SysTime:TSystemTime;
begin
  {
   with SysTime do begin
      wMonth:=12;
      wDay:=30;
      wYear:=1899;
      wHour:=StrToInt(EHour.Text);
      wMinute:=StrToInt(EMin.Text);
      wSecond:=StrToInt(ESec.Text);
      wMilliseconds:=0;
   end;}
   //Result:=SystemTimeToDateTime(SysTime);
   Result:=Date() + EncodeTime(StrToInt(EHour.Text),StrToInt(EMin.Text),StrToInt(ESec.Text),0);
end;

procedure TTimeForm.SetTime(const Value:TDateTime);
var StrTime:string;
begin
  StrTime:=TimeToStr(Value);
  EHour.Text:=MidStr(StrTime,1,2);
  EMin.Text:=MidStr(StrTime,4,2);
  ESec.Text:=MidStr(StrTime,7,2);
end;

end.
