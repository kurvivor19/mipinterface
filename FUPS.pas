UNIT FUPS;

INTERFACE

uses
  SysUtils, Types, Classes, Variants, QTypes, QGraphics, QControls, QForms,
  QDialogs, QStdCtrls, QComCtrls,
  SmartWinner,
  RxUtils,
  RxAlias;

type TUPSStat = packed record
  IPV:F32;           // ������� ���������� �
  OPV:F32;           // �������� ���������� �
  OPF:F32;           // �������� ������� ��
  Load:F32;          // �������� % (�������� ���) 
  Temp:F32;          // ����������� ��� ����. �������
  UtilityFail:BOOL;  // ��� �������� ����������
  BatteryLow:BOOL;   // ������ ����� �������
  Failed:BOOL;       // ��� ���������
  Test:BOOL;         // ���� ���������������� ���
  BatteryCharge:F32; // ������� ������ �������
  Mode:TUPSMode;     // ����� ������ ���
  UPSType:TUPSType;  // ��� ��� 
  State:TUPSState;   // ������� ��������� ���
end;

type TUpsEvent = procedure of object;

type
  TUpsForm = class(TForm)
    GBUps: TGroupBox;
    LStat: TLabel;
    B3: TButton;
    B4: TButton;
    B2: TButton;
    LSOC: TLabel;
    pbSOC: TProgressBar;
    UpsState: TLabel;
    B1: TButton;
    LBON: TLabel;
    LSHTDWN: TLabel;
    LTIP: TLabel;
    LLI: TLabel;
    LONL: TLabel;
    LAVR: TLabel;
    LNORMAL: TLabel;
    LUPSF: TLabel;
    LBL: TLabel;
    LUF: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure B1Click(Sender: TObject);
  private
    FSOC:U8;
    Ups:TSmartWinner;
    FOnEvent: TUpsEvent;
    FET: TExchangeState;
    FState: TUPSState;
    FUPSStat: TUPSStat;
    procedure UpsDataAvaliable(Sender:TObject; ET:U16 );
    function GetDeviceName: string;
    procedure SetDeviceName(const Value: string);
    procedure SetUPSStat(const Value: TUPSStat);
  public
    Alm:BOOL;
    Load:U8;
    function Connect():BOOL;
    procedure Disconnect();
    //
    property UPSStat:TUPSStat read FUPSStat write SetUPSStat;
    //
    property DeviceName:string read GetDeviceName write SetDeviceName;
    property OnEvent:TUpsEvent read FOnEvent write FOnEvent default NIL;
    property State:TUPSState read FState;
    property SOC:U8 read FSOC;
  end;

var
  UpsForm: TUpsForm;

IMPLEMENTATION

{$R *.xfm}

const ParamName:array[1..7] of string = (
  '������� ����������      [�]  ',
  '������� ���������� ������� ��������� [�]',
  '�������� ����������     [�]  ',
  '�������� ��� (��������)  %   ',
  '�������� �������       [��]  ',
  '���������� �������      [�]  ',
  '�����������            [�C]  '
);

const
  UpsStateName:array[0..UPS_MAX_STATE] of string = (
  '�� ���������',
  '�������������',
  '��������',
  '�������',
  'WShtDwn',
  'ShtDwn',
  '�� ��������'
);

procedure TUpsForm.FormCreate(Sender: TObject);
begin
  Ups:=TSmartWinner.Create(Self, True);
  Ups.Chnl.DeviceName:='';
  //Ups.Chnl[1].DeviceName:='';
  Ups.AutoDetect:=True;
  Ups.OnDataAvaliable:=UpsDataAvaliable;

  LStat.Caption:=ParamName[1] + CRLF +
  //ParamName[2] + CRLF +
  ParamName[3] + CRLF +
  ParamName[4] + CRLF +
  ParamName[5] + CRLF +
  ParamName[6] + CRLF +
  ParamName[7];
end;

procedure TUpsForm.UpsDataAvaliable(Sender: TObject; ET: U16);
begin
 (*
  FState:=Ups.UPSState;
  
  case ET of
    ANS_STATUS_INQUIRY:begin
      //LStat.Color:=clSilver;
      with Ups.StatusInquire do begin
        LStat.Caption:=
          ParamName[1] + Format('%5.1f',[IPV]) + CRLF +
          //ParamName[2] + Format('%5.1f',[IPFV]) + CRLF +
          ParamName[3] + Format('%5.1f',[OPV]) + CRLF +
          ParamName[4] + Format('%4d'  ,[OPC]) + CRLF +
          ParamName[5] + Format('%4.1f',[OPF]) + CRLF +
          ParamName[6] + Format('%4.2f',[BATV]) + CRLF +
          ParamName[7] + Format('%4.1f',[TEMPC]);
          //Format('Battery SOC=        %4d %%'  ,[0]) + CRLF
        ;
        if(UF)     then LUF.Color:=clLime else LUF.Color:=clSilver;
        if(BATLOW) then LBL.Color:=clLime else LBL.Color:=clSilver;

        if(UPSM = upsmAVR) then LAVR.Color:=clLime else LAVR.Color:=clSilver;
        if(UPSM = upsmNormal) then LNORMAL.Color:=clLime else LNORMAL.Color:=clSilver;

        if(UPSF) then LUPSF.Color:=clLime else LUPSF.Color:=clSilver;

        if(UPST = upstLineInteractive) then LLI.Color:=clLime else LLI.Color:=clSilver;
        if(UPST = upstOnLine) then LONL.Color:=clLime else LONL.Color:=clSilver;

        if(TST)    then LTIP.Color:=clLime else LTIP.Color:=clSilver;
        if(SHTDWN) then LSHTDWN.Color:=clLime else LSHTDWN.Color:=clSilver;
        if(BEEPON) then LBON.Color:=clLime else LBON.Color:=clSilver;

        FSOC:=5;
        pbSOC.Position:=FSOC;
        pbSOC.Caption:=IntToStr(FSOC) + '%';

        //UpsState.Caption:=UpsStateName[U8(Ups.UPSState)];
      end;
    end;
    ANS_RATING_INFORMATION:begin
    end;
    ANS_CENTRALION_PROTOCOL_DETECTED:begin
    end;
    NO_ANSWERED:begin
      LStat.Caption:=ParamName[1] + CRLF +
      //ParamName[2] + CRLF +
      ParamName[3] + CRLF +
      ParamName[4] + CRLF +
      ParamName[5] + CRLF +
      ParamName[6] + CRLF +
      ParamName[7];
      
      LUF.Color:=clGray;
      LBL.Color:=clGray;
      LNORMAL.Color:=clGray;
      LAVR.Color:=clGray;
      LUPSF.Color:=clGray;
      LLI.Color:=clGray;
      LONL.Color:=clGray;
      LTIP.Color:=clGray;
      LSHTDWN.Color:=clGray;
      LBON.Color:=clGray;
    end;
  end;

  //for I:=0 to UPS_MAX_STATE do UpsStat[I].Color:=clSilver;
  //UpsStat[U8(Ups.UPSState)].Color:=clLime;
  UpsState.Color:=clLime;
  UpsState.Caption:=UpsStateName[U8(Ups.UPSState)];

  if Assigned(OnEvent) then OnEvent;
*)
  FState:=Ups.UPSState;
  
  case ET of
    ANS_STATUS_INQUIRY:begin
      //LStat.Color:=clSilver;
      with Ups.StatusInquire do begin
        LStat.Caption:=
          ParamName[1] + Format('%5.1f',[IPV]) + CRLF +
          //ParamName[2] + Format('%5.1f',[IPFV]) + CRLF +
          ParamName[3] + Format('%5.1f',[OPV]) + CRLF +
          ParamName[4] + Format('%4d'  ,[OPC]) + CRLF +
          ParamName[5] + Format('%4.1f',[OPF]) + CRLF +
          ParamName[6] + Format('%4.2f',[BATV]) + CRLF +
          ParamName[7] + Format('%4.1f',[TEMPC]);
          //Format('Battery SOC=        %4d %%'  ,[0]) + CRLF
        ;
        //if(UF)     then LUF.Color:=clLime else LUF.Color:=clSilver;
        //if(BATLOW) then LBL.Color:=clLime else LBL.Color:=clSilver;

        if(UF)     then LUF.Color:=clRed else LUF.Color:=clSilver;
        if(BATLOW) then LBL.Color:=clRed else LBL.Color:=clSilver;

        if(UPSM = upsmAVR) then LAVR.Color:=clLime else LAVR.Color:=clSilver;
        if(UPSM = upsmNormal) then LNORMAL.Color:=clLime else LNORMAL.Color:=clSilver;

        //if(UPSF) then LUPSF.Color:=clLime else LUPSF.Color:=clSilver;
        if(UPSF) then LUPSF.Color:=clRed else LUPSF.Color:=clSilver;

        if(UPST = upstLineInteractive) then LLI.Color:=clLime else LLI.Color:=clSilver;
        if(UPST = upstOnLine) then LONL.Color:=clLime else LONL.Color:=clSilver;

        if(TST)    then LTIP.Color:=clLime else LTIP.Color:=clSilver;
        if(not SHTDWN) then LSHTDWN.Color:=clLime else LSHTDWN.Color:=clSilver;
        if(BEEPON) then LBON.Color:=clLime else LBON.Color:=clSilver;

        Alm:=UF or BATLOW or UPSF;
        Load:=OPC;

        FSOC:=SOC;
        pbSOC.Position:=SOC;
        pbSOC.Caption:=IntToStr(SOC) + '%';

        //UpsState.Caption:=UpsStateName[U8(Ups.UPSState)];
      end;
    end;
    ANS_RATING_INFORMATION:begin
    end;
    ANS_CENTRALION_PROTOCOL_DETECTED:begin
    end;
    NO_ANSWERED:begin
      LStat.Caption:=ParamName[1] + CRLF +
      //ParamName[2] + CRLF +
      ParamName[3] + CRLF +
      ParamName[4] + CRLF +
      ParamName[5] + CRLF +
      ParamName[6] + CRLF +
      ParamName[7];
      
      LUF.Color:=clGray;
      LBL.Color:=clGray;
      LNORMAL.Color:=clGray;
      LAVR.Color:=clGray;
      LUPSF.Color:=clGray;
      LLI.Color:=clGray;
      LONL.Color:=clGray;
      LTIP.Color:=clGray;
      LSHTDWN.Color:=clGray;
      LBON.Color:=clGray;
    end;
  end;

  //for I:=0 to UPS_MAX_STATE do UpsStat[I].Color:=clSilver;
  //UpsStat[U8(Ups.UPSState)].Color:=clLime;
  UpsState.Color:=clLime;
  UpsState.Caption:=UpsStateName[U8(Ups.UPSState)];

  if Assigned(OnEvent) then OnEvent;



  FUPSStat.IPV:=Ups.StatusInquire.IPV;
  FUPSStat.OPV:=Ups.StatusInquire.OPV;
  FUPSStat.OPF:=Ups.StatusInquire.OPF;
  FUPSStat.Temp:=Ups.StatusInquire.TEMPC;
  FUPSStat.UtilityFail:=Ups.StatusInquire.UF;
  FUPSStat.BatteryLow:=Ups.StatusInquire.BATLOW;
  FUPSStat.Failed:=Ups.StatusInquire.UPSF;
  FUPSStat.BatteryCharge:=0;
  FUPSStat.Mode:=Ups.StatusInquire.UPSM;
  FUPSStat.UPSType:=Ups.StatusInquire.UPST;
  FUPSStat.State:=Ups.UPSState;
  FUPSStat.Load:=Ups.StatusInquire.OPC;
  FUPSStat.Test:=Ups.StatusInquire.TST;

end;

function TUpsForm.GetDeviceName: string;
begin
 Result:=Ups.Chnl.DeviceName;
end;

procedure TUpsForm.SetDeviceName(const Value: string);
begin
 Ups.Chnl.DeviceName:=Value;
end;

function TUpsForm.Connect: BOOL;
begin
  Result:=Ups.Connect();
end;

procedure TUpsForm.Disconnect;
begin
  Ups.Disconnect();
  Ups.Free();
end;

procedure TUpsForm.B1Click(Sender: TObject);
begin
  case (Sender as TButton).Tag of
    0:Ups.Test();
    1:Ups.ToggleBeeper();
    2:Ups.Shutdown();
    3:Ups.Restore();
  end;
end;

procedure TUpsForm.SetUPSStat(const Value: TUPSStat);
begin
  FUPSStat := Value;
end;

end.


